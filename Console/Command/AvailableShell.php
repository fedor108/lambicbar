<?php
/**
 * Манипуляции с доступностью блюд в ресторане
 */
class AvailableShell extends AppShell
{

    public $uses = array('Node', 'Connection');

/**
 *  Отобразить разделы и блюда меню
 */
    public function main()
    {
        $this->out("Сделать все разделы меню и блюда доступными во всех ресторанах");
        $this->out("Console/cake available set beer|menu");
        $this->out("Сделать все разделы меню и блюда доступными в ресторане place_id");
        $this->out("Console/cake available set beer|menu place_id");
        $this->out("Сделать раздел меню section_id и его блюда доступными в ресторане place_id");
        $this->out("Console/cake available set beer|menu place_id section_id");
        $this->out("\r\nДанные Меню\r\n");

        $address = $this->Node->find('first', array(
            'conditions' => array('Node.name' => 'address'),
            'recursive' => -1,
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        // $this->out(print_r(compact('address'), true));

        $places = $this->Node->find('all', array(
            'conditions' => array(
                'Node.parent_id' => $address['Node']['id'],
                'Node.node_state' => 'published'
            ),
            'contain' => array('Connection'),
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        foreach ($places as $place) {
            $out = "Ресторан ";
            foreach ($place['Node'] as $key => $value) {
                $out .= "{$key}: {$value} ";
            }
            if (!empty($place['Connection'])) {
                $out .= " connected: ";
                foreach ($place['Connection'] as $item) {
                    $out .= "{$item['term_id']}: {$item['active']} ";
                }
            }
            $this->out($out);
        }

        $catalogs = $this->Node->find('all', array(
            'conditions' => array('Node.name' => array('menu', 'beer')),
            'recursive' => -1,
            'fields' => array('id', 'name', 'title', 'slug'),
        ));

        // $this->out(print_r(compact('menu'), true));
        foreach ($catalogs as $catalog) {
            $sections = $this->Node->find('all', array(
                'conditions' => array(
                    'Node.parent_id' => $catalog['Node']['id'],
                    'Node.node_state' => 'published'
                ),
                'contain' => array('Connection'),
                'fields' => array('id', 'name', 'title', 'slug'),
            ));
            foreach ($sections as $section) {
                $out = "\r\nРаздел меню ";
                foreach ($section['Node'] as $key => $value) {
                    $out .= "{$key}: {$value} ";
                }
                if (!empty($section['Connection'])) {
                    $out .= " connected: ";
                    foreach ($section['Connection'] as $item) {
                        $out .= "{$item['term_id']}: {$item['active']} ";
                    }
                }
                $this->out($out);

                $products = $this->Node->find('all', array(
                    'conditions' => array(
                        'Node.parent_id' => $section['Node']['id'],
                        'Node.node_state' => 'published'
                    ),
                    'contain' => array('Connection'),
                    'fields' => array('id', 'name', 'title', 'slug'),
                ));
                foreach ($products as $product) {
                    $out = " Блюдо меню ";
                    foreach ($product['Node'] as $key => $value) {
                        $out .= "{$key}: {$value} ";
                    }
                    if (!empty($product['Connection'])) {
                        $out .= " connected: ";
                        foreach ($product['Connection'] as $item) {
                            $out .= "{$item['term_id']}: {$item['active']} ";
                        }
                    }
                    $this->out($out);
                }
            }
        }
    }

/**
 * Сделать все разделы и блюда доступными во всех ресторанах
 */

    public function set()
    {
        $this->out("Делаем все разделы и блюда доступными во всех ресторанах\r\n");


        // $this->out(print_r(compact('address'), true));

        $conditions = array(
            'Node.parent_id' => $address['Node']['id'],
            'Node.node_state' => 'published'
        );
        if (!empty($this->args[1])) {
            $conditions['Node.id'] = $this->args[1];
        }
        $places = $this->Node->find('all', array(
            'conditions' => $conditions,
            'contain' => array('Connection'),
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        foreach ($places as $place) {
            $out = "Ресторан ";
            foreach ($place['Node'] as $key => $value) {
                $out .= "{$key}: {$value} ";
            }
            if (!empty($place['Connection'])) {
                $out .= " connected: ";
                foreach ($place['Connection'] as $item) {
                    $out .= "{$item['term_id']}: {$item['active']} ";
                }
            }
            $this->out($out);
        }
        if (!in_array($this->args[0], array('beer', 'menu'))) {
            $this->out('Не правильно задан раздел каталога (beer или menu): ' . $this->args[0]);
            return 1;
        }
        $menu = $this->Node->find('first', array(
            'conditions' => array('Node.name' => $this->args[0]),
            'recursive' => -1,
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        // $this->out(print_r(compact('menu'), true));

        $conditions = array(
            'Node.parent_id' => $menu['Node']['id'],
            'Node.node_state' => 'published'
        );
        if (!empty($this->args[2])) {
            $conditions['Node.id'] = $this->args[2];
        }
        $sections = $this->Node->find('all', array(
            'conditions' => $conditions,
            'contain' => array('Connection'),
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        foreach ($sections as $section) {
            $out = "\r\nРаздел меню ";
            foreach ($section['Node'] as $key => $value) {
                $out .= "{$key}: {$value} ";
            }
            $connected = Hash::extract($section['Connection'], '{n}.term_id');
            foreach ($places as $place) {
                if (!array_key_exists($place['Node']['id'], $connected)) {
                    $connection = array(
                        'active' => 1,
                        'term_id' => $place['Node']['id'],
                        'node_id' => $section['Node']['id'],
                        'dictionary_id' => $address['Node']['id']
                    );
                    $out .= "\r\nДобавляем соединение: \r\n" . print_r(compact('connection'), true);
                    $this->Node->Connection->create();
                    if (!$this->Connection->save($connection)) {
                        $out .= "\r\nНе удалось записать соединения: \r\n" . print_r(compact('connection'), true);
                    }
                }
            }
            $this->out($out);

            $products = $this->Node->find('all', array(
                'conditions' => array(
                    'Node.parent_id' => $section['Node']['id'],
                    'Node.node_state' => 'published'
                ),
                'contain' => array('Connection'),
                'fields' => array('id', 'name', 'title', 'slug'),
            ));
            foreach ($products as $product) {
                $out = " Блюдо меню ";
                foreach ($product['Node'] as $key => $value) {
                    $out .= "{$key}: {$value} ";
                }
                $connected = Hash::extract($product['Connection'], '{n}.term_id');
                foreach ($places as $place) {
                    if (!array_key_exists($place['Node']['id'], $connected)) {
                        $connection = array(
                            'active' => 1,
                            'term_id' => $place['Node']['id'],
                            'node_id' => $product['Node']['id'],
                            'dictionary_id' => $address['Node']['id']
                        );
                        $out .= "\r\nДобавляем соединение: \r\n" . print_r(compact('connection'), true);
                        $this->Node->Connection->create();
                        if (!$this->Connection->save($connection)) {
                            $out .= "\r\nНе удалось записать соединения: \r\n" . print_r(compact('connection'), true);
                        }
                    }
                }
                $this->out($out);
            }
        }
    }

    private function getVars()
    {
        if (!empty($this->args[0])) {
            $name = $this->args[0];
        }
        if (!empty($this->args[1])) {
            $place = $this->args[1];
        }
        if (!empty($this->args[2])) {
            $section = $this->args[2];
        }
        return compact('place', 'name', 'section');
    }

    public function check()
    {
        $place = null;
        $name = null;
        $section = null;
        extract($this->getVars());

        $address = $this->address();
        $this->out($this->array2str($address['Node']));

        $places = $this->places($address['Node']['id'], $place);
        foreach ($places as $place) {
            $this->out($this->array2str($place['Node']));
        }
        $terms = Hash::extract($places, '{n}.Node.id');

        $menu = $this->menu($name);
        $this->out($this->array2str($menu['Node']));

        $sections = $this->sections($menu['Node']['id'], $section);
        foreach ($sections as $section) {
            $this->out("\r\nРаздел меню: " . $this->array2str($section['Node']));
            $this->on($section['Node']['id'], $terms, $address['Node']['id']);

            $products = $this->products($section['Node']['id']);
            foreach ($products as $product) {
                $this->out("        Продукт: " . $this->array2str($product['Node']));
                $this->on($product['Node']['id'], $terms, $address['Node']['id']);
            }
        }
    }

    private function on($node_id, $terms, $dictionary_id)
    {
        foreach ($terms as $term_id) {
            $connection = array(
                'node_id' => $node_id,
                'term_id' => $term_id,
                'dictionary_id' => $dictionary_id,
                'active' => 1
            );
            $this->createIfNotExists($connection);
        }
    }

    private function createIfNotExists($connection)
    {
        $exist = $this->Connection->find('first', array(
            'conditions' => array(
                'Connection.node_id' => $connection['node_id'],
                'Connection.term_id' => $connection['term_id']
            )
        ));
        // $this->Connection->Behaviors->disable('Loggable');
        if (empty($exist)) {
            $this->Connection->create();
            if (!$this->Connection->save(array('Connection' => $connection))) {
                $this->out("--- Не удалось создать связь: " . $this->array2str($connection));
            } else {
                $this->out("+++ Создана связь: {$this->Connection->id}: " . $this->array2str($connection));
            }
        } else {
            $this->out("=== Найдена связь: " . $this->array2str($exist['Connection']));
            $connection['id'] = $exist['Connection']['id'];
        }
        $this->Connection->clear();
    }

    private function array2str($arr = array()) {
        $str = '';
        if (!empty($arr))
        foreach ($arr as $key => $value) {
            $str .= "{$key}: {$value} ";
        }
        return $str;
    }

    private function address()
    {
        $address = $this->Node->find('first', array(
            'conditions' => array(
                'Node.name' => 'address',
                'Node.parent_id' => 1
            ),
            'recursive' => -1,
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        return $address;
    }

    private function places($parent_id, $id = null)
    {
        $conditions = array(
            'Node.parent_id' => $parent_id,
            'Node.node_state' => 'published'
        );
        if (!empty($id)) {
            $conditions['Node.id'] = $id;
        }
        $places = $this->Node->find('all', array(
            'conditions' => $conditions,
            'contain' => array('Connection'),
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        return $places;
    }

    private function menu($name)
    {
        $address = $this->Node->find('first', array(
            'conditions' => array(
                'Node.name' => $name,
                'Node.parent_id' => 1
            ),
            'recursive' => -1,
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        return $address;
    }

    private function sections($parent_id, $id = null)
    {
        $conditions = array(
            'Node.parent_id' => $parent_id,
        );
        if (!empty($id)) {
            $conditions['Node.id'] = $id;
        }
        $sections = $this->Node->find('all', array(
            'conditions' => $conditions,
            'contain' => array('Connection'),
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        return $sections;
    }

    private function products($parent_id) {
        $products = $this->Node->find('all', array(
            'conditions' => array(
                'Node.parent_id' => $parent_id,
            ),
            'contain' => array('Connection'),
            'fields' => array('id', 'name', 'title', 'slug'),
        ));
        return $products;
    }

}
