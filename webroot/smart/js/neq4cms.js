$(document).ready(function() {

    // SmartAdmin вызов - не убирать
    pageSetUp();

    // дерево узлов
    $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
    $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > div').attr('title', 'Развернуть').find(' > i').on('click', function(e) {
        var children = $(this).parent('div').parent('li.parent_li').find(' > ul > li');
        if (children.is(':visible')) {
            children.hide('fast');
            $(this).attr('title', 'Развернуть').removeClass().addClass('fa fa-lg fa-folder');
        } else {
            children.show('fast');
            $(this).attr('title', 'Свернуть').removeClass().addClass('fa fa-lg fa-folder-open');
        }
        e.stopPropagation();
    });

    // подключаем визуальный редактор
    $('.visual').each(function(){
        CKEDITOR.replace( $(this).attr('id') );
    });

    // таблицы данных для списка новостей
    $('.data-table').dataTable({
        "language": {  "processing": "Подождите...",  "lengthMenu": "_MENU_",  "info": "Записи с _START_ до _END_ из _TOTAL_ записей",  "infoEmpty": "Записи с 0 до 0 из 0 записей",  "infoFiltered": "(отфильтровано из _MAX_ записей)",  "infoPostFix": "",  "loadingRecords": "Загрузка записей...",  "zeroRecords": "Записи отсутствуют.",  "emptyTable": "В таблице отсутствуют данные",  "paginate": {    "first": "Первая",    "previous": "Предыдущая",    "next": "Следующая",    "last": "Последняя"  },  "aria": {    "sortAscending": ": активировать для сортировки столбца по возрастанию",    "sortDescending": ": активировать для сортировки столбца по убыванию"  }},
        "pagingType": "full_numbers",
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-md-12'p>>",
        "autoWidth" : true
    });

    // сортировка списка потомков узла
    $('.node-children').sortable({
        //revert: true,
        placeholder: "ui-state-highlight",
        update: function( event, ui ) {
            $.ajax({
                type : "POST",
                url  :  '/admin/nodes/sort',
                data : { sort : $(this).sortable( "toArray", { attribute: "data-id" } ) },
                beforeSend: function() {
                    $(this).hide();
                },
                complete: function() {
                    $(this).show();
                }
            });
        }
    });

    $('.node-children').disableSelection();

    // прием узла элементом дерева
    // $('.tree-item').droppable({
    //     hoverClass: "drop-hover",
    //     tolerance: "pointer",
    //     over: function( event, ui ) {
    //         var children = $(this).parent('li.parent_li').find(' > ul > li');
    //         children.show('fast');
    //         $(this).find(' > i').attr('title', 'Развернуть').removeClass().addClass('fa fa-lg fa-folder-open');
    //     },
    //     drop: function( event, ui ) {
    //         var id = ui.draggable.data("id");
    //         var parent_id = $(this).data("id");
    //         console.log(id, parent_id);
    //         $.ajax({
    //             type : "POST",
    //             url  :  '/admin/nodes/move/'+id+'/'+parent_id,
    //             dataType: 'json',
    //             success: function(result) {
    //                 console.log(result);
    //                 if (result.error) {
    //
    //                 } else {
    //                     ui.draggable.remove();
    //                 }
    //             }
    //         });
    //     }
    // });

    // изображения в галерее
    $('.gallery').sortable({
        update: function( event, ui ) {
            $('.gallery').find('li').each(function(i) {
                $(this).find('#' + $(this).data('key') + 'Order').val(i);
            });
        }
    });

    // дерево пунктов меню
    $('#nestable-menu').nestable().on('change', function(){
        var order = window.JSON.stringify($(this).nestable('serialize'));
        $.ajax({
            type: "POST",
            url: "/admin/menu_items/reorder",
            data: {
                order : order
            }
        });
    });

    // настройка для ввода даты
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3C;Пред',
        nextText: 'След&#x3E;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Нед',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    // ввод даты и времени в одном поле для типа datetime
    $('.datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'YYYY-MM-DD HH:mm'
    });

    // кнопка сохранения узла вне формы
    $('.node-save-submite').click(function(){
        $('#'+$(this).data('form-id')).submit();
    });

})