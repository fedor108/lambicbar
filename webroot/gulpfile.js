var gulp = require('gulp'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer-core'),
	assets  = require('postcss-assets'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	csso = require('gulp-csso');

// Обрабываем цсс
gulp.task('css', function() {
	var processors = [
		require("postcss-import"),
		require('postcss-mixins'),
		require('postcss-nested'),
		require('postcss-simple-vars'),
		require("postcss-color-function"),
		require('postcss-calc'),
		require("postcss-color-rgba-fallback"),
		require('lost'),
		assets({
			loadPaths: ['images/']
		}),
		autoprefixer({
			browsers: ['last 2 version']
		})
    ];

  	gulp.src('source/css/style.css')
  		.pipe(postcss(processors))
  		// .pipe(csso())
  		.pipe(gulp.dest('css'));

  	gulp.src('source/css/404.css')
  		.pipe(postcss(processors))
  		// .pipe(csso())
  		.pipe(gulp.dest('css'));
});

gulp.task('watch', function() {
	// Предварительная сборка
	gulp.run('css');
	gulp.task('js-source');

	gulp.watch('source/css/**/*.css', function() {
        gulp.run('css');
    });

    gulp.watch('source/js/**/*.js', function() {
        gulp.run('js-source');
    });
});

gulp.task('js-libs', function() {
	gulp.src([
		'source/js/components/jquery/dist/jquery.min.js',
		'source/js/components/underscore/underscore-min.js',
		'source/js/components/fotorama/fotorama.js',
		'source/js/components/social-likes/social-likes.min.js',
		'source/js/components/icheck/icheck.min.js',
		'source/js/components/jquery.inputmask/dist/inputmask/jquery.inputmask.js',
		'source/js/components/jquery.inputmask/dist/inputmask/jquery.inputmask.extensions.js',
		'source/js/components/parsleyjs/dist/parsley.min.js',
		'source/js/components/parsleyjs/src/i18n/ru.js',
		'source/js/components/parsleyjs/src/i18n/ru.extra.js',
		'source/js/components/parsleyjs/src/extra/validator/words.js',
		'source/js/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
		'source/js/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js',
		'source/js/components/sticky/jquery.sticky.js',
		'source/js/components/select2/dist/js/select2.full.min.js',
		'source/js/components/bootstrap/js/transition.js',
		'source/js/components/bootstrap/js/modal.js'
	])
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest('js'));
});

gulp.task('js-source', function() {
	gulp.src([
		'source/js/file-input.js',
		'source/js/text-limit.js',
		'source/js/google-maps.js',
		'source/js/forms.js',
		'source/js/main.js'
	])
    .pipe(concat('script.js'))
    .pipe(uglify())
    .pipe(gulp.dest('js'));
});

gulp.task('js', function() {
	gulp.run('js-libs');
	gulp.run('js-source');
});

gulp.task('default', function() {
  	gulp.run('css');
  	gulp.run('js');
});
