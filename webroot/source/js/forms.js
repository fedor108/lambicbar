$(document).ready(function () {
    window.ParsleyValidator.addValidator(
        'phone',
        function (value, nbWords) {
            var phoneExp = /\+*[78] \(*\d{3}\)* \d{3}\-\d{2}\-\d{2}/;

            return phoneExp.test(val);
        }, 32)
        .addMessage('ru', 'phone', 'Телефон должен быть в формате +7 999 999-99-99');


    $(".form").each(function () {
        $(this).parsley({
            errorsContainer: function (ParsleyField) {
                var $field = ParsleyField.$element,
                    isSelect = $field.hasClass("select2-hidden-accessible"),
                    errorContainer = false;

                if (isSelect) {
                    errorContainer = $field.closest('.select2-widget--transparent').parent();
                }

                return errorContainer;
            }
        });
    });

    // Отправка формы отзыва
    $("#report-form").on("submit", function (event) {
        event.preventDefault();
        var data = new FormData($("#report-form")[0]);
        $.ajax({
            type: "POST",
            url: "/reports/add",
            processData: false,
            contentType: false,
            data: data,
            beforeSend: function () {
                $('button[type="submit"]').attr('disabled', 'disabled');
            },
            success: function (data) {
                $("#result").text(data.message);
                $('button[type="submit"]').removeAttr('disabled');
                $("#report-form")[0].reset();
                $('.datepicker').datepicker("setDate", new Date());
            },
            error: function (data) {
                $("#result").text(data.message);
                $('button[type="submit"]').removeAttr('disabled');
            },
            dataType: "json"
        });
    });

    // Отправка анкеты соискателя
    $("#vacancy-form").on("submit", function (event) {
        event.preventDefault();
        var data = new FormData($("#vacancy-form")[0]);
        $.ajax({
            type: "POST",
            url: "/resumes/add",
            processData: false,
            contentType: false,
            data: data,
            beforeSend: function () {
                $('button[type="submit"]').attr('disabled', 'disabled');
            },
            success: function (data) {
                $("#result").text(data.message);
                $('button[type="submit"]').removeAttr('disabled');
            },
            error: function (data) {
                $("#result").text(data.message);
                $('button[type="submit"]').removeAttr('disabled');
            },
            dataType: "json"
        });
    });

    // Бронирование столика
    $("#order-form").on("submit", function (event) {
        event.preventDefault();
        var data = new FormData($("#order-form")[0]);
        $.ajax({
            type: "POST",
            url: "/orders/add",
            processData: false,
            contentType: false,
            data: data,
            beforeSend: function () {
                $('button[type="submit"]').attr('disabled', 'disabled');
            },
            success: function (data) {
                $("#order-form")[0].reset();
                $("#popup-orderSuccess").modal("show");
                $('button[type="submit"]').removeAttr('disabled');
            },
            error: function (data) {
                $('button[type="submit"]').removeAttr('disabled');
            },
            dataType: "json"
        });
    });
});
