// $('.js-masonry').isotope({
//   	itemSelector: '.js-masonry-item',
//   	layoutMode: 'fitRows'
// });
// $('[data-responsive-menu]').each(function () {
// 	$(this).mobileMenu({
// 		switchWidth: $(this).data('responsiveMenu') || 640,
// 		prependTo: $(this).parent()
// 	});
// });

// $(".js-scroll-to").on("click", function() {
//     var target = $($(this).attr("href"));

//     $('html, body').stop().animate({scrollTop:target.offset().top}, 'slow');
//     return false;
// });

$(".gallery").not("[data-auto=false]").fotorama();
$(".gallery--home").on("fotorama:showend", function () {
    $(this).addClass("gallery--init");

    $(".gallery-slide-anchor").each(function () {
        var target = $($(this).attr("href"));

        $(this).on("click", function () {
            $('html, body').stop().animate({scrollTop: target.offset().top}, 'slow');
            return false;
        });
    });
}).fotorama();
$(".share").socialLikes({
    counters: true,
});
// $(".select").selectize();


$("input").iCheck();
$("[data-type=phone]").inputmask({
    mask: '+7 999 999-99-99',
    showMaskOnHover: false
});
$("[data-type=time]").inputmask({
    mask: '99:99',
    showMaskOnHover: false
});
$("[data-type=persons]").inputmask({
    mask: '9{1,2}',
    showMaskOnHover: false
});
$("[data-type=email]").inputmask({
    alias: "email",
    showMaskOnHover: false
});
$(".datepicker").datepicker({
    startDate: '0',
    format: "d MM yyyy",
    language: "ru",
    autoclose: true
});

$('.datepicker').datepicker("setDate", new Date());
$('.datepicker').datepicker("minDate", '0');
$('.datepicker').datepicker("startDate", '0');

$("[data-datepicker]").on("click", function () {
    var datepicker = $(this).data("datepicker");

    $("#" + datepicker).focus();
    return false;
});

function showMenu() {
    $(".mobileNav").show().scrollTop(0);
    $("body").addClass("modal-open");

    setTimeout(function () {
        $(".mobileNav").addClass("in");
    }, 100);
}

function hideMenu() {
    $(".mobileNav").removeClass("in");

    setTimeout(function () {
        $("body").removeClass("modal-open");
        $(".mobileNav").hide();
    }, 300);
}

$(".js-toggle-menu").on("click", function () {
    showMenu();
});

$(".js-close-menu").on("click", function () {
    hideMenu();
});

$(".mobileNav").on("click", function (event) {
    if (!$(event.target).closest(".mobileNav-inner").length) {
        hideMenu();
    }
});

$("[data-limit]").limiter();

$(".js-sticky").sticky({topSpacing: 20});


// Dropdown
$.fn.select2.amd.require(
    ["select2/utils", "select2/dropdown", "select2/dropdown/attachContainer"],
    function (Utils, DropdownAdapter, AttachContainer) {
        AttachContainer.prototype.bind = function (decorated, container, $container) {
            // Just pass the call through to the decorated class
            decorated.call(this, container, $container);
        };
        var CustomAdapter = Utils.Decorate(DropdownAdapter, AttachContainer);

        if ($(".js-select-page").length > 0) {
            $("select.js-select-page[data-shuffle]").each(function () {
                var parent = $(this),
                    elements = parent.children("option");
                while (elements.length > 0) {
                    // console.log(elements.length);
                    parent.append(elements.splice(Math.floor(Math.random() * elements.length), 1)[0]);
                }
            });
            $(".js-select-page").select2({
                dropdownAdapter: CustomAdapter
            });
        }
    });
$(".js-select-page").on("change", function () {
    var page = $(this).val();
    window.location.replace(page);
});
if ($(".js-feedback-select").length > 0) {
    $(".js-feedback-select").select2();
    $(window).on("resize", function () {
        $(".js-feedback-select").select2("close");
    });
}


$("[data-show-gallery]").on("click", function () {
    var galleryId = $(this).data("showGallery"),
        $gallery = $("#" + galleryId),
        $body = $("body"),
        isInit = $gallery.hasClass("is-init"),
        startSlideIndex = $(this).data("galleryStart") || false;

    function showGallery() {
        $gallery.fadeIn();
        $body.addClass("modal-open");
    }

    function hideGallery() {
        $gallery.fadeOut();
        $body.removeClass("modal-open");
    }

    function showSlideAtIndex(index) {
        if (index) {
            var api = $gallery.data("fotorama")
            api.show({index: index, time: 0});
        }
    }

    function hideOnESC() {
        $(window).keydown(function (eventObject) {
            if (eventObject.which == 27) {
                hideGallery();
            }
        });
    }

    if (!isInit) {
        $gallery
            .on("fotorama:ready", function () {
                $(this).addClass("is-init");
                showSlideAtIndex(startSlideIndex);

                $(this).find(".fotorama__stage").append("<a class='gallery-close icon-close'></a>");
                $(this).find(".gallery-close").on("click", hideGallery);

                hideOnESC();
            })
            .fotorama();
    }
    else {
        showSlideAtIndex(startSlideIndex);
    }

    showGallery();

    return false;
});


if ($("#contacts-map").length) {
    $("#contacts-map").googlemap({
        marker: {
            image: "/images/location-pin.png",
            size: {
                width: 50,
                height: 64
            }
        }
    });
}

var placesfromserver = placesfromserver || [
        {
            city: "Москва",
            address: "ул. Мясницкая д. 40А",
            infoWindowData: {
                "address": "ул. Мясницкая д. 40А",
                "email": "info@lambicbar.ru",
                "phone": "8 (495) 542-00-37",
                "workinghours": {
                    "weekdays": "Пн-Чт: с 10:00 до 23:00",
                    "weekend": "Пт-Вс: с 10:00 до 3:00"
                }
            }
        },
        {
            city: "Москва",
            address: "ул. Долгоруковская д. 19 стр. 7",
            infoWindowData: {
                "address": "ул. Долгоруковская д. 19 стр. 7",
                "email": "info@lambicbar.ru",
                "phone": "8 (495) 542-00-37",
                "workinghours": {
                    "weekdays": "Пн-Чт: с 10:00 до 23:00",
                    "weekend": "Пт-Вс: с 10:00 до 3:00"
                }
            }
        },
        {
            city: "Москва",
            address: "ул. Воронцово Поле д. 11/32",
            infoWindowData: {
                "address": "ул. Воронцово Поле д. 11/32",
                "email": "info@lambicbar.ru",
                "phone": "8 (495) 542-00-37",
                "workinghours": {
                    "weekdays": "Пн-Чт: с 10:00 до 23:00",
                    "weekend": "Пт-Вс: с 10:00 до 3:00"
                }
            }
        },
        {
            city: "Москва",
            address: "ул. Неверовскиго д. 10",
            infoWindowData: {
                "address": "ул. Неверовскиго д. 10",
                "email": "info@lambicbar.ru",
                "phone": "8 (495) 542-00-37",
                "workinghours": {
                    "weekdays": "Пн-Чт: с 10:00 до 23:00",
                    "weekend": "Пт-Вс: с 10:00 до 3:00"
                }
            }
        }
    ];

if ($("#restoranesMap").length) {
    $("#restoranesMap").googlemap({
        infoWindowTemplate: $("#infoWindowTemplate").html(),
        marker: {
            image: "/images/location-pin.png",
            size: {
                width: 50,
                height: 64
            }
        },
        center: {
            lat: 55.7558298,
            lng: 37.6199637
        },
        places: placesfromserver
    });
}

//
// Перемешивание в футере
//

$(".footer-column--address").each(function () {
    var ul = $(this).find("ul"),
        li = ul.children("li");
    // console.log(li.length);
    ul.append(li.splice(Math.floor(Math.random() * li.length), 1)[0]);
});