// window.onload = loadScript;

// function loadScript() {
// 	var script = document.createElement('script');
// 	script.type = 'text/javascript';
// 	script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBbJ16uUP1tqA_-qsojvMCBV12V71rukHA&sensor=true&language=ru&' +
// 	'callback=initialize';
// 	document.body.appendChild(script);
// }

// var geocoder, map;

// function initialize() {
// 	geocoder = new google.maps.Geocoder();

// 	var styles = [
// 	{
// 		"featureType": "water",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "color": "#d3d3d3" }
// 		]
// 	},{
// 		"featureType": "transit",
// 		"stylers": [
// 		{ "color": "#808080" },
// 		{ "visibility": "off" }
// 		]
// 	},{
// 		"featureType": "road.highway",
// 		"elementType": "geometry.stroke",
// 		"stylers": [
// 		{ "visibility": "on" },
// 		{ "color": "#b3b3b3" }
// 		]
// 	},{
// 		"featureType": "road.highway",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "color": "#ffffff" }
// 		]
// 	},{
// 		"featureType": "road.local",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "visibility": "on" },
// 		{ "color": "#ffffff" },
// 		{ "weight": 1.8 }
// 		]
// 	},{
// 		"featureType": "road.local",
// 		"elementType": "geometry.stroke",
// 		"stylers": [
// 		{ "color": "#d7d7d7" }
// 		]
// 	},{
// 		"featureType": "poi",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "visibility": "on" },
// 		{ "color": "#ebebeb" }
// 		]
// 	},{
// 		"featureType": "administrative",
// 		"elementType": "geometry",
// 		"stylers": [
// 		{ "color": "#a7a7a7" }
// 		]
// 	},{
// 		"featureType": "road.arterial",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "color": "#ffffff" }
// 		]
// 	},{
// 		"featureType": "road.arterial",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "color": "#ffffff" }
// 		]
// 	},{
// 		"featureType": "landscape",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "visibility": "on" },
// 		{ "color": "#efefef" }
// 		]
// 	},{
// 		"featureType": "road",
// 		"elementType": "labels.text.fill",
// 		"stylers": [
// 		{ "color": "#696969" }
// 		]
// 	},{
// 		"featureType": "administrative",
// 		"elementType": "labels.text.fill",
// 		"stylers": [
// 		{ "visibility": "on" },
// 		{ "color": "#737373" }
// 		]
// 	},{
// 		"featureType": "poi",
// 		"elementType": "labels.icon",
// 		"stylers": [
// 		{ "visibility": "off" }
// 		]
// 	},{
// 		"featureType": "poi",
// 		"elementType": "labels",
// 		"stylers": [
// 		{ "visibility": "off" }
// 		]
// 	},{
// 		"featureType": "road.arterial",
// 		"elementType": "geometry.stroke",
// 		"stylers": [
// 		{ "color": "#d6d6d6" }
// 		]
// 	},{
// 		"featureType": "road",
// 		"elementType": "labels.icon",
// 		"stylers": [
// 		{ "visibility": "off" }
// 		]
// 	},{
// 	},{
// 		"featureType": "poi",
// 		"elementType": "geometry.fill",
// 		"stylers": [
// 		{ "color": "#dadada" }
// 		]
// 	}
// 	];

// 	var styledMap = new google.maps.StyledMapType(styles, {
// 		name: "Styled Map"
// 	});

// 	var mapOptions = {
// 		zoom: 12,
// 		zoomControl: true,
// 		scrollwheel: false,
// 		panControl: false,
// 		scaleControl: false
// 	};

// 	var mapPlaceholder = document.getElementById('map');

// 	if(mapPlaceholder) {
// 		customMap = new google.maps.Map(mapPlaceholder, mapOptions);
// 		customMap.mapTypes.set('map_style', styledMap);
// 		customMap.setMapTypeId('map_style');

// 		var address = mapPlaceholder.getAttribute("data-address") || false;

// 		if (address) {
// 			var addressArray = address.split(';');

// 			for (var key in addressArray) {
//     			var addressItem = addressArray[key];

//     			if (addressItem) {
//     				codeAddress(customMap, addressItem);
//     			}
// 			}

// 			var center = mapPlaceholder.getAttribute("data-center") || false;

// 			if (center) {
// 				customMap.setCenter(new google.maps.LatLng(center));
// 			}
// 		}
// 	}
// }

// function codeAddress(theMap, address) {
// 	geocoder.geocode( { 'address': address}, function(results, status) {
// 		if (status == google.maps.GeocoderStatus.OK) {

// 			var image = new google.maps.MarkerImage("/images/location-pin.png", null, null, null, new google.maps.Size(50, 64));
// 			var beachMarker = new google.maps.Marker({
// 				map: theMap,
// 				icon: image,
// 				position: results[0].geometry.location
// 			});

// 			var contentString = $(".restaurant:first").html();

// 			var  infowindow = new google.maps.InfoWindow({
//       			content: contentString
//   			});

// 			google.maps.event.addListener(beachMarker, 'click', function() {
// 				theMap.setZoom(16);
// 				theMap.setCenter(beachMarker.getPosition());
// 				infowindow.open(theMap,beachMarker);
// 			});

// 		} else {
// 			alert('Geocode was not successful for the following reason: ' + status);
// 		}
// 	});
// }

(function($) {
	"use strict";

	var API_KEY = "AIzaSyBbJ16uUP1tqA_-qsojvMCBV12V71rukHA",
		lang = "ru",
		sensor = "true",
		googleMapScriptUrl = "https://maps.googleapis.com/maps/api/js",
		options = {
			zoom: 12,
			zoomControl: true,
			scrollwheel: false,
			panControl: false,
			scaleControl: false
		},
		theMap = {},
		$map,
		geocoder,
		styles = [
			{
				"featureType": "water",
				"elementType": "geometry.fill",
				"stylers": [
				{ "color": "#d3d3d3" }
				]
			},{
				"featureType": "transit",
				"stylers": [
				{ "color": "#808080" },
				{ "visibility": "off" }
				]
			},{
				"featureType": "road.highway",
				"elementType": "geometry.stroke",
				"stylers": [
				{ "visibility": "on" },
				{ "color": "#b3b3b3" }
				]
			},{
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [
				{ "color": "#ffffff" }
				]
			},{
				"featureType": "road.local",
				"elementType": "geometry.fill",
				"stylers": [
				{ "visibility": "on" },
				{ "color": "#ffffff" },
				{ "weight": 1.8 }
				]
			},{
				"featureType": "road.local",
				"elementType": "geometry.stroke",
				"stylers": [
				{ "color": "#d7d7d7" }
				]
			},{
				"featureType": "poi",
				"elementType": "geometry.fill",
				"stylers": [
				{ "visibility": "on" },
				{ "color": "#ebebeb" }
				]
			},{
				"featureType": "administrative",
				"elementType": "geometry",
				"stylers": [
				{ "color": "#a7a7a7" }
				]
			},{
				"featureType": "road.arterial",
				"elementType": "geometry.fill",
				"stylers": [
				{ "color": "#ffffff" }
				]
			},{
				"featureType": "road.arterial",
				"elementType": "geometry.fill",
				"stylers": [
				{ "color": "#ffffff" }
				]
			},{
				"featureType": "landscape",
				"elementType": "geometry.fill",
				"stylers": [
				{ "visibility": "on" },
				{ "color": "#efefef" }
				]
			},{
				"featureType": "road",
				"elementType": "labels.text.fill",
				"stylers": [
				{ "color": "#696969" }
				]
			},{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
				{ "visibility": "on" },
				{ "color": "#737373" }
				]
			},{
				"featureType": "poi",
				"elementType": "labels.icon",
				"stylers": [
				{ "visibility": "off" }
				]
			},{
				"featureType": "poi",
				"elementType": "labels",
				"stylers": [
				{ "visibility": "off" }
				]
			},{
				"featureType": "road.arterial",
				"elementType": "geometry.stroke",
				"stylers": [
				{ "color": "#d6d6d6" }
				]
			},{
				"featureType": "road",
				"elementType": "labels.icon",
				"stylers": [
				{ "visibility": "off" }
				]
			},{
			},{
				"featureType": "poi",
				"elementType": "geometry.fill",
				"stylers": [
				{ "color": "#dadada" }
				]
			}
			];;

	function initialize () {
		initMap();

		if (options.places) {
			addPlaces(options.places);
		}
		else {
			addPlace($map.data("address"));
		}

	}

	function loadGoogleMapScript() {
		var script = document.createElement('script'),
			getOptions = [
				'key=' + API_KEY,
				'sensor=' + sensor,
				'language=' + lang,
				'callback=initialize'
			].join('&');

		window.initialize = initialize;
		script.type = 'text/javascript';
		script.src = googleMapScriptUrl + '?' + getOptions;

		document.body.appendChild(script);
	}

	function initMap() {
		var styledMap = getStylesMap(styles);

		if($map[0]) {
			theMap = new google.maps.Map($map[0], options);
			theMap.mapTypes.set('map_style', styledMap);
			theMap.setMapTypeId('map_style');

			geocoder = new google.maps.Geocoder();

			if (options.center) {
				setCenter(options.center);
			}
		}

	}

	function setCenter(center) {
		theMap.setCenter(new google.maps.LatLng(center.lat, center.lng));
	}

	function setMarker(location) {
		var marker = new google.maps.Marker({
				map: theMap,
				icon: getMarkerImage(),
				position: location
			});

		return marker;
	}

	function getMarkerImage() {
		var image = new google.maps.MarkerImage(
				options.marker.image,
				new google.maps.Size(50, 64)
			);

		return image;
	}

	function addPlaces(places) {
		_.each(places, function (place) {
			addPlace(place.city + ", " + place.address, place.infoWindowData);
		});
	}

	function addPlace(address, infoWindowData) {
		geocoder.geocode( {'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var marker = setMarker(results[0].geometry.location);

				if (!options.center) {
					theMap.setCenter(marker.getPosition());
				}

				function onClickPlace() {
					// theMap.setZoom(16);
					// theMap.setCenter(marker.getPosition());

					if (infoWindowData) {
						var infoWindow = getInfoWindow(infoWindowData);
						infoWindow.open(theMap, marker);
					}
				}

				google.maps.event.addListener(marker, 'click', onClickPlace);
			}
		});
	}

	function getInfoWindow(data) {
		var infoWindow = new google.maps.InfoWindow({
  				content: getInfoWindowHtml(data)
			});

		return infoWindow;
	}

	function getInfoWindowHtml(data) {
		var template = _.template(options.infoWindowTemplate);

		return template(data);
	}

	function getStylesMap(styles) {
		var styledMap = new google.maps.StyledMapType(styles, {
			name: "Styled Map"
		});

		return styledMap;
	}

	$.fn.googlemap = function(userOptions) {
      	$map = $(this);
      	options = $.extend(options, userOptions) || {};

      	loadGoogleMapScript();

    	return this;
	};

})(jQuery);
