(function () {
	"use strict";

	var FileInput = (function (el) {
		var $el = $(el),
			$openBtn,
			$input,
			$fileNameText,
			$fileName,
			$resetBtn,
			$requirements;

		var init = function () {
			applyHtmlTemplate();
			bindEvents();
		};

		var applyHtmlTemplate = function () {

			var elementsTemplate =
				'<div class="file-name">\
                    <i class="icon-photo"></i>\
                    <span class="file-name-text"></span>\
                    <button type="button" class="file-reset icon-close"></button>\
                </div>\
                <button class="file-btn" type="button"></button>',
	            requirements = $el.data("requirements") || false;

			$el.removeClass("file-input").addClass("file-input-hidden").wrap("<div class='file-input'></div>").parent().prepend(elementsTemplate);
			$el.parent(".file-input").find(".file-btn").text($el.data("title") || "Загрузить");

			if(requirements) {
				$el.parent(".file-input").find(".file-name").after("<p class='file-requirements'>" + requirements + "</p>");
			}

		};

		var showFinderWindow = function () {
			$input.trigger("click");

			return false;
		};

		var setFile = function () {
			$openBtn.hide();
			$requirements.hide();

			$fileNameText.text(getFileName());
			$fileName.show();
		};

		var resetFile = function () {
			$input.replaceWith($input.clone(true));
			$input.one("change", setFile);

			$openBtn.show();
	        $requirements.show();
			$fileName.hide();

			return false;
		};

		var getFileName = function () {
			return $input[0].files[0].name || "";
		};

		var getValue = function () {
			return $input.val();
		};

		var isEmpty = function () {
			var file = getValue();

			if (file !== "" || file !== -1) {
				return false;
			}
			else {
				return true;
			}
		};

		var bindEvents = function () {
			var $fileInput = $el.parent(".file-input");
			$openBtn = $fileInput.find(".file-btn");
			$input = $fileInput.find(".file-input-hidden");
			$fileNameText = $fileInput.find(".file-name-text");
			$fileName = $fileInput.find(".file-name");
			$resetBtn = $fileInput.find(".file-reset");
	        $requirements = $fileInput.find(".file-requirements");

			$openBtn.on("click", showFinderWindow);
			$input.one("change", setFile);
			$resetBtn.on("click", resetFile);
		};

		init();

	});

	function autoInit() {
	    var $psFiles = $(".file-input");

	    $psFiles.each(function () {
	        new FileInput(this);
	    });
	}

	autoInit();
})();