<?php
/**
 * Маршрутизация по адресам узлов
 */
class CatalogRoute extends CakeRoute
{
    public function parse($url)
    {
        $params = parent::parse($url);
        if (empty($params['pass'])) {
            return false;
        } elseif (in_array($params['pass']['0'], array('menu', 'beer'))) {
            return $params;
        } else {
            return false;
        }
    }
}
