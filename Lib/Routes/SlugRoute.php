<?php
/**
 * Маршрутизация по адресам узлов
 */
class SlugRoute extends CakeRoute
{
    public function parse($url)
    {
        $params = parent::parse($url);
        if (empty($params)) {
            return false;
        }
        $pass = $params['pass'];

        $url = '/';
        if (!empty($pass)) {
            $url = '/' . implode("/", $pass);
        }

        App::import('Model', 'Node');
        $Node = new Node();
        $node = $Node->find('first', array(
            'conditions' => array(
                'Node.slug' => $url,
                'Node.node_state' => 'published',
            ),
            'fields'     => array('id', 'slug'),
            'recursive'  => -1
        ));
        if (!empty($node)) {
            $pass = array('0' => $node['Node']['id']);
            $params['pass'] = $pass;
            return $params;
        }

        return false;
    }
}
