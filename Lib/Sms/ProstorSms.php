<?php
class ProstorSms
{

    protected $login;
    protected $pass;
    protected $template;
    protected $layout;
    protected $viewVars;
    protected $phones;

    public $results;

    public function __construct()
    {
        $this->applyConfig();
        $this->template = 'default';
        $this->layout = 'default';
        $this->viewVars = array();
        $this->results = null;
    }

    private function applyConfig()
    {
        $config = Configure::read('ProstorSms');
        $this->login = $config['login'];
        $this->pass = $config['pass'];
    }

/**
 * Отправить сообщение
 */
    public function send()
    {
        $this->results = null;
        if (!empty($this->phones)) {
            $View = new View;
            $View->viewVars = $this->viewVars;
            $View->viewPath = 'Sms';
            $View->layoutPath = 'Sms';
            $text = $View->render($this->template, $this->layout);
            if (!empty($text)) {
                foreach ($this->phones as $phone) {
                    $url = 'http://api.prostor-sms.ru/messages/v2/send/'
                        . "?phone={$phone}"
                        . "&text=" . urlencode($text)
                        . "&login={$this->login}"
                        . "&password={$this->pass}";
                    $this->results[$phone] = file_get_contents($url);
                }
            }
        }

        return $this;
    }

/**
 * Установить шаблон отображения
 */
    public function template($template = false, $layout = false)
    {
        if (false === $template) {
            return array(
                'template' => $this->template,
                'layout' => $this->layout,
            );
        }
        $this->template = $template;
        if (false !== $layout) {
            $this->layout = $layout;
        }
        return $this;
    }

/**
 * Установить переменные для отображения
 */
    public function viewVars($viewVars = null)
    {
        if (null === $viewVars) {
            return $this->viewVars;
        }
        $this->viewVars = array_merge($this->viewVars, (array) $viewVars);
        return $this;
    }

/**
 * Установить телефоны для отправки сообщений
 */
    public function phones($phones = null)
    {
        if (null === $phones) {
            return $this->phones;
        }
        if (!is_array($phones)) {
           $phones = array($phones);
        }

        array_walk($phones, function (&$phone) {
            if (!is_string($phone)) {
                $phone = '';
            } else {
                $phone = preg_replace('/[^0-9\+]/', '', $phone);
            }
        });

        $phones = array_filter($phones, function ($phone) {
            $phone = trim($phone);
            return (!empty($phone));
        });

        $this->phones = $phones;

        return $this;
    }

}
