<?php
App::uses('AppController', 'Controller');
/**
 * Контроллер пунктов навигационного меню
 */
class MenuItemsController extends AppController
{

    public $uses = array('MenuItem', 'Node');

    // разграничение доступа
    public $permissions = array(
        'admin_edit'     => array('editor'),
        'admin_add'      => array('editor'),
        'admin_save'     => array('editor'),
        'admin_delete'   => array('editor'),
        'admin_reorder'  => array('editor'),
    );

/**
 * Редактирование
 */
    public function admin_edit($id = null)
    {
        if (!$this->MenuItem->exists($id)) {
            throw new NotFoundException("Не найден пункт меню для редактирования: {$id}");
        }
        $options = array('conditions' => array('MenuItem.' . $this->MenuItem->primaryKey => $id));
        $this->request->data = $this->MenuItem->find('first', $options);

        // список для выбора целевого узла
        $nodes = $this->MenuItem->nodes();
        $this->set(compact('nodes'));
    }

/**
 * Сохранение
 */
    public function admin_save()
    {
        if ($this->request->is(array('post', 'put'))) {
            if ($this->MenuItem->save($this->request->data, array('atomic' => false, 'deep' => true))) {
                $this->Session->setFlash('Пункт меню сохранен.', 'admin/flash_success');
            } else {
                $this->Session->setFlash('Не удалось обновить пункт меню.', 'admin/flash_error');
            }
        }
        return $this->redirect($this->referer());
    }

/**
 * Удаление
 */
    public function admin_delete($id = null)
    {
        $item = $this->MenuItem->find('first', array(
            'conditions' => array('MenuItem.id' => $id)
        ));
        if (empty($item)) {
            throw new NotFoundException("Не найден пункт меню для удаления: {$id}");
        }
        if ($this->MenuItem->delete($id)) {
            $this->Session->setFlash('Пункт меню удален.', 'admin/flash_success');
        } else {
            $this->Session->setFlash('Не удалось удалить пункт меню.', 'admin/flash_error');
        }

        // переход на страницу редактирования меню
        return $this->redirect(array('controller' => 'menus', 'action' => 'edit', $item['MenuItem']['menu_id']));
    }

/**
 * Новый порядок
 */
    public function admin_reorder()
    {
        $this->autoRender = false;
        $this->layout = null;
        $this->MenuItem->reorder(json_decode($this->data['order'], true));
        return true;
    }
}
