<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{

    // разграничение доступа
    public $permissions = array(
        'admin_profile' => '*',
    );
/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('logout');
    }

/**
 * index method
 *
 * @return void
 */
    public function admin_index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->User->find('all'));
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

/**
 * add method
 *
 * @return void
 */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Пользователь добавлен.', 'admin/flash_success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Не удалось добавить пользователя.', 'admin/flash_error');
            }
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Данные пользователя сохранены.', 'admin/flash_success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Не удалось сохранить данные пользователя.', 'admin/flash_error');
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash('Пользователь удален.', 'admin/flash_success');
        } else {
            $this->Session->setFlash('Не удалось удалить пользователя.', 'admin/flash_error');
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function login()
    {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Session->setFlash("Неверный логин или пароль!", 'admin/flash_error');
        }
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function admin_profile()
    {
        $id = $this->Auth->user('id');
        if (!$this->User->exists($id)) {
            throw new NotFoundException("Не найдена учетная запись: {$id}");
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Данные пользователя сохранены.', 'admin/flash_success');
            } else {
                $this->Session->setFlash('Не удалось сохранить данные пользователя.', 'admin/flash_error');
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }
}
