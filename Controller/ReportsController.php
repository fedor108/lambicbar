<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Reports Controller
 *
 * @property Report $Report
 * @property PaginatorComponent $Paginator
 */
class ReportsController extends AppController
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
    public function admin_index() {
        $this->Report->recursive = 0;
        $this->Paginator->settings = array('Report' => array(
            'order' => array('Report.id' => 'DESC'),
            'limit' => 10
        ));
        $this->set('reports', $this->Paginator->paginate('Report'));
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        if ($this->request->is('ajax')) {
            $this->response->type('json');
            $this->request->data['Report']['ip'] = $_SERVER['REMOTE_ADDR'];
            $this->Report->create();
            if ($this->Report->save($this->request->data)) {
                $this->email();
                $this->response->statusCode(200);
                $this->response->body(json_encode(
                    array('status' => 'OK', 'message' => 'Спасибо за отзыв')));
                $this->response->send();
                $this->_stop();
            } else {
                $this->response->statusCode(500);
                $this->response->body(json_encode(
                    array('status' => 'ERROR', 'message' => 'Не удалось отправить отзыв')));
                $this->response->send();
                $this->_stop();
            }
        } else {
            throw new NotFoundException();
        }
    }

    private function email()
    {
        $to = $this->Session->read('Setting.feedback_emails.value');
        $to = str_replace(' ', '', $to);
        $to = explode(',', $to);
        $from = 'no-reply@' . $_SERVER['SERVER_NAME'];
        $subject = 'Отзыв с сайта ' . $_SERVER['SERVER_NAME'];
        $Email = new CakeEmail();
        $Email->template('reports', null)
            ->subject($subject)
            ->emailFormat('text')
            ->to($to)
            ->from($from)
            ->viewVars($this->request->data);

        if (!empty($this->request->data['Report']['upload']['name']) &&
            empty($this->request->data['Report']['upload']['error'])
        ) {
            $Email->attachments(array(
                $this->request->data['Report']['upload']['name'] => $this->request->data['Report']['upload']['tmp_name'])
            );
        }

        $Email->send();
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->Report->id = $id;
        if (!$this->Report->exists()) {
            throw new NotFoundException("Не найден отзыв для удаления: {$id}");
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Report->delete()) {
            $this->Session->setFlash('Отзыв удален.', 'admin/flash_success');
        } else {
            $this->Session->setFlash('Не удалось удалить отзыв.', 'admin/flash_error');;
        }
        return $this->redirect(array('action' => 'index'));
    }
}
