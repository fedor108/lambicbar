<?php
App::uses('ConnectionManager', 'Manager');
App::uses('AppController', 'Controller');

/**
 * Контроллер для отображения узлов в публичной части сайта
 */
class PublicController extends AppController
{

    public $uses = array('Node', 'Seo');

    public $components = array('Paginator');

    public $layout = 'public';

    /**
     * Отображение узлов по id
     */
    public function node($id)
    {
        $node = $this->Node->find('first', array(
            'conditions' => array('Node.id' => $id),
            'contain' => array(
                'Text',
                'Line',
                'Flag',
                'Gallery',
                'Gallery.Image',
                'ParentNode',
                'ParentNode.ChildNode',
                'ChildNode',
                'Seo'
            )
        ));
        if (empty($node)) {
            throw new NotFoundException();
        }

        // переход к первому потомку, если установлен флаг
        if (!empty($node['named']['Flag']['use_first_child'])) {
            foreach ($node['ChildNode'] as $child) {
                if (!empty($child['slug'])) {
                    $this->redirect($child['slug']);
                }
            }
        }

        // список потомков
        if ('address' == $node['Node']['name']) {
            $children = $this->Node->find('all', array(
                'conditions' => array(
                    'Node.parent_id' => $node['Node']['id'],
                    'Node.node_state' => 'published',
                ),
                'contain' => array('Text', 'Line', 'Flag', 'Gallery', 'Gallery.Image', 'Link'),
            ));
            $this->set(compact('children'));
        }

        // список потомков для событий
        if ('events' == $node['Node']['name']) {

            // последнее событие для баннера
            $last_event = $this->Node->find('first', array(
                'conditions' => array(
                    'Node.parent_id' => $node['Node']['id'],
                    'Node.node_state' => 'published',
                ),
                'contain' => array('Text', 'Gallery', 'Gallery.Image'),
                'order' => 'Node.event_from DESC',

            ));

            // остальные события для постраничного вывода
            $this->Paginator->settings = array(
                'conditions' => array(
                    'Node.parent_id' => $node['Node']['id'],
                    'Node.node_state' => 'published',
                    'Node.id !=' => $last_event['Node']['id']
                ),
                'contain' => array('Text'),
                'order' => 'Node.event_from DESC',
                'limit' => 4
            );
            $children = $this->Paginator->paginate('Node');
            $nextPage = $this->params['paging']['Node']['nextPage'];
            $this->set(compact('children', 'last_event', 'nextPage'));
        }

        $path = $this->Node->getNodePath($id);
        $this->set(compact('node', 'path'));
        $this->render($this->getNodeView($node));
    }

    /**
     * Подбор отображения
     */
    private function getNodeView($node)
    {
        $views = array();
        $views[] = 'node_' . $node['Node']['name'];
        $views[] = 'node_type_' . $node['Node']['node_type'];
        $views[] = 'node_parent_' . $node['ParentNode']['name'];
        $views[] = 'node';
        foreach ($views as $view) {
            $file = APP . 'View' . DS . 'Public' . DS . $view . '.ctp';
            if (file_exists($file)) {
                $this->set(compact('view'));
                return $view;
            }
        }
    }

    /**
     * Список адресов для футера
     */
    public function address()
    {
        if (empty($this->request->params['requested'])) {
            throw new ForbiddenException();
        }
        $this->Node->recursive = -1;
        $address = $this->Node->findByName('address');
        $nodes = $this->Node->find('all', array(
            'conditions' => array(
                'Node.parent_id' => $address['Node']['id'],
                'Node.node_state' => 'published'
            ),
            'contain' => array('Text')
        ));
        return $nodes;
    }

    /**
     * Список для смс
     */
    public function getPhones($place = null)
    {
        if ($place !== null) {
            $db = ConnectionManager::getDataSource('default');
            $node = $db->query('SELECT `value` FROM `lines` WHERE `node_id` = (SELECT `node_id` FROM `texts` WHERE 
            `value` = "' . $place . '") AND `name` = "sms"');
            return $node[0]['lines']['value'];
        } else {
            return null;
        }
    }

    /**
     * Сквозные данные
     */
    public function mainpage()
    {
        if (empty($this->request->params['requested'])) {
            throw new ForbiddenException();
        }
        $this->Node->contain('Line');
        $mainpage = $this->Node->findByName('mainpage');
        return $mainpage;
    }

    /**
     * Раздел меню
     */
    public function catalog($parent_name = 'menu', $section_name = null, $place_name = null)
    {
        // узел раздела Адреса
        $address = $this->Node->find('first', array(
            'conditions' => array('Node.name' => 'address'),
            'contain' => array('ChildNode' => array(
                'conditions' => array('ChildNode.node_state' => 'published')
            )),
        ));
        if (empty($address)) {
            throw new NotFoundException("Не обнаружен узел Адреса.");
        }
        if (empty($address['ChildNode'])) {
            throw new NotFoundException("Не обнаружены узлы ресторанов.");
        }

        // узел раздела каталога Меню или Пиво
        $menu = $this->Node->find('first', array(
            'conditions' => array('Node.name' => $parent_name),
            'contain' => array('ChildNode', 'ChildNode.Connection'),
        ));
        if (empty($menu)) {
            throw new NotFoundException("Не обнаружен раздел {$parent_name}.");
        }
        if (empty($menu['ChildNode'])) {
            throw new NotFoundException("Не обнаружены разделы в {$parent_name}.");
        }

        if (empty($section_name)) {
            // перешли по адресу /menu - не указан раздел меню и ресторан
            // ищем первый доступный раздел меню и ресторан
            $this->redirectToSection($menu, $address);

        } elseif (empty($place_name)) {
            // перешли по адресу /{$parent_name}/{$section_name} - не указан ресторан
            // ищем ресторан для указанного раздела меню
            $this->redirectToPlace($menu, $address, $section_name);

        }

        // перешли по адресу /{$parent_name}/{$section_name}/{$place_name}

        // узел текущего раздела меню
        $node = $this->Node->find('first', array(
            'conditions' => array(
                'Node.name' => $section_name,
                'Node.parent_id' => $menu['Node']['id']
            ),
            'contain' => array('ParentNode', 'Connection', 'Text')
        ));
        if (empty($node)) {
            throw new NotFoundException("Не найден узел: {$section_name}");
        }

        // список id ресторанов, в которых доступен текущий разде меню
        // $connected = Hash::extract($node['Connection'], '{n}.term_id');
        // это теперь есть сразу в $node['connected']

        // узел текущего ресторана
        $place = $this->Node->find('first', array(
            'conditions' => array(
                'Node.node_state' => 'published',
                'Node.name' => $place_name,
                'Node.parent_id' => $address['Node']['id']
            ),
            'contain' => array('Connection')
        ));
        if (empty($place)) {
            throw new NotFoundException("Не найден ресторан: {$place_name}");
        }
        if (!in_array($place['Node']['id'], $node['connected'])) {
            throw new NotFoundException("Раздел меню {$section_name} не доступен в ресторане {$place_name}");
        }

        // разделы меню и продукты доступные в текущем ресторане
        $available = $this->Node->Connection->connectedNodes($place['Node']['id']);

        // список разделов меню доступных в текущем ресторане - для навигации
        $sections = array();
        foreach ($menu['ChildNode'] as $child) {
            if (in_array($child['id'], $available)) {
                $sections[] = $child;
            }
        }

        // узлы продуктов или подразделов
        $children = $this->Node->find('all', array(
            'conditions' => array(
                'Node.parent_id' => $node['Node']['id'],
                'Node.node_state' => 'published',
                'Node.id' => $available
            ),
            'contain' => array('Text', 'Line', 'Flag', 'Gallery', 'Gallery.Image', 'Price', 'Beer', 'Connection', 'Connection.Price')
        ));
        if (empty($children)) {
            throw new NotFoundException("Не найдены продукты в разделе меню: {$section_name}, для ресторана: {$place_name}");
        }

        // определяем подразделы текущего раздела меню /munu/section/sub_section
        $sub_sections = array();
        foreach ($children as $n => $child) {
            if (($child['Node']['rght'] - $child['Node']['lft']) > 1) {
                // продукты для подраздела меню
                $child['children'] = $this->Node->find('all', array(
                    'conditions' => array(
                        'Node.parent_id' => $child['Node']['id'],
                        'Node.node_state' => 'published',
                        'Node.id' => $available
                    ),
                    'contain' => array('Text', 'Line', 'Flag', 'Gallery', 'Gallery.Image', 'Price', 'Beer', 'Connection', 'Connection.Price'),
                ));
                if (!empty($child['children'])) {
                    $sub_sections[] = $child;
                    unset($children[$n]);
                }
            }
        }

        // список ссылок для выбора ресторана
        $places = $this->places($address, $menu, $node);

        // для опредления title
        $path = $this->Node->getNodePath($node['Node']['id']);

        $this->set(compact('places', 'place', 'children', 'node', 'path', 'sections', 'sub_sections'));

        $this->render("catalog_{$parent_name}");
    }

    /**
     * Перешли по адресу /menu или по адресу /menu/place:{name}
     * Раздел меню не указан
     * Ищем первый доступный раздел меню (и ресторан, если не указан)
     */
    private function redirectToSection($menu, $address)
    {
        if (!empty($this->params['named']['place'])) {
            // ресторан указан через именной параметр /menu/place:{place_name}
            // это переход со страницы рестораны
            // ищем первый раздел меню, доступный в данном ресторане

            // определяем id ресторана
            $place_id = null;
            $place_name = $this->params['named']['place'];
            foreach ($address['ChildNode'] as $child) {
                if ($child['name'] == $place_name) {
                    $place_id = $child['id'];
                    break;
                }
            }
            if (empty($place_id)) {
                throw new NotFoundException("Не обнаружен ресторан: {$place_name}.");
            }

            // доступные в ресторане разделы и продукты
            $available = $this->Node->Connection->connectedNodes($place_id);

            // первый доступный в ресторане раздел меню
            $section_name = '';
            foreach ($menu['ChildNode'] as $section) {
                if (in_array($section['id'], $available)) {
                    $section_name = $section['name'];
                    break;
                }
            }
            if (empty($section_name)) {
                $this->redirect("/{$menu['Node']['name']}");
                // throw new NotFoundException("Не обнаружено доступных разделов меню для ресторана: {$place_name}.");
            }

            return $this->redirect("/{$menu['Node']['name']}/{$section_name}/{$place_name}");

        } else {
            $place_name = '';
            // перебираем разделы меню
            foreach ($menu['ChildNode'] as $section) {
                // список id ресторанов, в которых доступен раздел меню $section
                $connected = array_filter($section['Connection'], function ($connection) {
                    return $connection['active'];
                });
                $connected = Hash::extract($connected, '{n}.term_id');

                // случайный порядок ресторанов
                shuffle($connected);

                // первый ресторан, в котором доступен раздел меню $section
                $place = $this->Node->find('first', array(
                    'conditions' => array(
                        'Node.parent_id' => $address['Node']['id'],
                        'Node.node_state' => 'published',
                        'Node.id' => $connected,
                    ),
                    'recursive' => -1,
                    'order' => 'rand()',
                ));
                if (!empty($place)) {
                    $section_name = $section['name'];
                    $place_name = $place['Node']['name'];
                    break;
                }
            }
            if (empty($place_name)) {
                throw new NotFoundException("Не обнаружено доступных разделов меню.");
            }

            return $this->redirect("/{$menu['Node']['name']}/{$section_name}/{$place_name}");
        }

    }

    /**
     * Перешли по адресу /{$parent_name}/{$section_name} - не указан ресторан
     * ищем ресторан для указанного раздела меню
     */
    private function redirectToPlace($menu, $address, $section_name)
    {
        foreach ($menu['ChildNode'] as $section) {
            if ($section['name'] == $section_name) {
                // список id ресторанов, в которых доступен раздел меню $section
                $connected = array_filter($section['Connection'], function ($connection) {
                    return $connection['active'];
                });
                $connected = Hash::extract($connected, '{n}.term_id');

                // случайный порядок, для автоматического перехода к разным ресторанам
                shuffle($connected);

                // первый ресторан, в котором доступен раздел меню $section_name
                $place = $this->Node->find('first', array(
                    'conditions' => array(
                        'Node.parent_id' => $address['Node']['id'],
                        'Node.node_state' => 'published',
                        'Node.id' => $connected,
                    ),
                    'recursive' => -1,
                    'order' => 'rand()',
                ));

                break;
            }
        }

        if (empty($place)) {
            throw new NotFoundException("Раздел меню {$section_name} - {$section['title']} не доступен ни в одном из ресторанов.");
        }

        return $this->redirect("/{$menu['Node']['name']}/{$section_name}/{$place['Node']['name']}");
    }

    /**
     * Список ссылок для выбора ресторана
     * Ссылки на первый доступный в ресторане раздел меню
     * $address - узел раздела Адреса /address
     * $menu - узел раздела каталога Меню или Пиво
     * $section - узел текущего раздела меню
     */
    private function places($address, $menu, $section)
    {
        $places = array();
        foreach ($address['ChildNode'] as $child) {
            if (in_array($child['id'], $section['connected'])) {
                // ссылка на текущий раздел меню
                $places[] = array(
                    'url' => $section['Node']['slug'] . "/" . $child['name'],
                    'title' => $child['title']
                );
            } else {
                $available = $this->Node->Connection->connectedNodes($child['id']);
                if (!empty($available)) {
                    $first_section = $this->Node->find('first', array(
                        'conditions' => array(
                            'Node.parent_id' => $menu['Node']['id'],
                            'Node.id' => $available
                        ),
                        'recursive' => -1
                    ));
                    if (!empty($first_section)) {
                        // ссылка на первый доступный в ресторане раздел меню
                        $places[] = array(
                            'url' => $first_section['Node']['slug'] . "/" . $child['name'],
                            'title' => $child['title']
                        );
                    }
                }
            }
        }
        return $places;
    }

    /**
     * Xml карта сайта
     */
    public function sitemap()
    {
        $nodes = $this->Node->find('all', array(
            'conditions' => array(
                'Node.node_state' => 'published',
                'Node.node_type' => array('page', 'post', 'department', 'event'),
                'Node.name !=' => array('sitemap'),
            ),
            'fields' => array('id', 'name', 'title', 'slug', 'modified')
        ));
        $this->layout = false;
        $this->response->type('xml');
        $this->set(compact('nodes'));
    }

}
