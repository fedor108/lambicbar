<?php
App::uses('AppController', 'Controller');
App::uses('PublicController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('ProstorSms', 'Lib/Sms');

/**
 * Orders Controller
 *
 * @property Order $Order
 * @property PaginatorComponent $Paginator
 */
class OrdersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->Order->recursive = 0;
        $this->Paginator->settings = array('Order' => array(
            'order' => array('Order.id' => 'DESC'),
            'limit' => 10
        ));
        $this->set('orders', $this->Paginator->paginate('Order'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('ajax')) {
            $this->response->type('json');
            $this->request->data['Order']['ip'] = $_SERVER['REMOTE_ADDR'];
            $this->Order->create();

            if ($this->Order->save($this->request->data)) {
                $this->request->data['Order']['id'] = $this->Order->id;

                $message = 'Ваша бронь принята. Мы свяжемся с вами для подтверждения резерва.';
                $status = 'OK';
                // отправить сообщение администратору по почте
                $this->email();

                // отправить SMS сообщение администратору
                $sms_results = $this->sms($this->request->data['Order']['address']);
                // отправить SMS сообщение клиенту
                $client_sms_result = $this->clientSms($message);

                // ответ скрипту
                $data = $this->request->data;
                $this->response->statusCode(200);
                $this->response->body(json_encode(compact(
                    'status',
                    'message',
                    'sms_results',
                    'client_sms_result',
                    'data'
                )));
                $this->response->send();
                $this->_stop();
            } else {
                $status = 'ERROR';
                $message = 'Не удалось отправить заказ';
                $data = $this->request->data;
                $this->response->statusCode(500);
                $this->response->body(json_encode(compact(
                    'status',
                    'message',
                    'data'
                )));
                $this->response->send();
                $this->_stop();
            }
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Отправить SMS администратору
     */
    private function sms($place)
    {
        $publicController = new PublicController();
        $sms = new ProstorSms;
        $phones = array_map(function ($item) {
            return trim($item);
        }, explode(',', ($publicController->getPhones($place))));
        $sms->phones($phones)
            ->template('orders')
            ->viewVars(array('data' => $this->request->data['Order'], 'title' => 'Бронирование столика'))
            ->send();

        return $sms->results;
    }

    /**
     * Отправить SMS клиенту
     */
    private function clientSms($text)
    {
        $sms = new ProstorSms;
        $sms->phones($this->request->data['Order']['phone'])
            ->template('orders')
            ->viewVars(array('data' => $this->request->data['Order'], 'title' => $text))
            ->send();

        return $sms->results;
    }

    private function email()
    {
        $to = $this->Session->read('Setting.order_emails.value');
        $to = str_replace(' ', '', $to);
        $to = explode(',', $to);
        $from = 'no-reply@' . $_SERVER['SERVER_NAME'];
        $subject = 'Бронирование столика ' . $_SERVER['SERVER_NAME'] . ' #' . $this->request->data['Order']['id'];
        $Email = new CakeEmail();
        $Email->template('orders', null)
            ->subject($subject)
            ->emailFormat('text')
            ->to($to)
            ->from($from)
            ->viewVars(array('data' => $this->request->data['Order'], 'title' => $subject));

        $Email->send();
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException("Не найдена бронь для удаления: {$id}");
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Order->delete()) {
            $this->Session->setFlash('Бронь удалена.', 'admin/flash_success');
        } else {
            $this->Session->setFlash('Не удалось удалить бронь.', 'admin/flash_error');;
        }
        return $this->redirect(array('action' => 'index'));
    }

}
