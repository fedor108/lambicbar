<?php
App::uses('AppController', 'Controller');
/**
 * Контроллер Меню
 *
 */
class MenusController extends AppController
{

    public $uses = array('Menu', 'Node');

    // разграничение доступа
    public $permissions = array(
        'admin_index'  => array('editor'),
        'admin_edit'   => array('editor'),
        'admin_add'    => array('editor'),
        'admin_save'   => array('editor'),
        'admin_delete' => array('editor'),
    );

/**
 * Список меню в админ-панели
 */
    public function admin_index()
    {
        $this->set('menus', $this->Menu->find('all'));
    }

/**
 * Страница добавления меню
 */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->Menu->create();
            if ($this->Menu->save($this->request->data)) {
                $this->Session->setFlash('Меню создано.', 'admin/flash_success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Не удалось создать меню.', 'admin/flash_error');
            }
        }
    }

/**
 * Страница редактирования меню
 */
    public function admin_edit($id = null)
    {
        $this->Menu->recursive = -1;
        $menu = $this->Menu->find('first', array('conditions' => array('Menu.id' => $id)));
        if (empty($menu)) {
            throw new NotFoundException("Не найдено меню для редактирования: {$id}");
        }
        // данные для отображения
        $this->set(compact('menu'));

        // данные для редактирования меню
        $this->request->data = $menu;

        // пункты меню
        $this->Menu->MenuItem->contain('Node');
        $data = $this->Menu->MenuItem->find('all', array(
            'conditions' => array('MenuItem.menu_id' => $menu['Menu']['id'])
        ));
        $items = Hash::nest($data);
        $this->set(compact('items'));

        // список для выбора целевого узла
        $nodes = $this->Menu->MenuItem->nodes();
        $this->set(compact('nodes'));
    }

/**
 * Сохранение
 */
    public function admin_save($id = null)
    {
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Menu->saveAssociated($this->request->data, array('atomic' => false, 'deep' => true))) {
                $this->Session->setFlash('Меню сохранено.', 'admin/flash_success');
            } else {
                $this->Session->setFlash('Не удалось сохранить меню.', 'admin/flash_error');
            }
        }
        return $this->redirect($this->referer());
    }

/**
 * Удаление
 */
    public function admin_delete($id = null)
    {
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            throw new NotFoundException("Не найдено меню для удаления: {$id}");
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Menu->delete()) {
            $this->Session->setFlash('Меню удалено.', 'admin/flash_success');
        } else {
            $this->Session->setFlash('Не удалось удалить меню.', 'admin/flash_error');
        }
        return $this->redirect(array('action' => 'index'));
    }

/**
 * Запрос пунктов меню меню по Menu.name - для requesAction()
 */
    public function items($name = null)
    {
        $this->autoRender = false;
        $this->layout = null;
        return $this->Menu->MenuItem->getTree($name);
    }

}

