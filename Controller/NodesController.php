<?php
App::uses('AppController', 'Controller');
/**
 * Nodes Controller
 *
 * @property Node $Node
 * @property PaginatorComponent $Paginator
 */
class NodesController extends AppController
{
/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');

    // разграничение доступа
    public $permissions = array(
        'admin_index'   => array('editor'),
        'admin_edit'    => array('editor'),
        'admin_options' => array('editor'),
        'admin_add'     => array('editor'),
        'admin_save'    => array('editor'),
        'admin_move'    => array('editor'),
        'admin_delete'  => array('editor'),
        'admin_tree'    => array('editor'),
        'admin_sort'    => array('editor'),
    );

    public function admin_index() {
        return $this->redirect(array('admin' => true, 'action' => 'edit', 1));
    }

/**
 * edit method
 *
 * @return void
 */
    public function admin_edit($id = null) {

        $this->Node->recursive = 2;
        if (empty($id)) {
            $node = $this->Node->find('first', array(
                'conditions' => array('Node.name' => 'mainpage'),
            ));
        } else {
            $node = $this->Node->find('first', array(
                'conditions' => array('Node.id' => $id),
            ));
        }
        if (empty($node)) {
            throw new NotFoundException('Узел не найден.');
        }

        $this->request->data = $node;

        $users      = $this->Node->User->find('list');
        $path       = $this->Node->getPath($node['Node']['id']);
        $nodeTypes  = $this->Node->types;
        $nodeStates = $this->Node->states;
        $targets    = $this->Node->Link->targets;
        $dictionaries = $this->Node->Dictionary->find('all', array(
            'conditions' => array('Dictionary.node_id' => Hash::extract($path, '{n}.Node.id')),
            'recursive'  => 1
        ));
        $this->set(compact('node', 'users', 'children', 'path', 'nodeTypes', 'nodeStates', 'targets', 'dictionaries', 'terms'));
    }

/**
 * options method
 *
 * @return void
 */
    public function admin_options($id = null) {
        $node = $this->Node->find('first', array(
            'conditions' => array('Node.id' => $id),
        ));
        if (empty($node)) {
            throw new NotFoundException('Узел не найден.');
        }

        $this->request->data = $node;

        $users     = $this->Node->User->find('list', array('fields' => array('id', 'username')));
        $path      = $this->Node->getPath($node['Node']['id']);
        $nodeTypes = $this->Node->types;
        $editors   = $this->Node->Text->editors;
        $items     = $this->Node->itemsForAdd($id, $node['Node']['node_type']);
        $dictionaries = $this->Node->find('all', array('conditions' => array('Node.is_dictionary' => 1), 'recursive' => -1));

        $this->set(compact('users', 'path', 'nodeTypes', 'node', 'editors', 'items', 'dictionaries'));
    }

/**
 * add method
 *
 * @return void
 */
    public function admin_add($parent_id) {
        $node = $this->Node->find('first', array(
            'conditions' => array('Node.id' => $parent_id),
        ));
        if (empty($node)) {
            throw new NotFoundException('Узел не найден.');
        }
        $path = $this->Node->getPath($node['Node']['id']);
        $nodeTypes = $this->Node->types;
        $this->set(compact('parentNodes', 'users', 'children', 'path', 'nodeTypes', 'node'));
    }

/**
 * save method
 *
 */
    public function admin_save() {
        if ($this->request->is(array('post', 'put'))) {
            if ($result = $this->Node->saveAssociated($this->request->data, array('atomic' => false, 'deep' => true))) {

                if (!empty($this->request->data['Node']['id'])) {
                    $this->Node->checkConnections($this->request->data['Node']['id']);
                }

                $message = 'Узел сохранен.';
                // если есть ошибки загрузки изображений, то добавляем их в сообщение
                if (!empty($this->Node->Gallery->Image->errors)){
                    foreach($this->Node->Gallery->Image->errors as $error){
                        $message .= '<br> ' . $error['filename'] . ' - ' . $error['message'];
                    }
                }

                $this->Session->setFlash($message, 'admin/flash_success');

            } else {
                $this->Session->setFlash('Не удалось сохранить узел.', 'admin/flash_error');
            }
        }

        if (strpos($this->referer(), 'nodes/add')) {
            return $this->redirect(array('action' => 'edit', $this->Node->id));
        } else {
            return $this->redirect($this->referer());
        }
    }

/**
 * move method
 *
 */
    public function admin_move($id, $parent_id) {
        $this->autoRender = false;
        if ($this->Node->save(array('id' => $id, 'parent_id' => $parent_id))) {
            $message = 'Узел перемещен.';
            $error  = 0;
        } else {
            $message = 'Не удалось переместить узел';
            $error = 1;
        }
        return json_encode(compact('message', 'error'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 */
    public function admin_delete($id = null) {
        $node = $this->Node->find('first', array(
            'conditions' => array('Node.id' => $id),
        ));
        if (empty($node)) {
            throw new NotFoundException('Узел не найден.');
        }

        if ($this->Node->delete($id)) {
            $this->Session->setFlash('Узел удален.', 'admin/flash_success');
        } else {
            $this->Session->setFlash('Не удалось удалит узел.', 'admin/flash_error');
        }

        if (!empty($node['Node']['parent_id'])) {
            return $this->redirect(array('action' => "edit", $node['Node']['parent_id']));
        } else {
            return $this->redirect(array('action' => "index"));
        }
    }

/**
 * tree method
 *
 * @return array
 */
    public function admin_tree() {
        $this->autoRender = false;
        return $this->Node->find('all', array(
            'conditions' => array('Node.children_count >' => 0),
            'fields'     => array('id', 'name', 'title', 'parent_id'),
            'recursive'  => -1
        ));
    }

/**
 * Сохранение порядка после перетаскивания узла в списке
 */
    public function admin_sort() {
        $this->autoRender = false;
        $this->Node->treeSort($this->request->data['sort']);
        return 1;
    }
}

/**
 *  ===============================================
 *  Далее методы публичной части без префикса admin
 *  ===============================================
 */
