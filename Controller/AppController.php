<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $uses = array('Setting');
    public $components = array(
        'DebugKit.Toolbar',
        'Session',
        'Auth' => array(
            'authorize' => array('Controller'),
            'loginRedirect' => array(
                'controller' => 'nodes',
                'action' => 'index',
                'admin'  => true
            ),
            'logoutRedirect' => "/"
        )
    );

    public $helpers = array(
        'Html' => array('className' => 'Bootstrap3.BootstrapHtml'),
        'Form' => array('className' => 'Bs3Helpers.Bs3Form'),
        'Modal' => array('className' => 'Bootstrap3.BootstrapModal')
    );

/**
 * Перед выполнением любых действий
 */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();
        if (!$this->isAuthorized($this->Auth->user())) {
            $this->redirect('/login');
        }
        if (!empty($this->params['admin'])) {
            $this->layout = 'admin';
        }
        $this->setSettings();
    }

/**
 * Проверка авторизации
 */
    public function isAuthorized($user = null)
    {
        if (empty($this->params['admin'])) {

            // не админ-панель
            return true;

        } elseif (!empty($user['Group']['name'])
            && ('admin' == $user['Group']['name'])
        ) {
            // админ-панель и пользователь из группы admin
            return true;
        }

        // Если в контроллере установлены параметры доступа
        if (!empty($this->permissions)) {

            if (empty($user['Group']['name'])) {
                $this->Session->setFlash("Нужно войти", 'admin/flash_error');
                return false;
            }

            if ($this->permissions[$this->action] == '*') {

                // текущее действие доступно всем
                return true;

            } elseif (!empty($this->permissions[$this->action])
                && in_array($user['Group']['name'], $this->permissions[$this->action])
            ) {
                // для группы пользователя установлено разрешение на выполнение действия
                return true;

            } else {
                // у группы нет доступа к этому действию
                $this->Session->setFlash("Группа {$user['Group']['name']} не имеет доступа к {$this->name}::{$this->action}", 'admin/flash_error');
                return false;
            }
        }

        // В других случаях - запрет доступа
        return false;
    }

/**
 * Запись настроек в сессию
 */
    private function setSettings()
    {
        $settings = $this->Setting->find('all');
        $settings = Hash::combine($settings, '{n}.Setting.name', '{n}.Setting');
        $this->Session->write('Setting', $settings);
    }

}
