<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller - Настройки сайта
 */
class SettingsController extends AppController
{

/**
 * Список настроек для редактирования
 */
    public function admin_index()
    {
        $data = $this->Setting->find('all');
        foreach ($data as $item) {
            $this->request->data['Setting'][] = $item['Setting'];
        }
    }

/**
 * Сохранение, удаление и создание
 */
    public function admin_save()
    {
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Setting->saveMany($this->request->data['Setting'], array('atomic' => false, 'deep' => true))) {
                $this->Session->setFlash('Настройки сохранены.', 'admin/flash_success');
            } else {
                $this->Session->setFlash('Не удалось сохранить настройки.', 'admin/flash_error');
            }
        }
        return $this->redirect(array('action' => 'index'));
    }
}
