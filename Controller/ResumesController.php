<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Resume Controller
 *
 */
class ResumesController extends AppController
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session');

    public function add() {
        if ($this->request->is('ajax')) {
            $this->response->type('json');

             //Объединяем вакансии в строку
            $positions = explode("\r\n", $this->Session->read('Setting.vacancy_positions.value'));
            $checked_positions = array();
            foreach($this->request->data['Resume']['position'] as $i => $value )
                $checked_positions[] = $positions[$i];
            $this->request->data['Resume']['position'] = implode(', ', $checked_positions);

            $this->request->data['Resume']['ip'] = $_SERVER['REMOTE_ADDR'];
            $this->email();
            $this->response->statusCode(200);
            $this->response->body(json_encode(
                array('status' => 'OK', 'message' => 'Ваша анкета отправлена')));
            $this->response->send();
            $this->_stop();
        } else {
            throw new NotFoundException();
        }
    }

    private function email()
    {
        $to = $this->Session->read('Setting.vacancy_emails.value');
        $to = str_replace(' ', '', $to);
        $to = explode(',', $to);
        $from = 'no-reply@' . $_SERVER['SERVER_NAME'];
        $subject = 'Анкета соискателя с сайта ' . $_SERVER['SERVER_NAME'];
        $Email = new CakeEmail();
        $Email->template('resumes', null)
            ->subject($subject)
            ->emailFormat('text')
            ->to($to)
            ->from($from)
            ->viewVars($this->request->data);

        $attachments = array();
        if (!empty($this->request->data['Resume']['image']['name']) &&
            empty($this->request->data['Resume']['image']['error'])
        ) {
            $attachments[ $this->request->data['Resume']['image']['name'] ] = $this->request->data['Resume']['image']['tmp_name'];
        }
        if (!empty($this->request->data['Resume']['document']['name']) &&
            empty($this->request->data['Resume']['document']['error'])
        ) {
            $attachments[ $this->request->data['Resume']['document']['name'] ] = $this->request->data['Resume']['document']['tmp_name'];
        }
        if (!empty($attachments)) {
            $Email->attachments( $attachments);
        }

        $Email->send();
    }
}