<?php
App::uses('AppHelper', 'View/Helper');
/**
 * Помощник отображения журнала изменений в админ-панели
 */
class LogHelper extends AppHelper
{

/**
 * Ссылка на страницу редактирования измененного объекта
 */
    public function target($item)
    {
        $alias = $item['Log']['model_name'];
        $action = $item['Log']['action'];
        $title = '';
        $url = '';

        if ('delete' == $action) {
            $data = unserialize($item['Log']['diff']);
        } else {
            $data = $item[$alias];
        }

        if ('Node' == $alias) {
            $url = "/admin/nodes/edit/{$data['id']}";
        } elseif ('Menu' == $alias) {
            $url = "/admin/menus/edit/{$data['id']}";
        } elseif ('MenuItem' == $alias) {
            $url = "/admin/menus/edit/{$data['menu_id']}";
        } elseif (!empty($data['node_id'])) {
            $url = "/admin/nodes/edit/{$data['node_id']}";
        } elseif ('Image' == $alias) {
            $url = ('delete' == $action) ? "#" : "/admin/nodes/edit/{$data['node_id']}";
        } else {
            $url = "#";
        }
        $title = $this->title($data, $item['Log']['model_label']);
        return compact('url', 'title');
    }

/**
 * Название измененного объекта
 */
    private function title($data, $label)
    {
        $title = "{$label}: ";
        if (!empty($data['title'])) {
            $title .= $data['title'];
        } elseif (!empty($data['label'])) {
            $title .= $data['label'];
        } else {
            $title .= "{$data['id']}";
        }
        return $title;
    }

/**
 * Русское название действия
 */
    public function action($name)
    {
        $actions = array(
            'create' => "Создан",
            'delete' => "Удален",
            'edit' => "Изменен"
        );
        if (array_key_exists($name, $actions)) {
            $result = $actions[$name];
        } else {
            $result = $name;
        }
        return $result;
    }
}
