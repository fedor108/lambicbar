<?php
App::uses('AppHelper', 'View/Helper');
/**
 * Помошник построения дерева узлов в админ-панели
 */
class TreeHelper extends AppHelper {
    public function create($current_node) {
        $nodes = $this->requestAction(array('admin' => true, 'controller' => 'nodes', 'action' => 'tree'));
        $nodes = Hash::combine($nodes, '{n}.Node.id', '{n}');

        $id = $current_node['Node']['id'];
        if (empty($nodes[$id])) {
            $id = $current_node['Node']['parent_id'];
        }

        if (!empty($nodes[$id])) {
            $nodes[$id]['active'] = true;
        }

        while (!empty($nodes[$id])) {
            $nodes[$id]['open'] = true;
            $nodes[$id]['display'] = true;
            foreach ($nodes as &$node) {
                if ($node['Node']['parent_id'] == $nodes[$id]['Node']['parent_id']) {
                    $node['display'] = true;
                }
                if (isset($nodes[$node['Node']['parent_id']]['open'])) {
                    $node['display'] = true;
                }
            }
            $id = $nodes[$id]['Node']['parent_id'];
        }

        $nodes = Hash::nest($nodes);
        return $nodes;
    }

    public function itemIcon($item) {
        $class = '';
        if (!empty($item['children'])) {
            $class = 'fa fa-lg fa-folder';
            if (isset($item['open'])) {
                $class = 'fa fa-lg fa-folder-open';
            }
        }
        if (empty($class)) {
            return null;

        } else {
            return "<i class=\"{$class}\"></i>";
        }
    }

    public function itemSpanClass($item) {
        $class = 'tree-item';
        if (!empty($item['active'])) {
            $class .= ' label label-success';
        }
        return $class;
    }
}
