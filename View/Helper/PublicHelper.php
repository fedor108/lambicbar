<?php
App::uses('AppHelper', 'View/Helper');
/**
 * Помошник для отображения публичной части сайта
 */
class PublicHelper extends AppHelper
{

/**
 * Подготовка пунктов меню для отображения
 */
    public function menuItems($items)
    {
        $result = array();
        foreach ($items as $item) {
            $result[] = array(
                'url' => $item['MenuItem']['url'],
                'text' => $item['MenuItem']['title'],
                'title' => $item['MenuItem']['link_title']
            );
        }
        return $result;
    }

/**
 * title страниц
 */
    public function title($path, $seo_title = NULL)
    {
        $title = '';

        foreach ($path as $item) {
            if ($title) {
                $title = ' | ' . $title;
            }

            if (!empty($item['Seo']['title'])) {
                $t = $item['Seo']['title'];
            } else {
                $t = $item['Node']['title'];
            }
            $title = $t . $title;
        }
        return $title;
    }

/**
 * Меню раздела, формируется из ссылок на опубликованные страницы раздела
 */
    public function contentMenu($childs)
    {
        $menu = array();
        foreach ($childs as $node) {
            if ('published' == $node['node_state']) {
                $menu[] = array(
                    'title' => $node['title'],
                    'slug' => $node['slug']
                );
            }
        }
        return $menu;
    }

/**
 * Меню раздела Меню, с учетом текущего ресторана
 */
    public function contentPlaceMenu($sections, $place)
    {
        $menu = array();
        foreach ($sections as $section) {
            if ('published' == $section['node_state']) {
                $menu[] = array(
                    'title' => $section['title'],
                    'slug' => "{$section['slug']}/{$place['Node']['name']}"
                );
            }
        }
        return $menu;
    }

/**
 * Подготовка к отображению данных ресторанов
 */
    public function restaurants($nodes)
    {
        $results = array();
        foreach ($nodes as $node) {
            $results[] = array(
                'address' => $node['named']['Text']['address']['value'],
                'email' => $node['named']['Line']['email']['value'],
                'phone' => $node['named']['Line']['phone']['value'],
                'schedule' => explode("\r\n", $node['named']['Text']['schedule']['value']),
                'veranda' => $node['named']['Flag']['veranda']['value'],
                'parking' => "https://maps.yandex.ru/?text=" . urlencode(trim($node['named']['Text']['address']['value']) . ", парковка"),
                'map' => "https://maps.yandex.ru/?text=" . urlencode(trim($node['named']['Text']['address']['value'])),
                'menu' => "/menu/place:{$node['Node']['name']}",
                'photos' => $node['named']['Gallery']['main']['Image'],
                'preview' => $node['named']['Gallery']['main']['Image'][0]['src'],
                'panorama' => $node['named']['Link']['panorama']
            );
        }
        return $results;
    }

/**
 * Определение актуальной цены продукта в зависимости от текущего ресторана
 */
    public function price($prices, $connections, $place_id)
    {
        foreach ($connections as $connection) {
            $result = $prices;
            if ($connection['term_id'] == $place_id) {
                if ($connection['active'] && !empty($connection['Price'])) {
                    $result = $connection['Price'];
                }
                break;
            }
        }
        return $result;
    }

/**
 * Русское отображение текущей даты
 */
    public function ruToday()
    {
        $date = date('j M Y');
        if (!$this->lang){
            $month = array("Jan"=>"Января", "Feb"=>"Февраля", "Mar"=>"Марта", "Apr"=>"Апреля",
                "May"=>"Мая", "Jun"=>"Июня", "Jul"=>"Июля", "Aug"=>"Августа", "Sep"=>"Сентября",
                "Oct"=>"Октября", "Nov"=>"Ноября", "Dec"=>"Декабря");
            $m = date('M');
            $date = str_replace($m, $month[$m], $date);
        }
        return $date;
    }

/**
 * Контакты ресторанов для карты в формате json
 */
    public function mapJson($address)
    {
        $map = array();
        foreach ($address as $id => $place) {
            $res['city'] = "Москва";
            $res['address'] = $place['address'];
            $res['infoWindowData'] = array(
                    "address" => $place['address'],
                    "email" => $place['email'],
                    "phone" => $place['phone'],
                    "workinghours" => array(
                            0 => $place['schedule'][0]
                        )
                );
            if(!empty($place['schedule'][1])) $res['infoWindowData']['workinghours'][1] = $place['schedule'][1];
            $map[] = $res;
        }
        return json_encode($map);
    }
}
