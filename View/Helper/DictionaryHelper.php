<?php
App::uses('AppHelper', 'View/Helper');
/**
 * Помошник для построения формы подключения справочников к узлу
 */
class DictionaryHelper extends AppHelper {

/**
 * Отмечаем уже подключенные к узлу справочники
 */
    public function dictionaries($dictionaries, $node) {
        $active = Hash::combine($node['Dictionary'], '{n}.dictionary_id', '{n}');
        foreach ($dictionaries as &$item) {
            if (in_array($item['Node']['id'], array_keys($active))) {
                $item['id'] = $active[$item['Node']['id']]['id'];
            }
        }
        return $dictionaries;
    }
}
