<?php

class ImageHelper extends AppHelper
{
/**
 * Делает миниатюры изображений, возвращает путь до миниатюры (относительный адрес) для вставки в html код
 *
 * @param string $src - Путь до изображения относительно каталога webroot.
 * @param string $type - Тип миниатюры.
 */
    public function thumb($src, $type)
    {
        //проверка существования файла изображения
        $srcFullName = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.$src;
		if( !file_exists($srcFullName) ){
            return $src;
        }

        //проверка формата изображения
        $tmp = explode('.', $src);
        $ext = end($tmp);
        $valid_ext = array('jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'); // разрешенные форматы изображения
        if( !in_array($ext, $valid_ext)){
            return $src;
        }

        //делаем путь до миниатюры
        $thumbName = $this->thumbnailName($src, $type); //имя файла миниатюры
        $thumbUrl = '/media/tmp/'.$thumbName; // относительный URL миниатюры
        $thumbFullName = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.$thumbUrl; // полное имя файла миниатюры

        //делаем миниатюру заданной ширины
        if ( !file_exists($thumbFullName) ) {
            if( $type == 'mid' ){
                $width = 300;
            }
            copy($srcFullName, $thumbFullName); //копируем картинку на место миниатюры
            $this->resizeImageToWidth($thumbFullName, $width);
            chmod($thumbFullName, 0777); //устанавливаем права, чтобы мог прочитать файл другой вебсервер (nginx, например)
        }

        return $thumbUrl;
    }

/**
 * Делает имя файла миниатюры
 *
 * @param string $src - Путь до изображения относительно каталога webroot.
 * @param string $type - Тип миниатюры.
 */
    private function thumbnailName($src, $type)
    {
        $tmp = explode('/', $src);
        $srcNameExt = end($tmp);
        $tmp = explode('.', $srcNameExt);
        $srcName = $tmp[0];
        $srcExt = $tmp[1];
        return hash("md5", $src) . '-' . $srcName . '-' . $type . '.' . $srcExt;
	}

/**
 * Уменьшение размера изображения до заданной ширины, пропорции сохраняются
 */
    private function resizeImageToWidth($imagePath, $WidthToResize)
    {
        list($w, $h, $type) = getimagesize($imagePath); //получаем данные загруженного файла

        $type = image_type_to_extension($type, false);
        if ('jpeg' == $type){
            $type = 'jpg';
        }

        $HeightToResize = ceil($h * $WidthToResize / $w); //Вычисляем высоту уменьшенного изображения

        $width = $WidthToResize;
        $height = $HeightToResize;

        /* calculate new image size with ratio */
        $ratio = max($width/$w, $height/$h);
        $h = ceil($height / $ratio);
        $x = ($w - $width / $ratio) / 2;
        $w = ceil($width / $ratio);

        $path = tempnam(sys_get_temp_dir(), ''); //Временный путь для нового файла, включая его имя

        /* read binary data from image file */
        $imgString = file_get_contents($imagePath);
        /* create image from string */
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);
        imagecopyresampled($tmp, $image,
        0, 0,
        $x, 0,
        $width, $height,
        $w, $h);
        /* Save image */
        switch ($type) {
        case 'jpg':
            imagejpeg($tmp, $path, 100);
            break;
        case 'png':
            imagepng($tmp, $path, 0);
            break;
        case 'gif':
            imagegif($tmp, $path);
            break;
        default:
            exit;
            break;
        }
        /* cleanup memory */
        imagedestroy($image);
        imagedestroy($tmp);

        unlink($imagePath); //удаляем исходный файл
        rename($path, $imagePath); //перемещаем уменьшенное изображение на место исходного
    }
}