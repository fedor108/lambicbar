<?php
App::uses('AppHelper', 'View/Helper');
/**
 * Помощник отображений в админ-панели
 */
class AdminHelper extends AppHelper
{
    public $helpers = array('Session');

/**
 * title для страниц админ-панели
 */
    public function title()
    {
        $title = 'Админ-панель | ';
        if ($this->Session->check('Setting.name.value')) {
            $title .= $this->Session->read('Setting.name.value');
        } else {
            $title .= 'neq4cms';
        }
        return $title;
    }

}
