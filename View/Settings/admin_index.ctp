<?= $this->element("admin/title", array('title' => "Настройки сайта")); ?>

<?= $this->Form->create('Setting', array('action' => "save", 'type' => "file" )); ?>

    <?php if (!empty($this->request->data['Setting'])) : ?>
        <?php foreach ($this->request->data['Setting'] as $n => $item) : ?>
            <?php $prefix = "Setting.{$n}";?>
            <fieldset class="well">
                <?= $this->Form->hidden("{$prefix}.id"); ?>
                <?= $this->Form->hidden("{$prefix}.type"); ?>
                <div class="row">

                    <?php if ('file' == $item['type']) : ?>
                        <img class="col-md-2" src="<?= $item['value']; ?>">
                        <div class="col-md-8">
                            <?= $this->Form->input("{$prefix}.value", array(
                                'label' => $item['label'],
                                'help' => $item['help'],
                                'type' => $item['type'],
                                'class' => "btn btn-default"
                            )); ?>
                        </div>
                    <?php else : ?>
                        <div class="col-md-10">
                            <?= $this->Form->input("{$prefix}.value", array(
                                'label' => $item['label'],
                                'help' => $item['help'],
                                'type' => $item['type'],
                            )); ?>
                        </div>
                    <?php endif; ?>

                    <div class="col-md-2">
                        <br />
                        <?= $this->Form->input("{$prefix}.delete", array(
                            'type' => 'checkbox',
                            'checkboxLabel' => 'Удалить',
                            'label' => false,
                        )); ?>
                    </div>
                </div>
            </fieldset>
        <?php endforeach; ?>
    <?php endif; ?>

    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Сохранить</button>

    <h3>Добавить настройку</h3>

    <?php
        $prefix = "Setting.0";
        if (!empty($this->request->data['Setting'])) {
            $prefix = "Setting." . count($this->request->data['Setting']);
        }
    ?>
    <fieldset class="well">
        <div class="row">
            <div class="col-md-5">
                <?= $this->Form->input("{$prefix}.name", array(
                    'label' => "Латинское обозначение",
                    'help' => "Используется, как ключ, для получения значения настройки.",
                    'value' => 'new_setting'
                )); ?>
            </div>
            <div class="col-md-5">
                <?= $this->Form->input("{$prefix}.label", array(
                    'label' => "Русское название",
                    'help' => "Для обозначения настройки в админ-панели.",
                    'value' => 'Нужно ввести, что-то понятное'
                )); ?>
            </div>
            <div class="col-md-2">
                <?= $this->Form->input("{$prefix}.create", array(
                    'type'  => 'checkbox',
                    'checkboxLabel' => 'Создать',
                    'label' => false,
                )); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <?= $this->Form->input("{$prefix}.help", array(
                    'label' => "Подсказка",
                    'help' => "Комментарий для админ-панели.",
                )); ?>
            </div>
            <div class="col-md-5">
                <?= $this->Form->input("{$prefix}.type", array(
                    'label' => "Тип значения",
                    'help' => "Для разных типов значений используются разные элементы формы.",
                    'options' => array(
                        'text' => "Одна строка или число",
                        'textarea' => "Несколько строк",
                        'file' => "Файл"
                    )
                )); ?>
            </div>
        </div>
    </fieldset>

<?= $this->Form->end(); ?>
