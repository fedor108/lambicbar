<?php if (!empty($orders)) : ?>
    <?= $this->element("admin/title", array('title' => "Бронирование столиков")); ?>
    <table class="table table-hover well">
        <thead>
        <tr>
                <th><?php echo $this->Paginator->sort('id', 'Номер'); ?></th>
                <th><?php echo $this->Paginator->sort('name', 'Имя'); ?></th>
                <th><?php echo $this->Paginator->sort('email', 'Эл. адрес'); ?></th>
                <th><?php echo $this->Paginator->sort('phone', 'Телефон'); ?></th>
                <th><?php echo $this->Paginator->sort('persons', 'Количество гостей'); ?></th>
                <th><?php echo $this->Paginator->sort('address', 'Адрес ресторана'); ?></th>
                <th><?php echo $this->Paginator->sort('visit_date', 'Дата визита'); ?></th>
                <th><?php echo $this->Paginator->sort('visit_time', 'Время визита'); ?></th>
                <th><?php echo $this->Paginator->sort('body', 'Комментарий'); ?></th>
                <th><?php echo $this->Paginator->sort('created', 'Дата отправки'); ?></th>
                <th><?php echo $this->Paginator->sort('ip', 'IP адрес отправителя'); ?></th>
                <th class="actions">Действие</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($orders as $order): ?>
        <tr>
            <td><?php echo h($order['Order']['id']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['name']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['email']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['phone']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['persons']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['address']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['visit_date']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['visit_time']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['body']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['created']); ?>&nbsp;</td>
            <td><?php echo h($order['Order']['ip']); ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Form->postLink("Удалить", array('action' => 'delete', $order['Order']['id']), array('confirm' => "Удалить бронь номер: {$order['Order']['id']}" )); ?>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>
        <ul class="pagination pagination-sm">
        <?= $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active', 'currentTag' => 'a')); ?>
        </div>
    </div>
<?php endif; ?>
