<?php if (!empty($reports)) : ?>
    <?= $this->element("admin/title", array('title' => "Список отзывов")); ?>
    <table class="table table-hover well">
        <thead>
        <tr>
                <th><?php echo $this->Paginator->sort('id', 'Номер'); ?></th>
                <th><?php echo $this->Paginator->sort('type', 'Тип'); ?></th>
                <th><?php echo $this->Paginator->sort('reason', 'Причина'); ?></th>
                <th><?php echo $this->Paginator->sort('name', 'Имя'); ?></th>
                <th><?php echo $this->Paginator->sort('email', 'Эл. адрес'); ?></th>
                <th><?php echo $this->Paginator->sort('phone', 'Телефон'); ?></th>
                <th><?php echo $this->Paginator->sort('address', 'Адрес ресторана'); ?></th>
                <th><?php echo $this->Paginator->sort('visit_date', 'Дата визита'); ?></th>
                <th><?php echo $this->Paginator->sort('body', 'Комментарий'); ?></th>
                <th><?php echo $this->Paginator->sort('created', 'Дата отправки'); ?></th>
                <th><?php echo $this->Paginator->sort('ip', 'IP адрес отправителя'); ?></th>
                <th class="actions">Действие</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($reports as $report): ?>
        <tr>
            <td><?php echo h($report['Report']['id']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['type']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['reason']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['name']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['email']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['phone']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['address']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['visit_date']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['body']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['created']); ?>&nbsp;</td>
            <td><?php echo h($report['Report']['ip']); ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Form->postLink("Удалить", array('action' => 'delete', $report['Report']['id']), array('confirm' => "Удалить отзыв номер: {$report['Report']['id']}" )); ?>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>
        <ul class="pagination pagination-sm">
        <?= $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active', 'currentTag' => 'a')); ?>
        </div>
    </div>
<?php endif; ?>
