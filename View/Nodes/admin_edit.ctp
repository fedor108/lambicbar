<!-- row -->
<div class="row">

    <div class="col-md-3">
        <div class="tree smart-form">
            <?= $this->element("admin/tree"); ?>
        </div>
    </div>

    <div class="col-md-9">
        <?= $this->element("admin/title", array('title' => array($node['Node']['title'], 'Редактирование'))); ?>
        <?= $this->element("admin/nodes", array('nodes' => $node['ChildNode'])); ?>
        <?= $this->Form->create('Node', array('action' => 'save', 'type' => 'file')); ?>
            <?php if ('event' == $node['Node']['node_type']) : ?>
                <?= $this->element("admin/event"); ?>
            <?php else : ?>
                <?= $this->element("admin/node"); ?>
            <?php endif; ?>
            <?= $this->element("admin/flags"); ?>
            <?= $this->element("admin/texts"); ?>
            <?= $this->element("admin/lines"); ?>
            <?= $this->element("admin/links"); ?>
            <?= $this->element("admin/numbers"); ?>
            <?php if ('product' == $node['Node']['node_type']) : ?>
                <?php if ('beer' == $path[1]['Node']['name']) : ?>
                    <?= $this->element("admin/beer"); ?>
                <?php endif; ?>
                <?= $this->element("admin/prices"); ?>
            <?php endif; ?>
            <?= $this->element("admin/connections"); ?>
            <?= $this->element("admin/galleries"); ?>
            <?= $this->element("admin/attachments"); ?>
            <?= $this->element("admin/seo"); ?>
        <?= $this->Form->end(); ?>
    </div>
    <!-- col-md-9 -->

</div>
<!-- end row -->
