<!-- row -->
<div class="row">

    <div class="col-md-3">
        <div class="tree smart-form">
            <?= $this->element("admin/tree"); ?>
        </div>
    </div>

    <div class="col-md-9">
        <?= $this->element("admin/title", array('title' => 'Создать узел')); ?>
        <?= $this->Form->create('Node', array('action' => 'save')); ?>
            <?= $this->element("admin/node_add"); ?>
        <?= $this->Form->end(); ?>
    </div>
    <!-- col-md-9 -->

</div>
<!-- end row -->
