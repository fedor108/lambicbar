<!-- row -->
<div class="row">

    <div class="col-md-3">
        <div class="tree smart-form">
            <?= $this->element("admin/tree"); ?>
        </div>
    </div>

    <div class="col-md-9">
        <?= $this->element("admin/title", array('title' => array($node['Node']['title'], 'Настройки'))); ?>
        <?= $this->Form->create('Node', array('action' => 'save')); ?>
            <?= $this->element("admin/node_options"); ?>
            <?= $this->element("admin/dictionaries_options"); ?>
            <?= $this->element("admin/flags_options"); ?>
            <?= $this->element("admin/texts_options"); ?>
            <?= $this->element("admin/lines_options"); ?>
            <?= $this->element("admin/links_options"); ?>
            <?= $this->element("admin/numbers_options"); ?>
            <?= $this->element("admin/galleries_options"); ?>
        <?= $this->Form->end(); ?>
    </div>
    <!-- col-md-9 -->

</div>
<!-- end row -->
