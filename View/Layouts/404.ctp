<!DOCTYPE html>
<html>
<head>
	<title>404</title>
	<link rel="stylesheet" type="text/css" href="/css/404.css">
</head>
<body>
	<?php echo $this->fetch('content'); ?>
	<video poster="/images/404-bg.jpg" preload="auto" autoplay="autoplay" loop="loop">
		<source src="/images/404-bg-video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
	</video>
</body>
</html>