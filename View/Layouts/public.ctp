<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php
            if( !empty($node['Seo']['title']) ){
                echo $this->Public->title($path, $node['Seo']['title']);
            }else{
                echo $this->Public->title($path, NULL);
            }; ?></title>
    <meta name="description" content="<?php if( !empty($node['Seo']['description']) ) echo $node['Seo']['description']; ?>">
    <meta name="keywords" content="<?php if( !empty($node['Seo']['keywords']) ) echo $node['Seo']['keywords']; ?>">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <meta name="viewport" content="width=device-width">
    <?php echo $this->element('favicon'); ?>
</head>
<body>
    <?php echo $this->element('head'); ?>
    <?php echo $this->element('head_mobile'); ?>
    <?php echo $this->fetch('content'); ?>
    <?php echo $this->element('foot'); ?>
    <?php echo $this->element('menus/mobile'); ?>
    <?php echo $this->element('counters'); ?>
    <script src="/js/libs.js"></script>
    <script src="/js/script.js"></script>
</body>
</html>
