<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title>Вход</title>
        <meta name="description" content="Панель управления сайтом">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/neq4cms.css">

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">


        <!-- FAVICONS -->
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <script src="/smart/js/libs/jquery-2.1.1.min.js"></script>
        <script src="/smart/js/libs/jquery-ui-1.10.3.min.js"></script>
        <script src="/smart/js/bootstrap/bootstrap.min.js"></script>

    </head>
    <body class="">

        <div id="main" role="main">

            <div id="content">

                <?php echo $this->fetch('content'); ?>

            </div>
            <!-- END MAIN CONTENT -->

        </div>
        <!-- END MAIN PANEL -->

        <script src="/smart/js/app.config.js"></script>

        <!-- MAIN APP JS FILE -->
        <script src="/smart/js/app.min.js"></script>

        <script type="text/javascript">
            runAllForms();

            $(function() {
                // Validation
                $("#login-form").validate({
                    // Rules for form validation
                    rules : {
                        email : {
                            required : true,
                            email : true
                        },
                        password : {
                            required : true,
                            minlength : 3,
                            maxlength : 20
                        }
                    },

                    // Messages for form validation
                    messages : {
                        email : {
                            required : 'Please enter your email address',
                            email : 'Please enter a VALID email address'
                        },
                        password : {
                            required : 'Please enter your password'
                        }
                    },

                    // Do not change code below
                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());
                    }
                });
            });
        </script>

    </body>
</html>