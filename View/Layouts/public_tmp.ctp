<?php
/**
 * TODO: удалить это файл после завершения разработки
 * Он используется только для статической верстки
 * Рабочий файл: public.ctp
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $this->fetch('title'); ?></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<meta name="viewport" content="width=device-width">
</head>
<body>
	<?php echo $this->element('header'); ?>
	<?php echo $this->element('header--mobile'); ?>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element('footer'); ?>

	<script src="/js/libs.js"></script>
	<script src="/js/script.js"></script>
	<script src="/source/js/google-maps.js"></script>
</body>
</html>
