<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title><?= $this->Admin->title(); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/smartadmin-production-plugins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/smartadmin-skins.min.css">

        <!-- SmartAdmin RTL Support -->
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/smartadmin-rtl.min.css">

        <!-- We recommend you use "your_style.css" to override SmartAdmin
             specific styles this will also ensure you retrain your customization with each SmartAdmin update.
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/your_style.css"> -->

        <!-- Выбор даты и времени -->
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/bootstrap-datetimepicker.min.css">

        <!-- Наши дополнительные стили -->
        <link rel="stylesheet" type="text/css" media="screen" href="/smart/css/neq4cms.css">

        <!-- FAVICONS -->
        <link rel="shortcut icon" href="/smart/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/smart/img/favicon/favicon.ico" type="image/x-icon">

        <!-- GOOGLE FONT -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

        <!-- Specifying a Webpage Icon for Web Clip
             Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
        <link rel="apple-touch-icon" href="/smart/img/splash/sptouch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/smart/img/splash/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/smart/img/splash/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/smart/img/splash/touch-icon-ipad-retina.png">

        <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <!-- Startup image for web apps -->
        <link rel="apple-touch-startup-image" href="/smart/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
        <link rel="apple-touch-startup-image" href="/smart/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="/smart/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

    </head>

    <!--

    TABLE OF CONTENTS.

    Use search to find needed section.

    ===================================================================

    |  01. #CSS Links                |  all CSS links and file paths  |
    |  02. #FAVICONS                 |  Favicon links and file paths  |
    |  03. #GOOGLE FONT              |  Google font link              |
    |  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
    |  05. #BODY                     |  body tag                      |
    |  06. #HEADER                   |  header tag                    |
    |  07. #PROJECTS                 |  project lists                 |
    |  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
    |  09. #MOBILE                   |  mobile view dropdown          |
    |  10. #SEARCH                   |  search field                  |
    |  11. #NAVIGATION               |  left panel & navigation       |
    |  12. #RIGHT PANEL              |  right panel userlist          |
    |  13. #MAIN PANEL               |  main panel                    |
    |  14. #MAIN CONTENT             |  content holder                |
    |  15. #PAGE FOOTER              |  page footer                   |
    |  16. #SHORTCUT AREA            |  dropdown shortcuts area       |
    |  17. #PLUGINS                  |  all scripts and plugins       |

    ===================================================================

    -->

    <!-- #BODY -->
    <!-- Possible Classes

        * 'smart-style-{SKIN#}'
        * 'smart-rtl'         - Switch theme mode to RTL
        * 'menu-on-top'       - Switch to top navigation (no DOM change required)
        * 'no-menu'           - Hides the menu completely
        * 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
        * 'fixed-header'      - Fixes the header
        * 'fixed-navigation'  - Fixes the main menu
        * 'fixed-ribbon'      - Fixes breadcrumb
        * 'fixed-page-footer' - Fixes footer
        * 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
    -->
    <body class="menu-on-top fixed-header fixed-navigation fixed-ribbon fixed-navigation">

        <!-- Left panel : Navigation area -->
        <!-- Note: This width of the aside area can be adjusted through LESS variables -->
        <aside id="left-panel">

            <!-- Навигация админ-панели -->
            <?= $this->element("admin/nav"); ?>

        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">

            <!-- Полоска под навигаций -->
            <?= $this->element("admin/ribbon"); ?>

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <?= $this->Session->flash(); ?>
                    <?= $this->fetch('content') ?>
                </section>

            </div>
            <!-- END MAIN CONTENT -->

        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">

                </div>
                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <span class="txt-color-white"><span class="hidden-xs">Система управления сайтом</span> <a href="http://digital.neq4.ru/" target="_blank">neq4cms 1.1</a> © 2014-2015</span>
                </div>
            </div>
        </div>
        <!-- END PAGE FOOTER -->


        <!--================================================== -->

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="/smart/js/plugin/pace/pace.min.js"></script>

        <script src="/smart/js/libs/jquery-2.1.1.min.js"></script>

        <script src="/smart/js/libs/jquery-ui-1.10.3.min.js"></script>

        <!-- IMPORTANT: APP CONFIG -->
        <script src="/smart/js/app.config.js"></script>

        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="/smart/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

        <!-- BOOTSTRAP JS -->
        <script src="/smart/js/bootstrap/bootstrap.min.js"></script>

        <!-- CUSTOM NOTIFICATION -->
        <script src="/smart/js/notification/SmartNotification.min.js"></script>

        <!-- JARVIS WIDGETS -->
        <script src="/smart/js/smartwidgets/jarvis.widget.min.js"></script>

        <!-- EASY PIE CHARTS -->
        <script src="/smart/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

        <!-- SPARKLINES -->
        <script src="/smart/js/plugin/sparkline/jquery.sparkline.min.js"></script>

        <!-- JQUERY VALIDATE -->
        <script src="/smart/js/plugin/jquery-validate/jquery.validate.min.js"></script>

        <!-- JQUERY MASKED INPUT -->
        <script src="/smart/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

        <!-- JQUERY SELECT2 INPUT -->
        <script src="/smart/js/plugin/select2/select2.min.js"></script>

        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="/smart/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

        <!-- browser msie issue fix -->
        <script src="/smart/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

        <!-- FastClick: For mobile devices -->
        <script src="/smart/js/plugin/fastclick/fastclick.min.js"></script>

        <!--[if IE 8]>

        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->

        <!-- CKEditor -->
        <script src="/smart/js/plugin/ckeditor/ckeditor.js"></script>

        <!-- MAIN APP JS FILE -->
        <script src="/smart/js/app.min.js"></script>

        <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
        <!-- Voice command : plugin
        <script src="/smart/js/speech/voicecommand.min.js"></script>
        -->

        <!-- SmartChat UI : plugin -->
        <script src="/smart/js/smart-chat-ui/smart.chat.ui.min.js"></script>
        <script src="/smart/js/smart-chat-ui/smart.chat.manager.min.js"></script>

        <!-- PAGE RELATED PLUGIN(S)
        <script src="..."></script>-->
        <script src="/smart/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
        <script src="/smart/js/plugin/datatables/jquery.dataTables.min.js"></script>
        <script src="/smart/js/plugin/datatables/dataTables.colVis.min.js"></script>
        <script src="/smart/js/plugin/datatables/dataTables.tableTools.min.js"></script>
        <script src="/smart/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
        <script src="/smart/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

        <!-- Выбор даты и времени -->
        <script src="/smart/js/plugin/moment/moment-with-locales.min.js"></script>
        <script src="/smart/js/plugin/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>


        <!-- Специальные скрипты -->
        <script src="/smart/js/neq4cms.js"></script>

        <script type="text/javascript">

        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->

    </body>

</html>