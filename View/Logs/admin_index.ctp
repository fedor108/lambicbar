<?php
/**
 * Журнал изменений
 */
?>
<?php if (!empty($logs)) : ?>
    <?= $this->element("admin/title", array('title' => "Журнал изменений")); ?>
    <table class="table table-hover well">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('created', 'Время'); ?></th>
            <th><?php echo $this->Paginator->sort('action', 'Действие'); ?></th>
            <th>Объект</th>
            <th><?php echo $this->Paginator->sort('username', 'Пользователь'); ?></th>
        </tr>
        </thead>
        <tbody class="logs">
            <?php foreach ($logs as $log): ?>
            <tr>
                <td><?= h($log['Log']['created']); ?>&nbsp;</td>
                <td><?= $this->Log->action($log['Log']['action']); ?>&nbsp;</td>
                <td>
                    <?php $target = $this->Log->target($log); ?>
                    <?= $this->Html->link($target['title'], $target['url']); ?>
                </td>
                <td><?= h($log['User']['username']); ?>&nbsp;</td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p><?= $this->Paginator->numbers(); ?></p>
<?php endif; ?>
