<?php $isResponsive = (isset($responsive) && $responsive); ?>

<!-- Contentmenu (begin) -->
<div class="contentmenu
	<?= isset($horizontal) && $horizontal ? 'contentmenu--horizontal' : '' ?>
	<?= $isResponsive ? 'contentmenu--responsive' : '' ?>">
	<ul class="contentmenu-list">
		<?php foreach($contentmenu as $item): ?>
			<?php if($this->here == $item['slug']): ?>
				<li class="contentmenu-item is-active"><?= $item['title'] ?></li>
			<?php else: ?>
				<li class="contentmenu-item">
					<a href="<?= $item['slug'] ?>"><?= $item['title'] ?></a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>

	<?php if ($isResponsive): ?>
		<select class="contentmenu-select">
			<?php foreach($contentmenu as $item): ?>
				<option value="<?= $item['slug'] ?>" <?= $this->here == $item['slug'] ? 'checked' : '' ?>>
					<?= $item['title'] ?>
				</option>
			<?php endforeach; ?>
		</select>
	<?php endif ?>
</div>
<!-- Contentmenu (end) -->