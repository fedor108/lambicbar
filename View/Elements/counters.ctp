<?php
/**
 * Счетчики
 */
    $yandex_metrika_counter = $this->Session->read('Setting.yandex_metrika_counter');
    $google_analytics_counter = $this->Session->read('Setting.google_analytics_counter');
?>
<!-- Счетчики -->
<?php if (false !== strpos($_SERVER['SERVER_NAME'], 'local')) : ?>
    <!-- Локально не отображаются -->
<?php else : ?>
    <?php if (!empty($yandex_metrika_counter['value'])) : ?>
        <?= $yandex_metrika_counter['value']; ?>
    <?php endif; ?>

    <?php if (!empty($google_analytics_counter['value'])) : ?>
        <?= $google_analytics_counter['value']; ?>
    <?php endif; ?>
<?php endif; ?>