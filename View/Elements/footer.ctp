<?php
	$isHome = $this->here == "/" ? true : false;
?>

<footer class="footer">
	<div class="footer-inner">
		<div class="footer-column footer-column--address">
			<h3>Адреса</h3>
			<ul>
				<li>
					<a href="" title="">ул. Воронцово Поле д. 11/32</a>
				</li>
				<li>
					<a href="" title="">ул. Мясницкая д. 40А</a>
				</li>
				<li>
					<a href="" title="">ул. Долгоруковская д. 19 стр. 7</a>
				</li>
				<li>
					<a href="" title="">ул. Неверовского д. 10</a>
				</li>
			</ul>
		</div>
		<div class="footer-column footer-column--service">
			<div class="footer-logo">
				<?php if ($isHome): ?>
					<img src="/images/footer-logo.svg">
				<?php else: ?>
					<a class="footer-logo-link" href="/" title="Перейти на главную">
						<img src="/images/footer-logo.svg">
					</a>
				<?php endif; ?>
			</div>
			<p class="copyright">2015 &bull; Lambic Brasserie</p>
			<p class="developer">Сделано <a href="http://digital.neq4.ru">нек4</a></p>
			<ul class="social-links">
				<li>
					<a href="">
						<i class="icon-vk-square-icon"></i>
					</a>
				</li>
				<li>
					<a href="">
						<i class="icon-facebook-square-icon"></i>
					</a>
				</li>
				<li>
					<a href="">
						<i class="icon-instagram-square-icon"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</footer>