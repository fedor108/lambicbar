<?php
/**
 * Список блюд
 */
    // разделяем список блюд на 2 стобца
    $col_count = (count($children) > 1) ? '2' : 1;
    $cols = array();
    if (2 == $col_count) {
        // больше одного блюда - два столбца
        $cols = array_chunk($children, (intval(count($children) / 2) + count($children) % 2));
    } else {
        // одно блюдо в списке - один столбец
        $cols[] = $children;
    }
?>
<?php if (!empty($sub_section_title)) : ?>
    <h2 class="menu-subtitle"><?= $sub_section_title; ?></h2>
<?php endif; ?>

<?php $indexOfDishWithPhoto = 0; ?>

<!-- Первая колонка -->
<div class="dish-list">
    <?php foreach($cols[0] as $item): ?>
        <div class="dish <?= (!empty($item['named']['Flag']['new']['value'])) ? 'dish--new' : '' ?>">
            <div class="dish-description">
                <h2 class="dish-title">
                    <?php if(!empty($item['named']['Gallery']['main']['Image'][0]['src'])): ?>
                        <a href="<?= $item['named']['Gallery']['main']['Image'][0]['src'] ?>"
                            data-show-gallery="menu-gallery"
                            data-gallery-start="<?= $indexOfDishWithPhoto ?>">
                            <span><?= $item['Node']['title'] ?></span>
                        </a>
                        <?php $indexOfDishWithPhoto++; ?>
                    <?php else: ?>
                        <span><?= $item['Node']['title'] ?></span>
                    <?php endif; ?>
                </h2>
                <?php if (!empty($item['named']['Text']['announce']['value'])) : ?>
                    <p class="dish-desc"><?= $item['named']['Text']['announce']['value']; ?></p>
                <?php endif; ?>
            </div>
            <ul class="dish-price">
                <?= $this->element("price", array('prices' => $item['Price'], 'connections' => $item['Connection'])); ?>
            </ul>
        </div>
    <?php endforeach; ?>
</div>

<?php if (2 == $col_count) : ?>
    <!-- Вторая колонка -->
    <div class="dish-list">
        <?php foreach($cols[1] as $item): ?>
            <div class="dish <?= (!empty($item['named']['Flag']['new']['value'])) ? 'dish--new' : '' ?>">
                <div class="dish-description">
                    <h2 class="dish-title">
                        <?php if(isset($item['named']['Gallery']['main']['Image'][0]['src'])): ?>
                            <a href="<?= $item['photo'] ?>"
                                data-show-gallery="menu-gallery"
                                data-gallery-start="<?= $indexOfDishWithPhoto ?>">
                                <span><?= $item['Node']['title'] ?></span>
                            </a>
                            <?php $indexOfDishWithPhoto++; ?>
                        <?php else: ?>
                            <span><?= $item['Node']['title'] ?></span>
                        <?php endif; ?>
                    </h2>
                    <?php if (!empty($item['named']['Text']['announce']['value'])) : ?>
                        <p class="dish-desc"><?= $item['named']['Text']['announce']['value']; ?></p>
                    <?php endif; ?>
                </div>
                <ul class="dish-price">
                    <?= $this->element("price", array('prices' => $item['Price'], 'connections' => $item['Connection'])); ?>
                </ul>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
