<?php
/**
 * TODO: удалить этот файл после завершения
 * Он используется только для статичной верстки
 * Рабочий файл head.ctp
 */
    $mainNav = array(
        array(
            array(
                'url' => '/pages/about',
                'text' => 'О нас',
                'title' => 'О нас'
            ),
            array(
                'url' => '/pages/philosophy',
                'text' => 'Философия',
                'title' => 'Философия'
            ),
            array(
                'url' => '/pages/feedback',
                'text' => 'Написать отзыв',
                'title' => 'Написать отзыв'
            )
        ),
        array(
            array(
                'url' => '/pages/menu',
                'text' => 'Меню',
                'title' => 'Меню'
            ),
            array(
                'url' => '/pages/beer',
                'text' => 'Пиво',
                'title' => 'Пиво'
            ),
            array(
                'url' => '/pages/address',
                'text' => 'Адреса',
                'title' => 'Адреса'
            ),
            array(
                'url' => '/pages/events/list',
                'text' => 'События',
                'title' => 'События'
            )
        )
    );

    $is_home = ($this->here == "/" || $this->here == "/pages/home");
?>

<!-- Header (begin) -->
<header class="header header--desktop <?= $is_home ? "header--home" : ""; ?>">
	<div class="header-wrapper">
		<div class="header-inner">
			<div class="header-column">
				<?php echo $this->element('mainNav', array('mainNav' => $mainNav[0])); ?>
			</div>
			<div class="header-column header-column--center">
				<?php echo $this->element('logo'); ?>
			</div>
			<div class="header-column">
				<?php echo $this->element('mainNav', array('mainNav' => $mainNav[1])); ?>
			</div>
		</div>
	</div>
</header>
<!-- Header (end) -->