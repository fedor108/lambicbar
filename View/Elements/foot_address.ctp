<?php
/**
 * Блок адресов в футере
 */
    $nodes = $this->requestAction("/public/address");
?>
<div class="footer-column footer-column--address">
<h3>Адреса</h3>
<ul>
    <?php foreach ($nodes as $item) : ?>
        <li>
            <a href="/address" title=""><?= $item['named']['Text']['address']['value']; ?></a>
        </li>
    <?php endforeach; ?>
</ul>
</div>