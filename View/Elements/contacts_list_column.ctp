<?php foreach ($items as $item) : ?>
    <div class="contact-item">
        <?php $item = explode("\r\n", $item); ?>
        <h2><?= $item[0]; ?></h2>
        <ul>
            <?php foreach ($item as $n => $str) : ?>
                <?php if ($n) : ?>
                    <li><?= $this->Text->autoLink($str, array('escape' => false)); ?></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endforeach; ?>