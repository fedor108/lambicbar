<?php
    $alias = 'Beer';
    $prefix = "{$alias}";
?>
<fieldset class="well">
    <?= $this->Form->hidden("{$prefix}.id"); ?>
    <?= $this->Form->hidden("{$prefix}.node_id", array('value' => $this->request->data['Node']['id'])); ?>

    <div class="row">
        <?= $this->Form->input("{$prefix}.type", array(
            'label' => 'Тип',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
        <?= $this->Form->input("{$prefix}.fermentation", array(
            'label' => 'Брожение',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
    </div>

    <div class="row">
        <?= $this->Form->input("{$prefix}.alcohol", array(
            'label' => 'Алкоголь',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
        <?= $this->Form->input("{$prefix}.density", array(
            'label' => 'Плотность',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
    </div>

    <div class="row">
        <?= $this->Form->input("{$prefix}.color", array(
            'label' => 'Цвет',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
        <?= $this->Form->input("{$prefix}.brewery", array(
            'label' => 'Пивоварня',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
    </div>
</fieldset>
