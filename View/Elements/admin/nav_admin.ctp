<?php
/**
 * Навигация админ-панели - для группы admin
 */
?>
<nav>
    <ul>
        <li>
            <a href="/" title="Открыть сайт" target="_blank"><i class="fa fa-lg fa-fw fa-home"></i> На сайт</a>
        </li>
        <li>
            <a href="/admin/nodes/edit/1" title="Дерево cтраниц сайта"><i class="fa fa-lg fa-fw fa-sitemap"></i> Содержание</a>
        </li>
        <li>
            <a href="/admin/nodes/edit/8" title="Лента новостей"><i class="fa fa-lg fa-fw fa-list"></i> Новости</a>
        </li>
        <li>
            <a href="/admin/users" title="Управление пользователями"><i class="fa fa-lg fa-fw fa-user"></i> Пользователи</a>
        </li>
        <li>
            <a href="/admin/settings" title="Нстройки сайта"><i class="fa fa-lg fa-fw fa-sliders"></i> Настройки</a>
        </li>
        <li>
            <a href="/admin/logs" title="Журнал изменений"><i class="fa fa-lg fa-fw fa-info"></i> Журнал</a>
        </li>
        <li>
            <a href="/admin/reports" title="Сообщения через форму Отправить отзыв"><i class="fa fa-lg fa-fw fa-comment-o"></i> Отзывы</a>
        </li>
        <li>
            <a href="/admin/orders" title="Бронирование столиков"><i class="fa fa-lg fa-fw fa-phone"></i> Бронь</a>
        </li>
        <li>
            <a href="/admin/menus" title="Навигационные меню"><i class="fa fa-lg fa-fw fa-reorder"></i> Навигация</a>
        </li>
        <li>
            <a href="/logout" title="Выйти из админ-панели"><i class="fa fa-lg fa-fw fa-sign-out"></i> Выход</a>
        </li>
    </ul>
</nav>