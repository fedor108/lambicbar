<?php
/**
 * Элемент редактирования одной цены - Price
 */
?>
<fieldset class="well">
    <?php if ('delete' == $action) : ?>
        <?= $this->Form->hidden("{$prefix}.id"); ?>
    <?php elseif ('create' == $action) : ?>
        <?= $this->Form->hidden("{$prefix}.model_alias", array('value' => $model_alias)); ?>
        <?= $this->Form->hidden("{$prefix}.model_id", array('value' => $model_id)); ?>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-3">
            <?= $this->Form->input("{$prefix}.quantity", array(
                'label' => 'Количество',
            )); ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input("{$prefix}.units", array(
                'label' => 'Единицы измерения',
            )); ?>

        </div>
        <div class="col-md-3">
            <?= $this->Form->input("{$prefix}.value", array(
                'label' => 'Цена',
            )); ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input("{$prefix}.{$action}", array(
                'type'  => 'checkbox',
                'checkboxLabel' => $action_label,
                'label' => false,
            )); ?>
        </div>
    </div>
</fieldset>