<?php
/**
 * Наличие и специальные цены в ресторанах
 */
    $alias = 'Connection';
?>
<h3>Наличие и специальные цены в ресторанах</h3>
<fieldset class="well">
    <?php foreach ($this->request->data['Connection'] as $n => $connection) : ?>
        <?php $prefix = "{$alias}.{$n}"; ?>
        <?php if ($connection['dictionary_id'] == $dictionary['Dictionary']['dictionary_id']) : ?>
            <?php if (!empty($connection['id'])) : ?>
                <?= $this->Form->hidden("{$prefix}.id", array('value' => $connection['id'])); ?>
            <?php endif; ?>
            <?= $this->Form->hidden("{$prefix}.term_id", array('value' => $connection['term_id'])); ?>
            <?= $this->Form->hidden("{$prefix}.dictionary_id", array('value' => $connection['dictionary_id'])); ?>
            <?= $this->Form->input("{$prefix}.active", array(
                'checkboxLabel' => $connection['Term']['title'],
                'label' => false,
            )); ?>
            <?php if ($connection['active']) : ?>
                <?= $this->Element("admin/prices_special", array(
                    'model_alias' => $alias,
                    'model_n' => $n,
                )); ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</fieldset>
