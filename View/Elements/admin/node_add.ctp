<fieldset class="well">
    <?= $this->Form->hidden('parent_id', array('value' => $node['Node']['id'])); ?>

    <?= $this->Form->input('title', array('label' => 'Название', 'help' => 'Заголовок страницы. Тег title.')); ?>
    <?= $this->Form->input('name', array('label' => 'Латинское обозначение', 'help' => 'Часть адреса страницы.')); ?>
    <?= $this->Form->input('node_type', array('label' => 'Тип узла', 'help' => 'Учитывается при редактировании данных узла.')); ?>

    <div class="form-actions text-align-left">
        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Создать</button>
    </div>
</fieldset>
