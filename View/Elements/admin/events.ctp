<div class="row">
<!-- NEW WIDGET START -->
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false">
        <!-- widget options:
        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">


        data-widget-editbutton="false"
        data-widget-togglebutton="false"
        data-widget-deletebutton="false"
        data-widget-fullscreenbutton="false"
        data-widget-custombutton="false"
        data-widget-collapsed="true"
        data-widget-sortable="false"

        -->
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2>Список узлов раздела <?= $node['Node']['title']; ?> </h2>

        </header>

        <!-- widget div-->
        <div>

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body no-padding">

<table class="table data-table table-striped table-bordered table-hover" width="100%">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Название</th>
        <th class="actions">Действия</th>
    </tr>
    </thead>
    <tbody class="sortable">
        <?php foreach ($nodes as $node): ?>
        <tr>
            <td><?php echo h(substr($node['event_from'], 0, 10)); ?>&nbsp;</td>
            <td>
                <?= $this->Html->link($node['title'],
                    array('controller' => 'nodes', 'action' => 'edit', $node['id']),
                    array('class' => "node-{$node['node_state']}")
                ); ?>
                &nbsp;
            </td>
            <td class="actions">
                <?php echo $this->Form->postLink('Удалить', array('action' => 'delete', $node['id']), array('confirm' => "Действительно, удалить узел #{$node['id']} - {$node['title']}?")); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>
    <!-- end widget -->

</article>
</div>
