<table class="table table-hover well">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Название</th>
        <th>Обозначение</th>
        <th class="actions">Действия</th>
    </tr>
    </thead>
    <tbody class="node-children">
        <?php foreach ($nodes as $node): ?>
        <tr data-id="<?= h($node['id']); ?>">
            <td><?= h($node['id']); ?>&nbsp;</td>
            <td>
                <?= $this->Html->link($node['title'],
                    array('controller' => 'nodes', 'action' => 'edit', $node['id']),
                    array('class' => "node-{$node['node_state']}")
                ); ?>
                &nbsp;
            </td>
            <td><?= h($node['name']); ?>&nbsp;</td>
            <td class="actions">
                <?= $this->Html->link('Удалить',
                    array('action' => 'delete', $node['id']),
                    array('confirm' => "Действительно, удалить узел #{$node['id']} - {$node['title']}?")
                ); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
