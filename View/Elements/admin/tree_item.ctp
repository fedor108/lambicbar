<li <?php if (empty($item['display'])) echo 'style="display:none"'; ?>>
    <div class="<?= $this->Tree->itemSpanClass($item); ?>" data-id="<?= $item['Node']['id']; ?>">
        <?= $this->Tree->itemIcon($item); ?>
        <?= $this->Html->link($item['Node']['title'],
                array('controller' => 'nodes', 'action' => 'edit', $item['Node']['id']),
                array('title' => "Перейти")
        ); ?>
    </div>
    <?php if (!empty($item['children'])) {
        echo $this->element("admin/tree", array('tree' => $item['children']));
    } ?>
</li>
