<?php
/**
 * Элемент редактирования ссылок узла
 */
    $alias = 'Number';
?>
<?php if (!empty($this->request->data[$alias])) : ?>
    <?php foreach ($this->request->data[$alias] as $n => $item) : ?>
        <?php $prefix = "{$alias}.{$n}"; ?>
        <fieldset class="well">
            <?= $this->Form->hidden("{$prefix}.id"); ?>
            <?= $this->Form->input("{$prefix}.value", array(
                'label' => $item['label'],
                'help'  => ''
            )); ?>
        </fieldset>
    <?php endforeach; ?>
<?php endif; ?>
