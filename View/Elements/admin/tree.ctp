<?php if (empty($tree)) $tree = $this->Tree->create($node); ?>
<ul>
    <?php foreach ($tree as $item) : ?>
        <?= $this->element("admin/tree_item", array('item' => $item)); ?>
    <?php endforeach; ?>
</ul>
