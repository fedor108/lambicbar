<fieldset class="well">
    <?php
        echo $this->Form->hidden('id');
        echo $this->Form->input('node_type', array('label' => 'Тип узла', 'help' => 'Учитывается при редактировании данных узла'));
        echo $this->Form->input('user_id', array('label' => 'Автор'));
        echo $this->Form->input('is_dictionary', array('checkboxLabel' => 'Справочник', 'label' => false));
        echo $this->Form->input('protected', array('checkboxLabel' => 'Запрет удаления', 'label' => false));
    ?>
</fieldset>
<?php
    // задаем кнопки действий
    $this->start('actions');
?>
    <?= $this->element("admin/buttons/submit"); ?>
    <?= $this->element("admin/buttons/view"); ?>
    <?= $this->element("admin/buttons/add"); ?>
    <?= $this->element("admin/buttons/edit"); ?>
    <?= $this->element("admin/buttons/delete"); ?>
<?php $this->end(); ?>
