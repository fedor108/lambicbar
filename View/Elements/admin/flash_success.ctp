<div class="alert alert-success alert-block">
    <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">Успешно!</h4>
    <?= $message; ?>
</div>