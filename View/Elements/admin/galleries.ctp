<?php
/**
 * Элемент редактирования галереи узла
 */
    $alias = 'Gallery';
?>
<?php if (!empty($this->request->data[$alias])) : ?>
    <?php foreach ($this->request->data[$alias] as $n => $item) : ?>
        <?php
            // пропускаем галерею прикрепленных файлов
            if ('attachments' == $item['name']) continue;
        ?>
        <?php $prefix = "{$alias}.{$n}"; ?>
        <div class="row">
            <h3 class="col-md-6"><?= $item['label'];?></h3>
            <div class="col-md-6">
                Загрузить изображения в галерею
                <input type="file" multiple name="data[Gallery][<?=$n;?>][upload][]" class='btn btn-default'>
            </div>
        </div>
        <?= $this->Form->hidden("{$prefix}.id"); ?>
        <?= $this->element("admin/images", array('images' => $item['Image'], 'gallery_n' => $n)); ?>
    <?php endforeach; ?>
<?php endif; ?>
