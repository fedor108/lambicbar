<?php $prefix = "MenuItem.999"; ?>
<h3>Добавить пункт меню</h3>
<fieldset class="well">
    <?= $this->Form->hidden("{$prefix}.menu_id", array('value' => $this->request->data['Menu']['id'])); ?>
    <?= $this->Form->input("{$prefix}.node_id", array('label' => 'Целевой узел', 'help' => 'Страница, на которую будет вести ссылка в меню.')); ?>
    <?= $this->Form->input("{$prefix}.url", array('label' => 'Целевой адрес', 'help' => 'Адрес, на который будет вести ссылка в меню. Если не выбран целевой узел.')); ?>
    <?= $this->Form->input("{$prefix}.title", array('label' => 'Текст', 'help' => 'Текст пункта меню, если он отличается от заголовка целевого узла.')); ?>
    <?= $this->Form->input("{$prefix}.link_title", array('label' => 'Всплывающая подсказка', 'help' => 'Отображается при наведении курсора на пункт меню.')); ?>
    <?= $this->Form->input("{$prefix}.target", array(
        'label' => 'Как открывать ссылку',
        'help' => 'В новом окне нужно открывать ссылки, ведущие на другие сайты.',
        'options' => array('' => "В этом же окне", '_blank' => "В новом окне")
    )); ?>
    <?= $this->Form->input("{$prefix}.create", array(
        'type'  => 'checkbox',
        'checkboxLabel' => 'Создать',
        'label' => false,
    )); ?>
</fieldset>
