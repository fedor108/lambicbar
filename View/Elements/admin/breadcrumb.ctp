<?php
    if (!empty($path)) {
        // список слов для крошек
        $titles = Hash::extract($path, '{n}.Node.title');

        // максимальная длинна крошек
        $max_length = 55;

        // текущая длинна крошек
        $length = 0;
        foreach ($titles as $title) {
            $length += mb_strlen($title);
        }

        // максимальная длинна одного слова
        $max_word_length = ceil ($max_length / count($titles));

        // добавка для сокращенных слов
        $sufix = '...';
        $sufix_length = strlen($sufix);

        // собираем крошки
        foreach ($path as $item) {
            $title = $item['Node']['title'];
            if ($length > $max_length) {
                // укорачиваем длинные слова
                $word_length = mb_strlen($title);
                $new_length = $max_word_length - $sufix_length;
                $title = mb_substr($title, 0, $new_length) . $sufix;
            }
            $this->Html->addCrumb($title, "/admin/nodes/edit/{$item['Node']['id']}", array(
                'title' => $item['Node']['title'],
            ));
        }
    } elseif ('menu_items' == $this->params['controller']) {
        $this->Html->addCrumb("Меню", "/admin/menus");
        $this->Html->addCrumb($this->request->data['Menu']['title'], "/admin/menus/edit/{$this->request->data['Menu']['id']}");
        $this->Html->addCrumb($this->request->data['MenuItem']['title'], "/admin/menu_items/edit/{$this->request->data['MenuItem']['id']}");
    } elseif ('menus' == $this->params['controller']) {
        $this->Html->addCrumb("Меню", "/admin/menus");
        if (!empty($this->request->data['Menu']['id'])) {
            $this->Html->addCrumb($this->request->data['Menu']['title'], "/admin/menus/edit/{$this->request->data['Menu']['id']}");
        }
    } elseif ('users' == $this->params['controller']) {
        $this->Html->addCrumb("Пользователи", "/admin/users");
        if (!empty($this->request->data['User']['id'])) {
            $this->Html->addCrumb($this->request->data['User']['username'], "/admin/users/edit/{$this->request->data['User']['id']}");
        }
    } elseif ('settings' == $this->params['controller']) {
        $this->Html->addCrumb("Настройки", "/admin/settings");
    } elseif ('logs' == $this->params['controller']) {
        $this->Html->addCrumb("Журанл изменений", "/admin/logs");
    }
    else {
        $this->Html->addCrumb($this->params['controller'], array('controller' => $this->params['controller'], 'action' => 'index'));
        $this->Html->addCrumb($this->params['action'], array('controller' => $this->params['controller'], 'action' => $this->params['action']));
    }

    echo $this->Html->getCrumbList(array('class' => 'breadcrumb'));
