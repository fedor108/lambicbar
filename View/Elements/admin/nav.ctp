<?php
/**
 * Навигация админ-панели
 */
    $user = $this->Session->read('Auth.User');
?>
<?php if ('admin' == $user['Group']['name']) : ?>
    <?= $this->element("/admin/nav_admin"); ?>
<?php elseif ('editor' == $user['Group']['name']) : ?>
    <?= $this->element("/admin/nav_editor"); ?>
<?php endif; ?>
<span class="minifyme" data-action="minifyMenu">
    <i class="fa fa-arrow-circle-left hit"></i>
</span>
