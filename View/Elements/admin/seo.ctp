<?php
/**
 * Элемент редактирования SEO полей узла
 */
    $alias = 'Seo';
    $prefix = $alias;
?>

<fieldset class="well">
    <?= $this->Form->hidden("{$prefix}.id"); ?>
    <?= $this->Form->hidden("{$prefix}.node_id", array('value' => $this->request->data['Node']['id'])); ?>
    <?= $this->Form->input("{$prefix}.title", array('label' => 'Тег title', 'help' => '')); ?>
    <?= $this->Form->input("{$prefix}.description", array('label' => 'Тег meta description', 'help' => '')); ?>
    <?= $this->Form->input("{$prefix}.keywords", array('label' => 'Тег meta keywords', 'help' => '')); ?>
</fieldset>