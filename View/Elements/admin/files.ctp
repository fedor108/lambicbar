<?php
/**
 * Элемент редактирования прикрепленных файлов в галерее attachments
 */
    $alias = 'Image';
?>
<?php if (!empty($images)) : ?>
    <ul class="gallery">
    <?php foreach ($images as $n => $item) : ?>
        <?php $prefix = "Gallery.{$gallery_n}.{$alias}.{$n}"; ?>
        <li class="well" data-id="<?= $item['id']; ?>" data-key="<?= "Gallery{$gallery_n}{$alias}{$n}"; ?>">

            <?= $this->Form->hidden("{$prefix}.id"); ?>
            <?= $this->Form->hidden("{$prefix}.order"); ?>

            <div class="row">
                <div class="col-md-10">
                    <?= $this->Form->input("{$prefix}.title", array('label' => 'Название')); ?>
                </div>
                <div class="col-md-2">
                    <?= $this->Form->input("{$prefix}.delete", array('type'  => 'checkbox', 'checkboxLabel' => 'Удалить', 'label' => false)); ?>
                </div>
            </div>
            <?= $this->Form->input("{$prefix}.description", array('label' => 'Описание')); ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->input("{$prefix}.upload", array('type' => 'file', 'label' => 'Заменить файл', 'class' => 'btn btn-default')); ?>
                </div>
                <div class="col-md-6">
                    Загруженный файл: <?= $this->Html->link(substr($item['src'], (strrpos($item['src'], '/') + 1)),
                        $item['src'],
                        array('target' => "_blank")
                    ); ?>
                </div>
            </div>
        </li>
    <?php endforeach; ?>
    </ul>
<?php endif; ?>
