<div class="alert alert-danger alert-block">
    <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">Ошибка!</h4>
    <?= $message; ?>
</div>