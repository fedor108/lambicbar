<?php
/**
 * Элемент редактирования соединений узлов и терминов справочника
 */
?>
<?php if (!empty($this->request->data['Connection'])) : ?>
    <?php $alias = 'Connection'; ?>
    <?php foreach ($dictionaries as $dictionary) : ?>
         <?php if (('address' == $dictionary['DictionaryNode']['name']) && ('product' == $node['Node']['node_type'])) : ?>
            <?= $this->element("admin/availabilities", array('dictionary' => $dictionary)); ?>
        <?php else : ?>
            <h3><?= $dictionary['DictionaryNode']['title']; ?></h3>
            <fieldset class="well">
                <?php foreach ($this->request->data['Connection'] as $n => $connection) : ?>
                    <?php $prefix = "{$alias}.{$n}"; ?>
                    <?php if ($connection['dictionary_id'] == $dictionary['Dictionary']['dictionary_id']) : ?>
                        <?php if (!empty($connection['id'])) : ?>
                            <?= $this->Form->hidden("{$prefix}.id", array('value' => $connection['id'])); ?>
                        <?php endif; ?>
                        <?= $this->Form->hidden("{$prefix}.term_id", array('value' => $connection['term_id'])); ?>
                        <?= $this->Form->hidden("{$prefix}.node_id", array('value' => $connection['node_id'])); ?>
                        <?= $this->Form->hidden("{$prefix}.dictionary_id", array('value' => $connection['dictionary_id'])); ?>
                        <?= $this->Form->input("{$prefix}.active", array(
                            'checkboxLabel' => $connection['Term']['title'],
                            'label' => false,
                        )); ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </fieldset>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
