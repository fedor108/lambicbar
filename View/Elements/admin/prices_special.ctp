<?php
/**
 * Элемент редактирования специальных цен в ресторане
 */
    $alias = 'Price';
    $model_alias = 'Connection';

    // цены подключенные к связи блюда и ресторана
    $prices = array();
    if (!empty($this->request->data['Connection'][$model_n][$alias])) {
        $prices = $this->request->data['Connection'][$model_n][$alias];
    }

?>
<?php if (!empty($prices)) : ?>
    <?php foreach ($prices as $n => $item) : ?>
        <?= $this->Element("admin/price", array(
            'prefix' => "{$model_alias}.{$model_n}.{$alias}.{$n}",
            'action' => 'delete',
            'action_label' => "Удалить"
        )); ?>
    <?php endforeach; ?>
<?php endif; ?>

<?php $n = count($prices); ?>
<?= $this->Element("admin/price", array(
    'prefix' => "{$model_alias}.{$model_n}.{$alias}.{$n}",
    'action' => 'create',
    'action_label' => "Создать",
    'model_alias' => $model_alias,
    'model_id' => $this->request->data['Connection'][$model_n]['id']
)); ?>