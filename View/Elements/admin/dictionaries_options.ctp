<?php if (!empty($dictionaries)) : ?>

    <?php $alias = 'Dictionary'; ?>

    <h3>Подключить справочники к узлу (разделу)</h3>
    <fieldset class="well">
    <?php foreach ($this->Dictionary->dictionaries($dictionaries, $node) as $n => $item) : ?>
        <?php $prefix = "{$alias}.{$n}"; ?>
        <?= $this->Form->hidden("{$prefix}.dictionary_id", array('value' => $item['Node']['id'])); ?>
        <?php if (!empty($item['id'])) : ?>
            <?= $this->Form->hidden("{$prefix}.id", array('value' => $item['id'])); ?>
            <?= $this->Form->input("{$prefix}.value", array(
                'type' => 'checkbox',
                'checked' => 1,
                'checkboxLabel' => $item['Node']['title'],
                'label' => false,
            )); ?>
        <?php else : ?>
            <?= $this->Form->input("{$prefix}.value", array(
                'type' => 'checkbox',
                'checked' => 0,
                'checkboxLabel' => $item['Node']['title'],
                'label' => false,
            )); ?>
        <?php endif; ?>

    <?php endforeach; ?>
    </fieldset>

<?php endif; ?>
