<?php
/**
 * Элемент редактирования общих цен
 */
    $alias = 'Price';
    $model_alias = 'Node';
?>
<?php if (!empty($this->request->data[$alias])) : ?>
    <h3>Общие цены для всех ресторанов</h3>
    <?php foreach ($this->request->data[$alias] as $n => $item) : ?>
        <?= $this->Element("admin/price", array(
            'prefix' => "{$alias}.{$n}",
            'action' => 'delete',
            'action_label' => "Удалить"
        )); ?>
    <?php endforeach; ?>
<?php endif; ?>

<h3>Добавить цену для всех ресторанов</h3>
<?php $n = count($this->request->data[$alias]); ?>
<?= $this->Element("admin/price", array(
    'prefix' => "{$alias}.{$n}",
    'action' => 'create',
    'action_label' => "Создать",
    'model_alias' => $model_alias,
    'model_id' => $this->request->data[$model_alias]['id']
)); ?>
