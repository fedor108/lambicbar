<fieldset class="well">

    <?= $this->Form->hidden('id'); ?>
    <?= $this->Form->hidden('node_type'); ?>

    <?= $this->Form->input('title', array('label' => 'Название', 'help' => 'Заголовок страницы и тег title')); ?>
    <?= $this->Form->input('name', array('label' => 'Латинское обозначение', 'help' => 'Часть адреса страницы')); ?>

    <div class="row">
        <?= $this->Form->input('node_state', array('label' => 'Состояние узла', 'help' => 'Опубликованные узлы отображаются на сайте, черновики - нет.', 'div' => array('class' => 'form-group col-md-6'))); ?>
        <?= $this->Form->input('published', array(
            'label' => 'Дата публикации',
            'type' => 'text',
            'div' => array('class' => 'form-group col-md-6'),
            'class' => 'datetimepicker form-control',
        )); ?>
    </div>

</fieldset>
<?php
    // задаем кнопки действий
    $this->start('actions');
?>
    <?= $this->element("admin/buttons/submit"); ?>
    <?= $this->element("admin/buttons/view"); ?>
    <?= $this->element("admin/buttons/add"); ?>
    <?= $this->element("admin/buttons/options"); ?>
    <?= $this->element("admin/buttons/delete"); ?>
<?php $this->end(); ?>
