<?php
    if (is_array($title)) {
        $data = $title;
    } else {
        $data[] = $title;
    }
?>
<h1 class="page-title txt-color-blueDark">
    <i class="fa fa-pencil-square-o fa-fw "></i>
    <?php foreach ($data as $key => $value) : ?>
        <?php if ($key) : ?>
            <span>>
                <?= $value; ?>
            </span>
        <?php else : ?>
            <?= $value; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</h1>