<?php
/**
 * Элемент редактирования настроек флагов узла
 * Удаление и создание флагов
 */
    $alias = 'Flag';
?>
<h3>Логические</h3>
<table class="table table-hover well">
    <thead>
    <tr>
        <th>#</th>
        <th>Русское обозначение</th>
        <th>Латинское обозначение</th>
        <th class="actions">Действия</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($this->request->data[$alias] as $n => $item): ?>
            <?php $prefix = "{$alias}.{$n}"; ?>
            <tr>
                <td><?php echo h($item['id']); ?>&nbsp;<?= $this->Form->hidden("{$prefix}.id"); ?></td>
                <td><?= $this->Form->input("{$prefix}.label", array('label' => false, 'div'  => false)); ?></td>
                <td><?= $this->Form->input("{$prefix}.name", array('label' => false,'div'  => false)); ?></td>
                <td class="actions">
                    <?= $this->Form->input("{$prefix}.delete", array(
                        'type'  => 'checkbox',
                        'checkboxLabel' => 'Удалить',
                        'label' => false,
                    )); ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php
            // добавляем элемент для произвольного нового поля
            $items[$alias]['add']['new_' . strtolower($alias)] = 'Новое поле (задайте обозначение)';

            // количество существующих записей + 1
            $n = count($this->request->data[$alias]);
        ?>
        <?php foreach ($items[$alias]['add'] as $name => $label): ?>
            <?php
                $prefix = "{$alias}.{$n}";
                $n++;
            ?>
            <?= $this->Form->hidden("{$prefix}.node_id", array('value' => $this->request->data['Node']['id'])); ?>
            <tr>
                <td><span class="fa fa-plus"></span></td>
                <td><?= $this->Form->input("{$prefix}.label", array('value' => $label, 'label' => false, 'div'  => false)); ?></td>
                <td><?= $this->Form->input("{$prefix}.name", array('value' => $name, 'label' => false,'div'  => false)); ?></td>
                <td class="actions">
                    <?= $this->Form->input("{$prefix}.create", array(
                        'type'  => 'checkbox',
                        'checkboxLabel' => 'Создать',
                        'label' => false,
                    )); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
