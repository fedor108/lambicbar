<fieldset class="well">
    <?= $this->Form->hidden('id'); ?>

    <div class="row">
        <?= $this->Form->input('title', array(
            'label' => 'Название',
            'help' => 'Заголовок страницы и тег title',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
        <?= $this->Form->input('name', array(
            'label' => 'Латинское обозначение',
            'help' => 'Часть адреса страницы',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
    </div>

    <div class="row">
        <?= $this->Form->input('event_from', array(
            'label' => 'Дата начала',
            'type' => 'text',
            'help' => 'Влияет на порядок отображения в календаре',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
        <?= $this->Form->input('event_to', array(
            'label' => 'Дата окончания',
            'type' => 'text',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
    </div>

    <div class="row">
        <?= $this->Form->input('event_duration', array(
            'label' => 'Продолжительность словами',
            'help' => 'Отображается в списке мероприятий',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
        <?= $this->Form->input('node_state', array(
            'label' => 'Состояние узла',
            'help' => 'Опубликованные узлы отображаются на сайте, черновики - нет.',
            'div' => array('class' => 'form-group col-md-6')
        )); ?>
    </div>

    <div class="form-actions text-align-left">
        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Сохранить</button>
        <?= $this->Html->link('<i class="fa fa-eye"></i> Посмотреть на сайте',
            $node['Node']['slug'] ? $node['Node']['slug'] : '/',
            array(
                'class' => "btn btn-success",
                'target'=> "_blank",
                'escape' => false,
                )
        ); ?>
        <?= $this->Html->link('<i class="fa fa-cog"></i> Настройки узла',
            array('action' => "options", $node['Node']['id']),
            array(
                'class' => "btn btn-default",
                'escape' => false,
            )
        ); ?>
        <?= $this->Html->link('<i class="glyphicon glyphicon-remove"></i> Удалить узел',
            array('action' => 'delete', $node['Node']['id']),
            array(
                'confirm' => "Удалить {$node['Node']['title']} #{$node['Node']['id']}?",
                'class'   => "btn btn-danger",
                'escape' => false,
            )
        ); ?>
    </div>
</fieldset>
