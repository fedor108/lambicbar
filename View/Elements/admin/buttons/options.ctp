<?php
/**
 * Настройки узла
 */
?>
<?= $this->Html->link('<i class="fa fa-cog"></i> Настройки',
    array('action' => "options", $node['Node']['id']),
    array(
        'class' => "btn btn-default btn-ribbon",
        'escape' => false,
        'title' => "Перейти на страницу настроек узла",
    )
); ?>