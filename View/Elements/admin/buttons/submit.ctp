<?php
/**
 * Основная кнопка отправки формы узла
 */
?>
<button type="submit"
        class="btn btn-primary btn-ribbon node-save-submite"
        data-form-id="NodeSaveForm">
    <i class="fa fa-save"></i> Сохранить
</button>
