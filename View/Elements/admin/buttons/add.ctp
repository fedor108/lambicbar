<?php
/**
 * Создать потомка
 */
?>
<?= $this->Html->link('<i class="fa fa-plus"></i> Создать',
    array('action' => "add", $node['Node']['id']),
    array(
        'class' => "btn btn-default btn-ribbon",
        'escape' => false,
        'title' => "Создать потомка текущего узла",
    )
); ?>