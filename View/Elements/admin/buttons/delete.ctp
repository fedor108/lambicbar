<?php
/**
 * Удалить узел
 */
?>
<?= $this->Html->link('<i class="glyphicon glyphicon-remove"></i> Удалить',
    array('action' => 'delete', $node['Node']['id']),
    array(
        'confirm' => "Удалить {$node['Node']['title']} #{$node['Node']['id']}?",
        'class'   => "btn btn-danger btn-ribbon",
        'escape' => false,
        'title' => "Удалить узел вместе с потомками",
    )
); ?>