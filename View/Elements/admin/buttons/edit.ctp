<?php
/**
 * Редактировать данные узла
 */
?>
<?= $this->Html->link('<i class="fa fa-cog"></i> Редактировать',
    array('action' => "edit", $node['Node']['id']),
    array(
        'class' => "btn btn-default btn-ribbon",
        'escape' => false,
        'title' => "Перейти на страницу редактирования данных узла",
    )
); ?>