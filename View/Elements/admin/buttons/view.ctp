<?php
/**
 * Посмотреть на сайте
 */
    $url = $node['Node']['slug'];
    $arr = explode('/', $url);
    if (in_array($arr[1], array('menu', 'beer'))
        && (count($arr) > 3)
    ) {
        $arr = array_slice($arr, 0, 3);
        $url = implode('/', $arr);
    }
?>
<?= $this->Html->link('<i class="fa fa-eye"></i> Посмотреть',
    $url,
    array(
        'class' => "btn btn-success btn-ribbon",
        'target'=> "_blank",
        'escape' => false,
        'title' => "Посмотреть на сайте",
        )
); ?>
