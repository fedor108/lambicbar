<?php if (!empty($nodes)) : ?>
    <?php if ('event' == $nodes[0]['node_type']) : ?>
        <?= $this->element('admin/events', array('nodes' => $nodes)); ?>
    <?php else : ?>
        <?= $this->element('admin/pages', array('nodes' => $nodes)); ?>
    <?php endif; ?>
<?php endif; ?>
