<?php
/**
 * Навигация админ-панели - для группы editor
 */
?>
<nav>
    <ul>
        <li>
            <a href="/" title="Открыть сайт" target="_blank"><i class="fa fa-lg fa-fw fa-home"></i> На сайт</a>
        </li>
        <li>
            <a href="/admin/nodes/edit/1" title="Дерево cтраниц сайта"><i class="fa fa-lg fa-fw fa-sitemap"></i> Содержание</a>
        </li>
        <li>
            <a href="/admin/menus" title="Навигационные меню"><i class="fa fa-lg fa-fw fa-reorder"></i> Навигация</a>
        </li>
        <li>
            <a href="/admin/users/profile" title="Личные данные"><i class="fa fa-lg fa-fw fa-user"></i> <?= AuthComponent::user('username'); ?></a>
        </li>
        <li>
            <a href="/logout" title="Выйти из админ-панели"><i class="fa fa-lg fa-fw fa-sign-out"></i> Выход</a>
        </li>
    </ul>
</nav>