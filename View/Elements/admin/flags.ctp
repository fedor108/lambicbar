<?php
/**
 * Элемент редактирования (снятия / установки) флагов узла
 */
    $alias = 'Flag';
?>
<?php if (!empty($this->request->data['Flag'])) : ?>
    <fieldset class="well">
        <?php foreach ($this->request->data['Flag'] as $n => $item) : ?>
            <?php $prefix = "{$alias}.{$n}"; ?>
            <?= $this->Form->hidden("{$prefix}.id"); ?>
            <?= $this->Form->input("{$prefix}.value", array(
                'checkboxLabel' => $item['label'],
                'label' => false,
                'help'  => ''
            )); ?>
        <?php endforeach; ?>
    </fieldset>
<?php endif; ?>
