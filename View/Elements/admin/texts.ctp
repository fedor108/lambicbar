<?php
/**
 * Элемент редактирования текстов узла
 */
    $alias = 'Text';
?>
<?php if (!empty($this->request->data['Text'])) : ?>
    <?php foreach ($this->request->data['Text'] as $n => $text) : ?>
        <fieldset class="well">
            <?php $prefix = "{$alias}.{$n}"; ?>
            <?= $this->Form->hidden("{$prefix}.id"); ?>
            <?= $this->Form->input("{$prefix}.value", array(
                'label' => $text['label'],
                'class' => "form-control {$text['editor']}",
                'help'  => ''
            )); ?>
        </fieldset>
    <?php endforeach; ?>
<?php endif; ?>
