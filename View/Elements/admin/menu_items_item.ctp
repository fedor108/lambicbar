<li class="dd-item dd3-item" data-id="<?= $item['MenuItem']['id']; ?>" data-title="<?= $item['MenuItem']['title']; ?>">
    <div class="dd-handle dd3-handle"></div>
    <div class="dd3-content">
        <?= $this->Html->link($item['MenuItem']['title'], array('controller' => 'menu_items', 'action' => 'edit', $item['MenuItem']['id'])); ?>
        <?= $this->Html->link("Удалить",
            array('controller' => 'menu_items', 'action' => 'delete', $item['MenuItem']['id']),
            array(
                'class' => 'pull-right',
                'confirm' => "Удалить пункт меню #{$item['MenuItem']['title']}?"
            )
        ); ?>
    </div>
    <?php if (!empty($item['children'])) : ?>
        <?= $this->element('admin/menu_items_list', array('items' => $item['children'])); ?>
    <?php endif; ?>
</li>
