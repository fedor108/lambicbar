<?php
/**
 * Элемент редактирования ссылок узла
 */
    $alias = 'Link';
?>
<?php if (!empty($this->request->data[$alias])) : ?>
    <?php foreach ($this->request->data[$alias] as $n => $item) : ?>
        <?php $prefix = "{$alias}.{$n}"; ?>
        <fieldset class="well">
            <label><?= $item['label'];?></label>
            <?= $this->Form->hidden("{$prefix}.id"); ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->input("{$prefix}.url", array(
                        'label' => 'Адрес',
                        'help'  => 'Адрес перехода по ссылке'
                    )); ?>
                    <?= $this->Form->input("{$prefix}.target", array(
                        'label' => 'Как открыть',
                        'help'  => ''
                    )); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->input("{$prefix}.text", array(
                        'label' => 'Текст ссылки',
                        'help'  => 'Отображается на странице'
                    )); ?>
                    <?= $this->Form->input("{$prefix}.title", array(
                        'label' => 'Текст подсказки',
                        'help'  => 'Показывается при наведении'
                    )); ?>
                </div>
            </div>
        </fieldset>
    <?php endforeach; ?>
<?php endif; ?>
