<?php
/**
 * Элемент редактирования изображений в галерее узла
 */
    $alias = 'Image';
?>
<?php if (!empty($images)) : ?>
    <ul class="gallery">
    <?php foreach ($images as $n => $item) : ?>
        <?php $prefix = "Gallery.{$gallery_n}.{$alias}.{$n}"; ?>
        <li class="well" data-id="<?= $item['id']; ?>" data-key="<?= "Gallery{$gallery_n}{$alias}{$n}"; ?>">
            <div class="row">
                <img class="col-md-3" src="<?= $item['src']; ?>" alt="<?= $item['title']; ?>" />
                <div class="col-md-9">
                    <?= $this->Form->hidden("{$prefix}.id"); ?>
                    <?= $this->Form->hidden("{$prefix}.order"); ?>
                    <div class="row">
                        <div class="col-md-10">
                            <?= $this->Form->input("{$prefix}.title", array('label' => 'Название / Заголовок')); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input("{$prefix}.delete", array('type'  => 'checkbox', 'checkboxLabel' => 'Удалить', 'label' => false)); ?>
                        </div>
                    </div>
                    <?= $this->Form->input("{$prefix}.description", array('label' => 'Описание / Текст на изображении')); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $this->Form->input("{$prefix}.url", array('label' => 'Адрес ссылки')); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input("{$prefix}.target", array(
                                'label' => 'Как открывать',
                                'options' => array('' => 'Нормально', '_blank' => 'В новом окне')
                            )); ?>
                        </div>
                    </div>
                    <?= $this->Form->input("{$prefix}.upload", array('type' => 'file', 'label' => 'Заменить изображение', 'class' => 'btn btn-default')); ?>
                </div>
            </div>
        </li>
    <?php endforeach; ?>
    </ul>
<?php endif; ?>
