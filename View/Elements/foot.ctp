<?php
/**
 * Общая нижняя часть страниц сайта
 */
    $is_home = ('mainpage' == $node['Node']['name']);
    if ($is_home) {
        $mainpage = $node;
    } else {
        $mainpage = $this->requestAction("/public/mainpage");
    }
?>
<footer class="footer">
    <div class="footer-inner">
        <?= $this->element('foot_address'); ?>
        <div class="footer-column footer-column--service">
            <div class="footer-logo">
                <?php if ($is_home): ?>
                    <img src="/images/footer-logo.svg">
                <?php else: ?>
                    <a class="footer-logo-link" href="/" title="Перейти на главную">
                        <img src="/images/footer-logo.svg">
                    </a>
                <?php endif; ?>
            </div>
            <p class="copyright"><?= $mainpage['named']['Line']['copyright']['value']; ?></p>
            <p class="developer">Сделано <a href="http://digital.neq4.ru" target="_blank">нек4</a></p>
            <ul class="social-links">
                <?php $names = array('vk', 'facebook', 'instagram'); ?>
                <?php foreach ($names as $name) : ?>
                    <?php if (!empty($mainpage['named']['Line'][$name]['value'])) : ?>
                        <li>
                            <a href="<?= $mainpage['named']['Line'][$name]['value']; ?>" target="_blank">
                                <i class="icon-<?= $name; ?>-square-icon"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</footer>