<?php
/**
 * Общая верхняя часть страниц сайта - Мобильная версия
 */
    $is_home = ('mainpage' == $node['Node']['name']);
?>
<!-- Header (begin) -->
<header class="header header--mobile <?= $is_home ? "header--home" : ""; ?>">
    <!-- Logo (begin) -->
    <div class="logo">
        <?php if ($is_home): ?>
            <img src="/images/logo--mobile-home.svg">
        <?php else: ?>
            <a class="logo-link" href="/" title="Перейти на главную">
                <img src="/images/logo--mobile.svg">
            </a>
        <?php endif; ?>
    </div>
    <!-- Logo (end) -->

    <button class="toggle-menu js-toggle-menu"></button>
</header>
<!-- Header (end) -->