<?php
	$is_home = ($this->here == "/" || $this->here == "/pages/home");
?>

<!-- Logo (begin) -->
<div class="logo">
	<?php if ($is_home): ?>
		<img src="/images/logo--home.svg">
	<?php else: ?>
		<a class="logo-link" href="/" title="Перейти на главную">
			<img src="/images/logo.svg">
		</a>
	<?php endif; ?>
</div>
<!-- Logo (end) -->