<?php
/**
 * Общая верхняя часть страниц сайта
 */
    $is_home = ('mainpage' == $node['Node']['name']);
?>

<!-- Header (begin) -->
<header class="header header--desktop <?= $is_home ? "header--home" : ""; ?>">
    <div class="header-wrapper">
        <div class="header-inner">
            <div class="header-column">
                <?php echo $this->element('menus/top_left'); ?>
            </div>
            <div class="header-column header-column--center">
                <?php echo $this->element('logo'); ?>
            </div>
            <div class="header-column">
                <?php echo $this->element('menus/top_right'); ?>
            </div>
        </div>
    </div>
</header>
<!-- Header (end) -->