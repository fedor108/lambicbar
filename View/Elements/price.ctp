<?php
/**
 * Элемент отображения цены Продукта
 */
    $prices = $this->Public->price($prices, $connections, $place['Node']['id']);
?>
<?php if (count($prices) > 1) : ?>
    <?php if (!empty($prices[0]['quantity'])) : ?>
        <?php foreach($prices as $price): ?>
            <li>
                <span class="volume"><?= $price['quantity']; ?> <?= $price['units']; ?></span>
                <?= $price['value']; ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
            </li>
        <?php endforeach;?>
    <?php else : ?>
        <li>
            <?= $prices[0]['value']; ?> - <?= $prices[1]['value']; ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
        </li>
    <?php endif;?>
<?php elseif (1 == count($prices)) : ?>
    <?php $price = $prices[0]; ?>
    <?php if (!empty($price['units'])) : ?>
        <li>
            <span class="volume"><?= $price['quantity']; ?> <?= $price['units']; ?></span>
            <?= $price['value']; ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
        </li>
    <?php else : ?>
        <li>
            <?= $price['value']; ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
        </li>
    <?php endif; ?>
<?php endif; ?>