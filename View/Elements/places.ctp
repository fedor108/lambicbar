<?php
/**
 * Выбор ресторана
 */
?>
<!-- <div class="select select--backgrounded"> -->
<div id="places-menu-widget" class="select2-widget--backgrounded">
    <select class="js-select-page" data-width="100%" data-shuffle>
        <?php foreach ($places as $item) : ?>
            <?php if ($item['url'] == $this->here) : ?>
                <option value="" selected="selected"><?= $item['title']; ?></option>
            <?php else : ?>
                <option value="<?= "{$item['url']}"; ?>"><?= $item['title']; ?></option>
            <?php endif; ?>
        <?php endforeach; ?>
    </select>
</div>