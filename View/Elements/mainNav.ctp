<?php
/**
 * TODO: удалить это файл после завершения создания отображений
 * Он используется только для статичной верстки
 */
?>
<!-- Main Nav (begin) -->
<nav class="mainNav">
    <ul class="mainNav-list">
        <?php foreach ($mainNav as $item) : ?>
            <?php if ($item['url'] == $this->here) : ?>
                <li class="mainNav-item is-active"><?= $item['text']; ?></li>
            <?php else : ?>
                <li class="mainNav-item">
                    <a href="<?= $item['url']; ?>" title="<?= $item['title']; ?>"><?= $item['text']; ?></a>
                </li>
            <?php endif; ?>
            <li class="mainNav-divider">zok</li>
        <?php endforeach; ?>
    </ul>
</nav>
<!-- Main Nav (end) -->