<?php
/**
 * Мобильное меню
 */
    $left_items = $this->requestAction("/menus/items/top_left");
    $left_items = $this->Public->menuItems($left_items);

    $right_items = $this->requestAction("/menus/items/top_right");
    $right_items = $this->Public->menuItems($right_items);

    $items = array_merge($left_items, $right_items);
?>
<!-- Mobile Nav (begin) -->
<nav class="mobileNav">
    <div class="mobileNav-inner">
        <ul class="mobileNav-list">
            <?php foreach ($items as $item) : ?>
                <?php if ($item['url'] == $this->here) : ?>
                    <li class="mobileNav-item is-active"><?= $item['text']; ?></li>
                <?php else : ?>
                    <li class="mobileNav-item">
                        <a href="<?= $item['url']; ?>" title="<?= $item['title']; ?>"><?= $item['text']; ?></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>

        <a class="mobileNav-close icon-close js-close-menu"></a>
    </div>
</nav>
<!-- Mobile Nav (end) -->