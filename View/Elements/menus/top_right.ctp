<?php
/**
 * Элемент отображения Правой части верхнего меню
 */
    $items = $this->requestAction("/menus/items/top_right");
    $items = $this->Public->menuItems($items);
?>
<?= $this->element("menus/top", array('items' => $items)); ?>
