<?php
/**
 * Элемент отображения Левой части верхнего меню
 */
    $items = $this->requestAction("/menus/items/top_left");
    $items = $this->Public->menuItems($items);
?>
<?= $this->element("menus/top", array('items' => $items)); ?>
