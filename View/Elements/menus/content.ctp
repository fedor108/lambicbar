<?php
/**
 * Навигация в разделе
 */
?>
<!-- Contentmenu (begin) -->
<div class="contentmenu
    <?= isset($horizontal) && $horizontal ? 'contentmenu--horizontal' : '' ?>
    <?= (!empty($responsive)) ? 'contentmenu--responsive' : '' ?>">
    <ul class="contentmenu-list">
        <?php foreach ($menu as $item) : ?>
            <?php if($this->here == $item['slug']): ?>
                <li class="contentmenu-item is-active"><?= $item['title']; ?></li>
            <?php else : ?>
                <li class="contentmenu-item">
                    <a href="<?= $item['slug'] ?>"><?= $item['title']; ?></a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>

    <?php if (!empty($responsive)) : ?>
        <!-- <div class="select contentmenu-select select--filter"> -->
        <div id="content-menu-widget" class="select2-widget--transparent contentmenu-select select--filter">
            <select class="js-select-page" data-width="100%">
                <?php foreach ($menu as $item) : ?>
                    <option value="<?= $item['slug']; ?>" <?= ($this->here == $item['slug']) ? 'selected' : ''; ?>>
                        <?= $item['title']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    <?php endif ?>
</div>
<!-- Contentmenu (end) -->