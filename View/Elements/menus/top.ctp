<?php
/**
 * Общий элемент отображения частей верхнего меню
 */
?>
<!-- Main Nav (begin) -->
<nav class="mainNav">
    <ul class="mainNav-list">
        <?php foreach ($items as $item) : ?>
            <?php if ($this->here == $item['url']) : ?>
                <li class="mainNav-item is-active"><?= $item['text']; ?></li>
            <?php elseif (false !== strpos($this->here, $item['url'])) : ?>
                <li class="mainNav-item is-active">
                    <a href="<?= $item['url']; ?>" title="<?= $item['title']; ?>"><?= $item['text']; ?></a>
                </li>
            <?php else : ?>
                <li class="mainNav-item">
                    <a href="<?= $item['url']; ?>" title="<?= $item['title']; ?>"><?= $item['text']; ?></a>
                </li>
            <?php endif; ?>
            <li class="mainNav-divider"></li>
        <?php endforeach; ?>
    </ul>
</nav>
<!-- Main Nav (end) -->
