<?php
/**
 * Отправка отзывов в текстовом формате
 */
    $labels = array(
        'name' => 'Имя',
        'phone' => 'Телефон',
        'email' => 'Эл. почта',
        'type' => 'Тип обращения',
        'reason' => 'Причина обращения',
        'address' => 'Адрес ресторана',
        'visit_date' => 'Дата визита',
        'body' => 'Сообщение',
        'ip' => 'IP адрес отправителя'
    );
    foreach ($labels as $key => $title) {
        if (!empty($Report[$key])) {
            echo "{$title}:\r\n{$Report[$key]}\r\n\r\n";
        }
    }
