<?php
/**
 * Отправка отзывов в текстовом формате
 */
    $labels = array(
        'position' => 'Вакансия',
        'education' => 'Образование',
        'language' => 'Знание языков',
        'experience' => 'Опыт работы',
        'name' => 'ФИО',
        'phone' => 'Телефон',
        'email' => 'Эл. почта',
        'ip' => 'IP адрес отправителя'
    );
    foreach ($labels as $key => $title) {
        if (!empty($Resume[$key])) {
            echo "{$title}:\r\n{$Resume[$key]}\r\n\r\n";
        }
    }
