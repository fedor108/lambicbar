<?php if (!empty($users)) : ?>
<?= $this->element("admin/title", array('title' => "Список пользователей")); ?>
<table class="table table-hover well">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Имя</th>
        <th>Группа</th>
        <th class="actions">Действия</th>
    </tr>
    </thead>
    <tbody class="node-userren">
        <?php foreach ($users as $user): ?>
        <tr data-id="<?= h($user['User']['id']); ?>">
            <td><?= h($user['User']['id']); ?>&nbsp;</td>
            <td>
                <?= $this->Html->link($user['User']['username'], array('action' => 'edit', $user['User']['id'])); ?>
                &nbsp;
            </td>
            <td><?= h($user['Group']['name']); ?>&nbsp;</td>
            <td class="actions">
                <?= $this->Form->postLink('Удалить',
                    array('action' => 'delete', $user['User']['id']),
                    array('confirm' => "Действительно, удалить пользователя #{$user['User']['id']} - {$user['User']['username']}?")
                ); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

<a href="/admin/users/add" class="btn btn-success"><i class="fa fa-plus"></i> Добавить пользователя</a>
