<?= $this->element("admin/title", array('title' => 'Новый пользователь', '')); ?>
<?= $this->Form->create('User', array('class' => "well")); ?>
    <?= $this->Form->input('username', array('label' => 'Имя', 'help' => 'Имя входа в админ-панель.')); ?>
    <?= $this->Form->input('password', array('label' => 'Пароль', 'help' => 'Необходимо задать пароль.', 'value' => '')); ?>
    <?= $this->Form->input('group_id', array('label' => 'Группа', 'help' => 'Группы для разграничения доступа.')); ?>
    <div class="form-actions text-align-left">
        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Сохранить</button>
    </div>
<?= $this->Form->end(); ?>
