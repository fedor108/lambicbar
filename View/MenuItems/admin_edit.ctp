<?= $this->element("admin/title", array('title' => array($this->request->data['MenuItem']['title'], 'Редактирование'))); ?>
<?= $this->Form->create('MenuItem', array('action' => "save")); ?>
    <fieldset class="well">
        <?= $this->Form->hidden("id"); ?>
        <?= $this->Form->input("node_id", array('label' => 'Целевой узел', 'help' => 'Страница, на которую будет вести ссылка в меню.')); ?>
        <?= $this->Form->input("url", array('label' => 'Целевой адрес', 'help' => 'Адрес, на который будет вести ссылка в меню. Если не выбран целевой узел.')); ?>
        <?= $this->Form->input("title", array('label' => 'Текст', 'help' => 'Текст пункта меню, если он отличается от заголовка целевого узла.')); ?>
        <?= $this->Form->input("link_title", array('label' => 'Всплывающая подсказка', 'help' => 'Отображается при наведении курсора на пункт меню.')); ?>
        <?= $this->Form->input("target", array(
            'label' => 'Как открывать ссылку',
            'help' => 'В новом окне нужно открывать ссылки, ведущие на другие сайты.',
            'options' => array('' => "В этом же окне", '_blank' => "В новом окне")
        )); ?>
        <div class="form-actions text-align-left">
            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Сохранить</button>
            <?= $this->Html->link("Вернуться к меню",
                array('controller' => "menus", 'action' => "edit", $this->request->data['MenuItem']['menu_id']),
                array(
                    'class'   => "btn btn-default",
                    'escape' => false,
                )
            ); ?>
            <?= $this->Html->link("Удалить",
                array('action' => 'delete', $this->request->data['MenuItem']['id']),
                array(
                    'confirm' => "Удалить пункт меню {$this->request->data['MenuItem']['title']}?",
                    'class'   => "pull-right btn btn-danger",
                    'escape' => false,
                )
            ); ?>
        </div>
    </fieldset>
<?= $this->Form->end(); ?>
