<?php
/**
 * Отправка SMS о бронировании столика
 */
$labels = array(
    'id' => 'Номер брони',
    'address' => 'Адрес ресторана',
    'visit_date' => 'Дата визита',
    'visit_time' => 'Время визита',
    'persons' => 'Количество гостей',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'email' => 'Эл. почта',
    'body' => 'Сообщение',
);
echo $title . PHP_EOL;
echo $labels['id'] . ': ' . $data['id'] . PHP_EOL;
echo $labels['address'] . ': ' . PHP_EOL . $data['address'] . PHP_EOL;
echo $labels['visit_date'] . ': ' . PHP_EOL . $data['visit_date'] . PHP_EOL;
echo $labels['visit_time'] . ': ' . $data['visit_time'] . PHP_EOL;
echo $labels['persons'] . ': ' . $data['persons'] . PHP_EOL;
echo $labels['name'] . ': ' . PHP_EOL . $data['name'] . PHP_EOL;
echo $labels['phone'] . ': ' . PHP_EOL . $data['phone'] . PHP_EOL;
echo $labels['email'] . ': ' . PHP_EOL . $data['email'] . PHP_EOL;
echo $labels['body'] . ': ' . PHP_EOL . $data['body'];
