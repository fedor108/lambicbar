<?= $this->element("admin/title", array('title' => array($this->request->data['Menu']['title'], 'Редактирование'))); ?>
<?= $this->Form->create('Menu', array('action' => "save")); ?>
    <fieldset class="well">
        <?= $this->Form->hidden("id"); ?>
        <?= $this->Form->input("title", array('label' => 'Название', 'help' => 'Отображается только в админ-панели.')); ?>
        <?= $this->Form->input("name", array('label' => 'Латинское обозначение', 'help' => 'Используется для отображения меню на сайте.')); ?>
        <div class="form-actions text-align-left">
            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Сохранить</button>
            <?= $this->Html->link("Удалить",
                array('action' => 'delete', $this->request->data['Menu']['id']),
                array(
                    'confirm' => "Удалить {$this->request->data['Menu']['title']}?",
                    'class'   => "pull-right btn btn-danger",
                    'escape' => false,
                )
            ); ?>
        </div>
    </fieldset>
    <div class="row">
        <div class="col-md-6"><?= $this->element("admin/menu_items"); ?></div>
        <div class="col-md-6"><?= $this->element("admin/menu_item_add"); ?></div>
    </div>
<?= $this->Form->end(); ?>
