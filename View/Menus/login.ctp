<div class="row">

    <form action="/login" method="post" id="login-form" class="smart-form client-form well no-padding col-md-6">

        <header>Вход</header>

        <fieldset>

            <?= $this->Session->flash(); ?>

            <section>
                <label class="label">Логин</label>
                <label class="input"> <i class="icon-append fa fa-user"></i>
                    <input type="text" name="data[User][username]" id="UserUsername" required="required">
                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Введите имя входа</b></label>
            </section>

            <section>
                <label class="label">Пароль</label>
                <label class="input"> <i class="icon-append fa fa-lock"></i>
                    <input type="password" name="data[User][password]" id="UserPassword" required="required">
                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Введите пароль</b> </label>
            </section>

        </fieldset>

        <footer>
            <button type="submit" class="btn btn-primary">Вход</button>
        </footer>

    </form>

</div>
