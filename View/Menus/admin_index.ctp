<?php if (!empty($menus)) : ?>
<?= $this->element("admin/title", array('title' => "Навигационные меню сайта")); ?>
<table class="table table-hover well">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Название</th>
        <th>Обозначение</th>
        <th class="actions">Действия</th>
    </tr>
    </thead>
    <tbody class="node-userren">
        <?php foreach ($menus as $menu): ?>
        <tr data-id="<?= h($menu['Menu']['id']); ?>">
            <td><?= h($menu['Menu']['id']); ?>&nbsp;</td>
            <td>
                <?= $this->Html->link($menu['Menu']['title'], array('action' => 'edit', $menu['Menu']['id'])); ?>
                &nbsp;
            </td>
            <td><?= h($menu['Menu']['name']); ?>&nbsp;</td>
            <td class="actions">
                <?= $this->Form->postLink('Удалить',
                    array('action' => 'delete', $menu['Menu']['id']),
                    array('confirm' => "Удалить меню #{$menu['Menu']['name']} : {$menu['Menu']['title']}?")
                ); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

<a href="/admin/menus/add" class="btn btn-success"><i class="fa fa-plus"></i> Добавить меню</a>
