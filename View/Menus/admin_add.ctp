<?= $this->element("admin/title", array('title' => 'Новое меню', '')); ?>
<?= $this->Form->create('Menu', array('class' => "well")); ?>
    <?= $this->Form->input('title', array('label' => 'Название', 'help' => 'Отображается только в админ-панели.')); ?>
    <?= $this->Form->input('name', array('label' => 'Латинское обозначение', 'help' => 'Используется при отображении меню на сайте.')); ?>
    <div class="form-actions text-align-left">
        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Создать</button>
    </div>
<?= $this->Form->end(); ?>
