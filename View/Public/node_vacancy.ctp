<?php
/**
 * Страница Вакансии
 */
	$menu = $this->Public->contentMenu($node['ParentNode']['ChildNode']);
?>

<div class="page page--about">
	<header class="page-header">
		<h1 class="page-title"><?= $node['ParentNode']['title']; ?></h1>
		<?php if (!empty($menu)) : ?>
			<?= $this->element('menus/content', array('menu' => $menu, 'horizontal' => true)); ?>
		<?php endif; ?>
	</header>
	<div class="page-inner">
		<div class="text">
			<?= $node['named']['Text']['main']['value']; ?>
			<p>&nbsp;</p>
			<p>
				<a href="mailto:<?= str_replace(' ', '', $this->Session->read('Setting.vacancy_emails.value')); ?>" class="btn btn--default vacancy-sendmail-btn">Откликнуться на вакансию</a>
			</p>
		</div>
		<form class="form vacancy-form" id="vacancy-form" method="post"  enctype="multipart/form-data">
			<h2 class="form-title">Анкета соискателя</h2>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Вакансия</label>
				</div>
				<div class="form-col form-col--control">
					<div class="checkbox-group">
						<?php $positions = explode("\r\n", $this->Session->read('Setting.vacancy_positions.value')); ?>
						<?php foreach( $positions as $i => $position ):?>
							<div class="checkbox">
								<input id="appeal[<?=$i+1?>]" type="checkbox" data-parsley-mincheck="1" name="data[Resume][position][<?=$i?>]">
								<label for="appeal[<?=$i+1?>]"><?=$position?></label>
							</div>
						<?php endforeach;?>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Образование</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Учебные заведения, ступень" name="data[Resume][education]" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Знание языков</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Пример: английский, французский, немецкий" name="data[Resume][language]" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Опыт работы</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Специальности и стаж работы" name="data[Resume][experience]" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">ФИО</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" data-parsley-minwords="3" placeholder="Фамилия Имя Отчество" name="data[Resume][name]" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Телефон</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="+7" data-parsley-phone data-type="phone" name="data[Resume][phone]" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Эл. почта</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" data-parsley-type="email" data-type="email" placeholder="Пример: lambic@mail.ru" name="data[Resume][email]" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Ваша фотография</label>
				</div>
				<div class="form-col form-col--alignright form-col--control">
					<input class="file-input" type="file" data-requirements=".jpg, .png не более 5Мб" data-title="Загрузить фото" name="data[Resume][image]">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Ваше резюме</label>
				</div>
				<div class="form-col form-col--alignright form-col--control">
					<input class="file-input" type="file" data-requirements=".pdf, .doc не более 1Мб" data-title="Загрузить резюме" name="data[Resume][document]">
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn--default">Отправить</button>
				<span class="afterSubmit" id="result"><?= $this->Session->flash(); ?></span>
			</div>
		</form>
	</div>
</div>