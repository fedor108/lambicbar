<?php
/**
 * Страницы в разделе О нас
 */
    $menu = $this->Public->contentMenu($node['ParentNode']['ChildNode']);
?>
<div class="page page--contacts">
    <header class="page-header">
        <h1 class="page-title"><?= $node['ParentNode']['title']; ?></h1>
        <?php echo $this->element('menus/content', array('menu' => $menu, 'horizontal' => true)); ?>
    </header>
    <div class="page-inner">
        <aside class="aside">
            <div id="contacts-map" class="map js-map" data-address="<?= $node['named']['Line']['address']['value']; ?>"></div>
        </aside>
        <div class="content">
            <div class="contacts-list">
                <div class="contacts-list-column">
                    <?php $items = explode("\r\n\r\n", $node['named']['Text']['contacts_left']['value']); ?>
                    <?= $this->element("contacts_list_column", array('items' => $items)); ?>
                </div>

                <div class="contacts-list-column">
                    <?php $items = explode("\r\n\r\n", $node['named']['Text']['contacts_right']['value']); ?>
                    <?= $this->element("contacts_list_column", array('items' => $items)); ?>
                </div>
            </div>
        </div>
    </div>
</div>