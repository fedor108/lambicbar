<?php
/**
 * Отображение раздела Пиво
 */
    $menu = $this->Public->contentPlaceMenu($sections, $place);
?>
<div class="page page--beer">
    <div class="page-inner">
        <aside class="aside">
            <h1 class="page-title"><?= $node['ParentNode']['title']; ?></h1>
            <?= $this->element("places"); ?>
            <?= $this->element("menus/content", array('menu' => $menu, 'responsive' => true)); ?>
        </aside>
        <div class="content">
            <div class="reference">
                <div class="outline">
                    <div class="outline-inner">
                        <div class="reference-content">
                            <h3><?= $node['Node']['title']; ?></h3>
                            <?= $node['named']['Text']['main']['value']; ?>
                            <i class="icon-branch"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="beer-list">
                <?php foreach($children as $item): ?>
                    <div class="beer">
                        <div class="beer-photo">
                            <img src="<?= $item['named']['Gallery']['main']['Image'][0]['src'] ?>">
                        </div>
                        <div class="beer-details">
                            <?php
                                if (empty($item['named']['Flag']['new']['value'])) {
                                    $class = "beer-title";
                                } else {
                                    $class = "beer-title beer--new";
                                }
                            ?>
                            <h2 class="<?= $class; ?>"><?= $item['Node']['title']; ?></h2>
                            <div class="beer-desc">
                                <?= str_replace("\r\n", "\r\n<br>", $item['named']['Text']['announce']['value']); ?>
                            </div>
                            <div class="beer-specifications">
                                <ul>
                                    <?php if (!empty($item['Beer']['type'])) : ?>
                                        <li><b>Тип.</b> <?= $item['Beer']['type'] ?></li>
                                    <?php endif; ?>
                                    <?php if (!empty($item['Beer']['alcohol'])) : ?>
                                        <li><b>Алкоголь.</b> <?= $item['Beer']['alcohol']; ?> %</li>
                                    <?php endif; ?>
                                    <?php if (!empty($item['Beer']['density'])) : ?>
                                        <li><b>Плотность.</b> <?= $item['Beer']['density']; ?> %</li>
                                    <?php endif; ?>
                                    <?php if (!empty($item['Beer']['fermentation'])) : ?>
                                        <li><b>Брожение.</b> <?= $item['Beer']['fermentation'] ?></li>
                                    <?php endif; ?>
                                    <?php if (!empty($item['Beer']['color'])) : ?>
                                        <li><b>Цвет.</b> <?= $item['Beer']['color'] ?></li>
                                    <?php endif; ?>
                                    <?php if (!empty($item['Beer']['brewery'])) : ?>
                                        <li><b>Пивоварня.</b> <?= $item['Beer']['brewery'] ?></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="beer-price">
                                <ul>
                                    <?php $item['Price'] = $this->Public->price($item['Price'], $item['Connection'], $place['Node']['id']); ?>
                                    <?php foreach($item['Price'] as $price): ?>
                                        <li>
                                            <span class="volume"><?= $price['quantity'] ?> <?= $price['units'] ?></span>
                                            <?= $price['value'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>