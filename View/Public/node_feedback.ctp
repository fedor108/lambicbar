<div class="page">
	<header class="page-header">
		<h1 class="page-title"><?= $node['Node']['title']; ?></h1>
	</header>
	<div class="page-inner">
		<form class="form" id="report-form" enctype="multipart/form-data">
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Вид обращения</label>
				</div>
				<div class="form-col form-col--control">
					<div class="select2-widget--transparent">
						<select class="js-feedback-select" name="data[Report][type]" data-width="100%">
							<?php $types = explode("\r\n", $this->Session->read('Setting.feedback_types.value')); ?>
							<?php foreach ($types as $n => $type) : ?>
								<option><?= $type; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Имя</label>
				</div>
				<div class="form-col form-col--control">
					<input name="data[Report][name]" class="text-input" placeholder="Введите имя, чтобы мы знали как к вам обращаться" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Эл. почта</label>
				</div>
				<div class="form-col form-col--control">
					<input name="data[Report][email]" class="text-input" type="text" data-parsley-type="email" data-type="email" placeholder="Пример: lambic@mail.ru" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Телефон</label>
				</div>
				<div class="form-col form-col--control">
					<input name="data[Report][phone]" class="text-input" data-parsley-phone="true" type="text" data-type="phone" placeholder="+7">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Адрес ресторана</label>
				</div>
				<div class="form-col form-col--control">
					<div class="select2-widget--transparent">
						<select class="js-feedback-select" name="data[Report][address]" data-width="100%">
							<?php $address = $this->requestAction("/public/address"); ?>
							<?php foreach ($address as $n => $node) : ?>
								<option><?= $node['named']['Text']['address']['value']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Дата посещения</label>
				</div>
				<div class="form-col form-col--control form-col--date">
					<button class="btn btn--light" data-datepicker="datepicker">Выбрать дату</button>
					<input name="data[Report][visit_date]" id="datepicker" class="datepicker" value="<?= $this->Public->ruToday() ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Причина обращения</label>
				</div>
				<div class="form-col form-col--control">
					<div class="select2-widget--transparent">
						<select class="js-feedback-select" name="data[Report][reason]" data-width="100%">
							<?php $reasons = explode("\r\n", $this->Session->read('Setting.feedback_reasons.value')); ?>
							<?php foreach ($reasons as $n => $reason) : ?>
								<option><?= $reason; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Комментарий</label>
					<p id="textarea-limit" class="form-limit"></p>
				</div>
				<div class="form-col form-col--control">
					<textarea name="data[Report][body]" class="textarea" data-limit="600" data-label="textarea-limit" required></textarea>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Прикрепить файл</label>
				</div>
				<div class="form-col form-col--alignright form-col--control">
					<input name="data[Report][upload]" class="file-input" type="file" data-requirements=".jpg, .png не более 5Мб" name="file">
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn--default">Отправить</button>
				<span class="afterSubmit" id="result"></span>
			</div>
		</form>
	</div>
</div>