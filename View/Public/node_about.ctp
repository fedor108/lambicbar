<?php
/**
 * Главная страница раздела О нас
 */
	$menu = $this->Public->contentMenu($node['ChildNode']);
?>
<div class="page page--about">
	<header class="page-header">
		<h1 class="page-title"><?= $node['Node']['title']; ?></h1>
		<?= $this->element('menus/content', array('menu' => $menu, 'horizontal' => true)); ?>
	</header>
	<div class="page-inner">
		<div class="text">
			<?= $node['named']['Text']['main']['value']; ?>
		</div>
	</div>
</div>