<?php
/**
 * XML карта сайта для поисковиков
 */
    echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<?php foreach ($nodes as $item) : ?>
    <url>
        <loc>http://<?= $_SERVER['SERVER_NAME'] . $item['Node']['slug']; ?></loc>
        <lastmod><?= $item['Node']['modified']; ?></lastmod>
    </url>
<?php endforeach; ?>
</urlset>