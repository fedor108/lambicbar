<div class="page page--home">
    <!-- Home Gallery (begin) -->
    <div class="gallery gallery--home"
        data-fit="cover"
        data-margin="0"
        data-width="100%"
        data-nav="false"
        data-height="100%"
        data-minheight="700"
        data-loop="true"
        data-auto="false"
        data-autoplay="4000">
        <?php foreach ($node['named']['Gallery']['main']['Image'] as $image) : ?>
            <?php if (!empty($image['src'])) : ?>
                <div class="gallery-slide" data-img="<?= $image['src']; ?>">
                    <h2 class="gallery-slide-title"><?= $image['title']; ?></h2>
                    <p class="gallery-slide-desc"><?= $image['description']; ?></p>
                    <p><a class="gallery-slide-link" href="<?= $image['url']; ?>">Подробнее</a></p>
                    <a class="gallery-slide-anchor" href="#intro"></a>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <!-- Home Gallery (end) -->

    <div class="page-inner">
        <!-- Intro (begin)  -->
        <section id="intro" class="intro text">
            <div class="intro-column">
                <h1 class="title-hero"><?= $node['Node']['title']; ?></h1>
            </div>
            <div class="intro-column">
                <?php $text = explode("\r\n", $node['named']['Text']['announce']['value']); ?>
                <p class="lead"><?= $text[0]; ?></p>
                <p><?= $text[1]; ?></p>
            </div>
            <div class="intro-column">
                <p><?= $text[2]; ?></p>
            </div>
        </section>
        <!-- Intro (end) -->
    </div>
</div>