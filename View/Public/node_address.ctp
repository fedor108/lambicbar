<?php
/**
 * Отображение страницы Рестораны
 */
    $restaurants = $this->Public->restaurants($children);
?>

<div class="page page--address">
    <header class="page-header">
        <h1 class="page-title"><?= $node['Node']['title']; ?></h1>
    </header>
    <div class="page-inner">
        <div class="restaurant-list">
            <?php foreach($restaurants as $key => $item): ?>
                <div class="restaurant">
                    <div class="restaurant-preview" style="background-image: url(<?= $this->Image->thumb($item['preview'], 'mid'); ?>);">
                        <a href="<?= $item['preview'] ?>" data-show-gallery="restaurant-<?= $key ?>">
                            <img src="/images/camera.svg">
                            <p><span>Фото ресторана</span></p>
                        </a>
                    </div>
                    <h3><?= $item['address'] ?></h3>
                    <ul>
                        <?php if (isset($item['email'])): ?>
                            <li>
                                Почта:
                                <a href="mailto:<?= $item['email'] ?>">
                                    <?= $item['email'] ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (isset($item['phone'])): ?>
                            <li>
                                Телефон:
                                <a class="tel" href="tel:<?= $item['phone'] ?>">
                                    <?= $item['phone'] ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>

                    <h3>Режим работы</h3>
                    <ul class="workinghours">
                        <li><?= $item['schedule'][0] ?></li>
                        <?php if (!empty($item['schedule'][1])) : ?>
                            <li><?= $item['schedule'][1] ?></li>
                        <?php endif;?>
                    </ul>

                    <ul>
                        <?php if ($item['veranda']): ?>
                            <li><i class="icon-veranda"></i> Есть веранда</li>
                        <?php endif; ?>
                        <?php if (!empty($item['menu'])) : ?>
                            <li>
                                <i class="icon-book"></i>
                                <a href="<?= $item['menu']; ?>">Меню ресторана</a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($item['parking'])) : ?>
                            <li>
                                <i class="icon-parking"></i>
                                <a target="_blank" href="<?= $item['parking']; ?>">Парковки</a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($item['map'])) : ?>
                            <li>
                                <i class="icon-location"></i>
                                <a target="_blank" href="<?= $item['map']; ?>">Карта</a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($item['panorama']['url'])): ?>
                            <li>
                                <i class="icon-pano"></i>
                                <a href="<?=$item['panorama']['url']?>" target="<?=$item['panorama']['target']?>">Панорама</a>
                            </li>
                        <?php endif; ?>
                    </ul>

                    <div id="restaurant-<?= $key ?>"
                        class="gallery gallery--fullscreen"
                        data-auto="false"
                        data-width="100%"
                        data-height="100%"
                        data-fit="cover"
                        data-nav="false"
                        data-thumbmargin="0"
                        data-thumbborderwidth="5"
                        data-loop="true"
                        data-margin="0"
                        data-trackpad="false"
                        data-keyboard="true"
                        data-click="true">
                        <?php foreach($item['photos'] as $image): ?>
                            <a href="<?= $image['src'] ?>"></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div id="restoranesMap"></div>

    <script>
        var placesfromserver = <?= $this->Public->mapJson($restaurants); ?>;
        /*
        [
            // {
            //     city: "Москва",
            //     address: "ул. Мясницкая д. 40А",
            //     infoWindowData: {
            //         "address": "ул. Мясницкая д. 40А",
            //         "email": "info@lambicbar.ru",
            //         "phone": "8 (495) 542-00-37",
            //         "workinghours": {
            //             "weekdays": "Пн-Чт: с 10:00 до 23:00",
            //             "weekend": "Пт-Вс: с 10:00 до 3:00"
            //         }
            //     }
            // },
            {
                city: "Москва",
                address: "ул. Долгоруковская д. 19 стр. 7",
                infoWindowData: {
                    "address": "ул. Долгоруковская д. 19 стр. 7",
                    "email": "info@lambicbar.ru",
                    "phone": "8 (495) 542-00-37",
                    "workinghours": {
                        "weekdays": "Пн-Чт: с 10:00 до 23:00",
                        "weekend": "Пт-Вс: с 10:00 до 3:00"
                    }
                }
            },
            {
                city: "Москва",
                address: "ул. Воронцово Поле д. 11/32",
                infoWindowData: {
                    "address": "ул. Воронцово Поле д. 11/32",
                    "email": "info@lambicbar.ru",
                    "phone": "8 (495) 542-00-37",
                    "workinghours": {
                        "weekdays": "Пн-Чт: с 10:00 до 23:00",
                        "weekend": "Пт-Вс: с 10:00 до 3:00"
                    }
                }
            },
            {
                city: "Москва",
                address: "ул. Неверовскаго д. 10",
                infoWindowData: {
                    "address": "ул. Неверовскаго д. 10",
                    "email": "info@lambicbar.ru",
                    "phone": "8 (495) 542-00-37",
                    "workinghours": {
                        "weekdays": "Пн-Чт: с 10:00 до 23:00",
                        "weekend": "Пт-Вс: с 10:00 до 3:00"
                    }
                }
            }
        ];*/
    </script>

    <script id="infoWindowTemplate" type="text/template">
        <div class="infoWindow">
            <h3><%= address %></h3>
            <ul>
                <% if (email) { %>
                    <li>
                        Почта:
                        <a href="mailto:<%= email %>">
                            <%= email %>
                        </a>
                    </li>
                <% } %>
                <% if (phone) { %>
                    <li>
                        Телефон:
                        <a class="tel" href="tel:<%= phone %>">
                            <%= phone %>
                        </a>
                    </li>
                <% } %>
            </ul>

            <h3>Режим работы</h3>
            <ul class="workinghours">
                <li><%= workinghours[0] %></li>
                <% if (workinghours[1]) { %>
                    <li><%= workinghours[1] %></li>
                <% } %>
            </ul>
        </div>
    </script>
</div>