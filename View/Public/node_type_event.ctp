<?php
/**
 * Отображение отдельной страницы мероприятия
 */
?>
<div class="page page--events-view">
    <div class="page-inner">
        <aside class="aside">
            <a class="goback js-sticky" href="<?= $node['ParentNode']['slug']; ?>">
                <i class="icon-anchor-left"></i>
                <span>Вернуться в&nbsp;события</span>
            </a>
        </aside>
        <div class="content">
            <div class="text">
                <time><?= $node['Node']['event_duration']; ?></time>
                <h1><?= $node['Node']['title']; ?></h1>
                <div class="gallery"
                    data-fit="cover"
                    data-margin="0"
                    data-width="100%"
                    data-nav="false"
                    data-height="100%"
                    data-maxheight="384">
                    <?php foreach ($node['named']['Gallery']['main']['Image'] as $image) : ?>
                        <img src="<?= $image['src']; ?>">
                    <?php endforeach; ?>
                </div>
                <?= $node['named']['Text']['main']['value']; ?>
            </div>

            <div class="social-likes">
                <h3 class="social-likes__title">Рассказать друзьям</h3>
                <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
                <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
            </div>
        </div>
    </div>
</div>