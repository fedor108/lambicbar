<?php
/**
 * Обычная страница
 */
	$menu = $this->Public->contentMenu($node['ChildNode']);
?>

<div class="page page--about">
	<header class="page-header">
		<h1 class="page-title"><?= $node['Node']['title']; ?></h1>
		<?php if (!empty($menu)) : ?>
			<?= $this->element('menus/content', array('menu' => $menu, 'horizontal' => true)); ?>
		<?php endif; ?>
	</header>
	<div class="page-inner">
		<div class="text">
			<?= $node['named']['Text']['main']['value']; ?>
		</div>
	</div>
</div>