<?php
/**
 * Отображение раздела Меню
 */
    $menu = $this->Public->contentPlaceMenu($sections, $place);
    // $products = $this->Public->menuProducts($children);
?>
<div class="page page--menu">
    <div class="page-inner">
        <aside class="aside">
            <h1 class="page-title"><?= $node['ParentNode']['title']; ?></h1>
            <?= $this->element("places"); ?>
            <?= $this->element("menus/content", array('menu' => $menu, 'responsive' => true)); ?>
        </aside>
        <div class="content">
            <div class="menu">
                <?php if (!empty($sub_sections)) : ?>
                    <?php foreach ($sub_sections as $sub_section) : ?>
                        <?= $this->element('dish_list', array(
                            'children' => $sub_section['children'],
                            'sub_section_title' => $sub_section['Node']['title']
                        )); ?>
                    <?php endforeach; ?>
                <?php endif;?>
                <?= $this->element('dish_list', array('children' => $children)); ?>
            </div>
        </div>
    </div>
</div>

<div id="menu-gallery"
    class="gallery gallery--fullscreen gallery--alignbottom"
    data-auto="false"
    data-width="100%"
    data-height="100%"
    data-fit="cover"
    data-nav="false"
    data-thumbmargin="0"
    data-thumbborderwidth="5"
    data-margin="0"
    data-trackpad="false"
    data-keyboard="true"
    data-click="false"
    data-loop="true">
    <?php foreach($children as $item): ?>
        <?php if (!empty($item['named']['Gallery']['main']['Image'][0]['src'])) : ?>
            <div class="gallery-slide" data-img="<?= $item['named']['Gallery']['main']['Image'][0]['src']; ?>">
                <div class="gallery-slide-footer">
                    <div class="dish">
                        <h2 class="dish-title fotorama__select"><?= $item['Node']['title']; ?></h2>
                        <?php if(isset($item['announce'])): ?>
                            <p class="dish-desc fotorama__select"><?= $item['named']['Text']['announce']['value']; ?></p>
                        <?php endif; ?>
                        <span class="separator"></span>
                        <ul class="dish-price fotorama__select">
                            <?= $this->element("price", array('prices' => $item['Price'])); ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
