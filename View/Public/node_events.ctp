<?php
/**
 * Отображение главной страницы раздела Мероприятия
 */
    $pagination = $this->Paginator->numbers(array(
        'tag' => 'li',
        'separator' => '',
        'currentClass' => 'is-active',
    ));
    if (!empty($nextPage)) {
        $pagination .= $this->Paginator->next('Дальше', array(
            'tag' => 'li',
            'separator' => '',
            'class' => 'next',
        ));
    }
    $pagination = str_replace("/{$node['Node']['id']}", $node['Node']['slug'], $pagination);
?>
<div class="page page--events">
    <header class="page-header">
        <h1 class="page-title"><?= $node['Node']['title']; ?></h1>
    </header>
    <div class="page-inner">
        <aside class="aside">
            <div class="banner" style="background-image: url(<?= $last_event['named']['Gallery']['main']['Image'][0]['src']; ?>);">
                <div class="banner-inner">
                    <time class="banner-time"><?= $last_event['Node']['event_duration']; ?></time>
                    <h2 class="banner-title"><?= $last_event['Node']['title']; ?></h2>
                    <p class="banner-desc"><?= $last_event['named']['Text']['announce']['value']; ?></p>
                    <a class="banner-link-more" href="<?= $last_event['Node']['slug']; ?>">Подробнее</a>
                </div>
                <a class="banner-link" href="<?= $last_event['Node']['slug']; ?>"></a>
            </div>
        </aside>
        <div class="content">
            <div class="events-list">
                <?php foreach ($children as $n => $item): ?>
                    <div class="event">
                        <time class="event-date"><?= $item['Node']['event_duration'] ?></time>
                        <h2 class="event-title">
                            <a href="<?= $item['Node']['slug'] ?>"><?= $item['Node']['title'] ?></a>
                        </h2>
                    </div>
                <?php endforeach; ?>

                <!-- Pagination (begin) -->
                <nav class="pagination">
                    <ul><?= $pagination; ?></ul>
                </nav>
                <!-- Pagination (end) -->
            </div>
        </div>
    </div>
</div>