<div class="page">
    <header class="page-header">
        <h1 class="page-title"><?= $node['Node']['title']; ?></h1>
    </header>
    <div class="page-inner">
        <form class="form js-validate-form" action="/orders/add" id="order-form" enctype="multipart/form-data">
            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Выберите ресторан</label>
                </div>
                <div class="form-col form-col--control">
                    <div class="select2-widget--transparent">
                        <select class="js-feedback-select" name="data[Order][address]" data-width="100%" required="">
                            <?php $address = $this->requestAction("/public/address"); ?>
                            <option value="">Выберите ресторан</option>
                            <?php foreach ($address as $n => $node) : ?>
                                <option value="<?= $node['named']['Text']['address']['value']; ?>"><?= $node['named']['Text']['address']['value']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Дата визита</label>
                </div>
                <div class="form-col form-col--control form-col--date">
                    <button class="btn btn--light" data-datepicker="datepicker">Выберите дату</button>
                    <input name="data[Order][visit_date]" id="datepicker" class="datepicker" value="<?= $this->Public->ruToday() ?>">
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Время визита</label>
                </div>
                <div class="form-col form-col--control">
                    <input name="data[Order][visit_time]" data-type="time" class="text-input" placeholder="Например: 19:30" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Количество гостей</label>
                </div>
                <div class="form-col form-col--control">
                    <input name="data[Order][persons]" data-type="persons" class="text-input" placeholder="Например: 5 человек" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Имя</label>
                </div>
                <div class="form-col form-col--control">
                    <input name="data[Order][name]" class="text-input" placeholder="Введите имя, чтобы мы знали как к вам обращаться" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Телефон</label>
                </div>
                <div class="form-col form-col--control">
                    <input name="data[Order][phone]" class="text-input" data-parsley-phone="true" type="text" data-type="phone" placeholder="+7" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Эл. почта</label>
                </div>
                <div class="form-col form-col--control">
                    <input name="data[Order][email]" class="text-input" type="text" data-parsley-type="email" data-type="email" placeholder="Пример: lambic@mail.ru">
                </div>
            </div>

            <div class="form-row">
                <div class="form-col form-col--label">
                    <label class="form-label">Пожелания к брони</label>
                    <p id="textarea-limit" class="form-limit"></p>
                </div>
                <div class="form-col form-col--control">
                    <textarea name="data[Order][body]" class="textarea" data-limit="600" data-label="textarea-limit"></textarea>
                </div>
            </div>

            <div class="form-submit">
                <button type="submit" class="btn btn--default">Отправить</button>
                <span class="afterSubmit" id="result"></span>
            </div>
        </form>
    </div>
</div>

<div id="popup-orderSuccess" class="popup">
    <div class="popup-dialog">
        <img class="dialog-illustration" src="/images/order-success.png" alt="Принято" />
        <h2 class="dialog-title">Ваша бронь принята</h2>
        <p class="dialog-desc">Мы свяжемся с вами для подтверждения резерва.</p>
        <button class="dialog-close icon-close" type="button" data-dismiss="modal"></button>
    </div>
</div>
