<?php
	$content_menu = array(
		array(
			'title' => 'Салаты и закуски',
			'slug' => '/pages/menu/salads'
		),
		array(
			'title' => 'Мидии и рыба',
			'slug' => '/pages/menu/fish'
		),
		array(
			'title' => 'Пивные закуски',
			'slug' => '/pages/menu/forbeer'
		),
		array(
			'title' => 'Супы',
			'slug' => '/pages/menu/soup'
		),
		array(
			'title' => 'Блюда из мяса и птицы',
			'slug' => '/pages/menu/meat'
		),
		array(
			'title' => 'Стейки и гриль',
			'slug' => '/pages/menu/grill'
		),
		array(
			'title' => 'Гарниры и овощи',
			'slug' => '/pages/menu'
		),
		array(
			'title' => 'Десерты',
			'slug' => '/pages/menu/deserty'
		)
	);

	$menu = array(
		array(
			'title' => 'Листья салата фризе',
			'slug' => '',
			'price' => '520',
			'desc' => 'с жареным сыром камамбер',
			'photo' => '/images/temp/menu-item-1.jpg',
			'markAsNew' => true
		),
		array(
			'title' => 'Битые баклажаны',
			'slug' => '',
			'price' => array(
				array(
					'price' => '365',
					'volume' => '250'
				),
				array(
					'price' => '600',
					'volume' => '500'
				)
			),
			'desc' => 'с листьями айсберга на гриле',
			'markAsNew' => true
		),
		array(
			'title' => 'Салат из сладких томатов',
			'slug' => '',
			'price' => '500',
			'desc' => 'с крымским луком и орехами кешью',
			'photo' => '/images/temp/menu-item-3.jpg',
			'markAsNew' => true
		),
		array(
			'title' => 'Мильфей',
			'slug' => '',
			'price' => '500',
			'desc' => 'из кабачка в темпуре с креветками',
			'photo' => '/images/temp/menu-item-4.jpg'
		),
		array(
			'title' => 'Теплый салат с морепродуктами',
			'slug' => '',
			'price' => '750',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Салат с дикой уткой',
			'slug' => '',
			'price' => '550',
			'desc' => 'с листьями шпината в соусе «Терияки»',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Соте из баклажанов',
			'slug' => '',
			'price' => '450',
			'desc' => 'с хрустящим яйцом пашот и пармской ветчиной',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Салат Цезарь',
			'slug' => '',
			'price' => '500 - 700',
			'desc' => 'c курицей или креветками',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Фирменный салат «Ламбик»',
			'slug' => '',
			'price' => '750',
			'desc' => 'cо сладкими томатами, мраморной говядиной гриль, свежим базиликом и кинзой в остром соусе',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Салат с куриной печенью',
			'slug' => '',
			'price' => '480',
			'desc' => 'c белыми грибами, рукколой и трюфельным маслом',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Моцарелла',
			'slug' => '',
			'price' => '450',
			'desc' => 'с рукколой и соусом « Песто»',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Тар-Тар из тунца с авокадо',
			'slug' => '',
			'price' => '650',
			'desc' => 'в соусе «Nobu»',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Тар-тар из копченого лосося',
			'slug' => '',
			'price' => '650',
			'desc' => 'с хрустящими тостами и домашней сметаной',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Тар-тар из говядины',
			'slug' => '',
			'price' => '680',
			'desc' => 'с маринованными белыми грибами',
			'photo' => '/images/temp/menu-item-1.jpg',
			'markAsNew' => true
		),
		array(
			'title' => 'Маринованное филе сельди',
			'slug' => '',
			'price' => '350',
			'desc' => 'с укропом и луком, сервируется молодым картофелем и тостами',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Ассорти сыров',
			'slug' => '',
			'price' => '700',
			'desc' => 'Камамбер, Горгонзола, Маасдам, Грана-Подана',
			'photo' => '/images/temp/menu-item-1.jpg'
		),
		array(
			'title' => 'Мясной сет',
			'slug' => '',
			'price' => '750',
			'desc' => 'Пармская ветчина, салями «Милано», шпег, брезаола с зернистой горчицей и хлебными тостами',
			'photo' => '/images/temp/menu-item-1.jpg'
		)
	);
?>

<div class="page page--menu">
	<div class="page-inner">
		<aside class="aside">
			<h1 class="page-title">Меню</h1>
			<div class="select-backgrounded">
				<select class="select js-select-page">
					<option value="/pages/voronsovo-pole/beer">Воронцово Поле</option>
					<option value="/pages/lebedinnoe-pole/beer">Лебединное Поле</option>
				</select>
			</div>
			<?php echo $this->element('contentmenu', array('contentmenu' => $content_menu)); ?>
		</aside>
		<div class="content">
			<div class="menu">
				<h2 class="menu-subtitle">Стейки и гриль</h2>
				<div class="dish-list">
					<?php $indexOfDishWithPhoto = 0; ?>
					<?php foreach($menu as $item): ?>
						<div class="dish <?= isset($item['markAsNew']) && $item['markAsNew'] == true ? 'dish--new' : '' ?>">
							<h2 class="dish-title">
								<?php if(isset($item['photo'])): ?>
									<a href="<?= $item['photo'] ?>"
										data-show-gallery="menu-gallery"
										data-gallery-start="<?= $indexOfDishWithPhoto ?>">
										<span><?= $item['title'] ?></span>
									</a>
									<?php $indexOfDishWithPhoto++; ?>
								<?php else: ?>
									<span><?= $item['title'] ?></span>
								<?php endif; ?>
							</h2>
							<?php if(isset($item['desc'])): ?>
								<p class="dish-desc"><?= $item['desc'] ?></p>
							<?php endif; ?>
							<ul class="dish-price">
								<?php if (is_array($item['price'])): ?>
									<?php foreach($item['price'] as $price): ?>
										<li>
											<span class="volume"><?= $price['volume'] ?> г</span>
											<?= $price['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
										</li>
									<?php endforeach;?>
								<?php else: ?>
									<li>
										<?= $item['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endforeach; ?>
				</div>

				<h2 class="menu-subtitle">Гарниры и овощи</h2>
				<div class="dish-list">
					<?php $indexOfDishWithPhoto = 0; ?>
					<?php foreach($menu as $item): ?>
						<div class="dish <?= isset($item['markAsNew']) && $item['markAsNew'] == true ? 'dish--new' : '' ?>">
							<h2 class="dish-title">
								<?php if(isset($item['photo'])): ?>
									<a href="<?= $item['photo'] ?>"
										data-show-gallery="menu-gallery"
										data-gallery-start="<?= $indexOfDishWithPhoto ?>">
										<span><?= $item['title'] ?></span>
									</a>
									<?php $indexOfDishWithPhoto++; ?>
								<?php else: ?>
									<span><?= $item['title'] ?></span>
								<?php endif; ?>
							</h2>
							<?php if(isset($item['desc'])): ?>
								<p class="dish-desc"><?= $item['desc'] ?></p>
							<?php endif; ?>
							<ul class="dish-price">
								<?php if (is_array($item['price'])): ?>
									<?php foreach($item['price'] as $price): ?>
										<li>
											<span class="volume"><?= $price['volume'] ?> г</span>
											<?= $price['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
										</li>
									<?php endforeach;?>
								<?php else: ?>
									<li>
										<?= $item['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="menu-gallery"
	class="gallery gallery--fullscreen gallery--alignbottom"
	data-auto="false"
	data-width="100%"
    data-height="100%"
    data-fit="cover"
    data-nav="false"
    data-thumbmargin="0"
    data-thumbborderwidth="5"
    data-margin="0"
    data-trackpad="false"
    data-keyboard="true"
    data-click="false"
    data-loop="true">
	<?php foreach($menu as $item): ?>
		<?php if (isset($item['photo'])): ?>
			<div class="gallery-slide" data-img="<?= $item['photo'] ?>">
				<div class="gallery-slide-footer">
	                <div class="dish">
	                    <h2 class="dish-title fotorama__select"><?= $item['title'] ?></h2>
	                    <?php if(isset($item['desc'])): ?>
	                    	<p class="dish-desc fotorama__select"><?= $item['desc'] ?></p>
	                	<?php endif; ?>
	                    <span class="separator"></span>
	                    <ul class="dish-price fotorama__select">
							<?php if (is_array($item['price'])): ?>
								<?php foreach($item['price'] as $price): ?>
									<li>
										<span class="volume"><?= $price['volume'] ?> г</span>
										<?= $price['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
									</li>
								<?php endforeach;?>
							<?php else: ?>
								<li>
									<?= $item['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
								</li>
							<?php endif; ?>
						</ul>
	                </div>
	            </div>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
</div>