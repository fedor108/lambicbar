<?php
	$contentmenu = array(
		array(
			'title' => 'История',
			'slug' => '/pages/about/history'
		),
		array(
			'title' => 'Сотрудничество',
			'slug' => '/pages/about/cooperation'
		),
		array(
			'title' => 'Вакансии',
			'slug' => '/pages/about/vacancy'
		),
		array(
			'title' => 'Контакты',
			'slug' => '/pages/about/contact'
		)
	);
?>

<div class="page page--about">
	<header class="page-header">
		<h1 class="page-title">О нас</h1>
		<?php echo $this->element('contentmenu', array('contentmenu' => $contentmenu, 'horizontal' => true)); ?>
	</header>
	<div class="page-inner">
		<div class="text">
			<h2>Мы предлагаем</h2>
			<ul>
				<li>Качественное обучение профессии.;</li>
				<li>Совмещение работы с учебой;</li>
				<li>Карьерный рост (Ваш карьерный рост не будет стоять на месте, ведь практически все управляющие ресторанов, начинали свою карьеру, работая официантом или бариста.);</li>
				<li>Стабильный и достойный доход;</li>
				<li>Социальный пакет (Вас ждет молодой коллектив, корпоративные мероприятия, стильная униформа,бесплатное питание, оплачиваемый отпуск, больничные листы, оформление по ТК РФ).</li>
			</ul>
			<p>&nbsp;</p>
			<p>
				<a href="mailto:jobs@lambicbar.ru" class="btn btn--default vacancy-sendmail-btn">Откликнутся на вакансию</a>
			</p>
		</div>
		<form class="form vacancy-form">
			<h2 class="form-title">Анкета соискателя</h2>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Вид обращения</label>
				</div>
				<div class="form-col form-col--control">
					<div class="checkbox-group">
						<div class="checkbox">
							<input id="appeal[1]" type="checkbox" data-parsley-mincheck="1" name="appeal[]">
							<label for="appeal[1]">Официант</label>
						</div>
						<div class="checkbox">
							<input id="appeal[2]" type="checkbox" data-parsley-mincheck="1" name="appeal[]">
							<label for="appeal[2]">Специалист по напиткам</label>
						</div>
						<div class="checkbox">
							<input id="appeal[3]" type="checkbox" data-parsley-mincheck="1" name="appeal[]">
							<label for="appeal[3]">Хостес</label>
						</div>
						<div class="checkbox">
							<input id="appeal[4]" type="checkbox" data-parsley-mincheck="1" name="appeal[]">
							<label for="appeal[4]">Повар</label>
						</div>
						<div class="checkbox">
							<input id="appeal[5]" type="checkbox" data-parsley-mincheck="1" name="appeal[]">
							<label for="appeal[5]">Другая вакансия</label>
						</div>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Образование</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Учебные заведения, ступень">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Знание языков</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Пример: английский, французский, немецкий">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Опыт работы</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Специальности и стаж работы">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">ФИО</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" data-parsley-minwords="3" placeholder="Фамилия Имя Отчество">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Телефон</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="+7" data-parsley-phone data-type="phone">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Эл. почта</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" data-parsley-type="email" data-type="email" placeholder="Пример: lambic@mail.ru">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Ваша фотография</label>
				</div>
				<div class="form-col form-col--alignright form-col--control">
					<input class="file-input" type="file" data-requirements=".jpg, .png не более 5Мб" data-title="Загрузить фото">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Ваше резюме</label>
				</div>
				<div class="form-col form-col--alignright form-col--control">
					<input class="file-input" type="file" data-requirements=".pdf, .doc не более 1Мб" data-title="Загрузить резюме">
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn--default">Отправить</button>
			</div>
		</form>
	</div>
</div>