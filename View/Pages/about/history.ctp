<?php
	$contentmenu = array(
		array(
			'title' => 'История',
			'slug' => '/pages/about/history'
		),
		array(
			'title' => 'Сотрудничество',
			'slug' => '/pages/about/cooperation'
		),
		array(
			'title' => 'Вакансии',
			'slug' => '/pages/about/vacancy'
		),
		array(
			'title' => 'Контакты',
			'slug' => '/pages/about/contact'
		)
	);
?>

<div class="page page--about">
	<header class="page-header">
		<h1 class="page-title">О нас</h1>
		<?php echo $this->element('contentmenu', array('contentmenu' => $contentmenu, 'horizontal' => true)); ?>
	</header>
	<div class="page-inner">
		<div class="text">
			<h2>Дизайна еще нет</h2>
		</div>
	</div>
</div>