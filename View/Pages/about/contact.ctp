<?php
	$contentmenu = array(
		array(
			'title' => 'История',
			'slug' => '/pages/about/history'
		),
		array(
			'title' => 'Сотрудничество',
			'slug' => '/pages/about/cooperation'
		),
		array(
			'title' => 'Вакансии',
			'slug' => '/pages/about/vacancy'
		),
		array(
			'title' => 'Контакты',
			'slug' => '/pages/about/contact'
		)
	);
?>

<div class="page page--contacts">
	<header class="page-header">
		<h1 class="page-title">О нас</h1>
		<?php echo $this->element('contentmenu', array('contentmenu' => $contentmenu, 'horizontal' => true)); ?>
	</header>
	<div class="page-inner">
		<aside class="aside">
			<div id="map" class="map" data-address="Москва, ул. Осенняя, д. 11 "></div>
		</aside>
		<div class="content">
			<div class="contacts-list">
				<div class="contacts-list-column">
					<div class="contact-item">
						<h2>Адрес офиса</h2>
						<ul>
							<li>121609, Москва, ул. Осенняя, д. 11 </li>
							<li>тел.: <a class="tel" href="tel:+74957811690 ">+7 (495) 781-16-90 </a></li>
							<li>факс: <a class="tel" href="tel:+74957811691">+7 (495) 781-16-91 </a></li>
							<li><a href="mailto:info@coffeemania.ru">info@coffeemania.ru</a></li>
						</ul>
					</div>

					<div class="contact-item">
						<h2>Трудоустройство</h2>
						<ul>
							<li>тел.: <a class="tel" href="tel:+79851370313">+7 (985) 137-03-13</a></li>
							<li>с 9.00 до 18.00 (кроме выходных)</li>
						</ul>
					</div>

					<div class="contact-item">
						<h2>Контакты для прессы</h2>
						<ul>
							<li>тел.: <a class="tel" href="tel:+74957811690">+7 (495) 781-16-90</a></li>
							<li>факс: <a class="tel" href="tel:+74957811691">+7 (495) 781-16-91</a></li>
							<li><a href="mailto:pr@coffemania.ru">pr@coffemania.ru</a></li>
						</ul>
					</div>
				</div>

				<div class="contacts-list-column">
					<div class="contact-item">
						<h2>Аренда</h2>
						<ul>
							<li>тел.: <a class="tel" href="tel:+79851370313">+7 (985) 137-03-13</a></li>
							<li>с 9.00 до 18.00 (кроме выходных)</li>
						</ul>
					</div>

					<div class="contact-item">
						<h2>По вопросам размещения печатных изданий в ресторанах</h2>
						<ul>
							<li>тел.: <a class="tel" href="tel:+79263771860 ">+7 (926) 377-18-60 </a></li>
							<li><a href="mailto:kofepressa@gmail.com ">kofepressa@gmail.com </a></li>
							<li><a href="http://www.pressdirect.ru">www.pressdirect.ru</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>