<?php
	$restaurants = array(
		array(
			'preview' => '/images/temp/restaurant-preview.jpg',
			'address' => 'ул. Воронцово Поле д. 11/32',
			'email' => 'info@lambicbar.ru',
			'phone' => '8 (495) 542-00-37',

			'workinghours' => array(
				'weekdays' => 'Пн-Чт: с 10:00 до 23:00',
				'weekend' => 'Пт-Вс: с 10:00 до 3:00'
			),
			'options' => array(
				'veranda' => true,
				'menu' => true,
				'parking' => true,
				'map' => true
			),
			'photos' => array(
				'/images/temp/menu-item-1.jpg',
				'/images/temp/menu-item-2.jpg',
				'/images/temp/menu-item-3.jpg',
				'/images/temp/menu-item-4.jpg'
			)
		),
		array(
			'preview' => '/images/temp/restaurant-preview.jpg',
			'address' => 'ул. Мясницкая д. 40А',
			'email' => 'info@lambicbar.ru',
			'phone' => '8 (495) 542-00-37',

			'workinghours' => array(
				'weekdays' => 'Пн-Чт: с 10:00 до 23:00',
				'weekend' => 'Пт-Вс: с 10:00 до 3:00'
			),
			'options' => array(
				'veranda' => true,
				'menu' => true,
				'parking' => true,
				'map' => true
			),
			'photos' => array(
				'/images/temp/menu-item-2.jpg',
				'/images/temp/menu-item-1.jpg',
				'/images/temp/menu-item-3.jpg',
				'/images/temp/menu-item-4.jpg'
			)
		),
		array(
			'preview' => '/images/temp/restaurant-preview.jpg',
			'address' => 'ул. Долгоруковская д. 19 стр. 7',
			'email' => 'info@lambicbar.ru',
			'phone' => '8 (495) 542-00-37',

			'workinghours' => array(
				'weekdays' => 'Пн-Чт: с 10:00 до 23:00',
				'weekend' => 'Пт-Вс: с 10:00 до 3:00'
			),
			'options' => array(
				'veranda' => true,
				'menu' => true,
				'parking' => true,
				'map' => true
			),
			'photos' => array(
				'/images/temp/menu-item-3.jpg',
				'/images/temp/menu-item-2.jpg',
				'/images/temp/menu-item-1.jpg',
				'/images/temp/menu-item-4.jpg'
			)
		),
		array(
			'preview' => '/images/temp/restaurant-preview.jpg',
			'address' => 'ул. Неверовского д. 10',
			'email' => 'info@lambicbar.ru',
			'phone' => '8 (495) 542-00-37',

			'workinghours' => array(
				'weekdays' => 'Пн-Чт: с 10:00 до 23:00',
				'weekend' => 'Пт-Вс: с 10:00 до 3:00'
			),
			'options' => array(
				'veranda' => true,
				'menu' => true,
				'parking' => true,
				'map' => true
			),
			'photos' => array(
				'/images/temp/menu-item-4.jpg',
				'/images/temp/menu-item-2.jpg',
				'/images/temp/menu-item-3.jpg',
				'/images/temp/menu-item-1.jpg'
			)
		)
	);
?>

<div class="page page--address">
	<header class="page-header">
		<h1 class="page-title">Адреса ресторанов</h1>
	</header>
	<div class="page-inner">
		<div class="restaurant-list">
			<?php foreach($restaurants as $key => $item): ?>
				<div class="restaurant">
					<div class="restaurant-preview" style="background-image: url(<?= $item['preview'] ?>);">
						<a href="<?= $item['preview'] ?>" data-show-gallery="restaurant-<?= $key ?>">
							<img src="/images/camera.svg">
							<p><span>Фото ресторана</span></p>
						</a>
					</div>
					<h3><?= $item['address'] ?></h3>
					<ul>
						<?php if (isset($item['email'])): ?>
							<li>
								Почта:
								<a href="mailto:<?= $item['email'] ?>">
									<?= $item['email'] ?>
								</a>
							</li>
						<?php endif; ?>
						<?php if (isset($item['phone'])): ?>
							<li>
								Телефон:
								<a class="tel" href="tel:<?= $item['phone'] ?>">
									<?= $item['phone'] ?>
								</a>
							</li>
						<?php endif; ?>
					</ul>

					<h3>Режим работы</h3>
					<ul class="workinghours">
						<li><?= $item['workinghours']['weekdays'] ?></li>
						<li><?= $item['workinghours']['weekend'] ?></li>
					</ul>

					<ul>
						<?php if ($item['options']['veranda']): ?>
							<li><i class="icon-veranda"></i> Есть веранда</li>
						<?php endif; ?>
						<?php if ($item['options']['menu']): ?>
							<li>
								<i class="icon-book"></i>
								<a href="/pages/menu">Меню ресторана</a>
							</li>
						<?php endif; ?>
						<?php if ($item['options']['parking']): ?>
							<li>
								<i class="icon-parking"></i>
								<a target="_blank" href="https://maps.yandex.ru/?text=%D0%9C%D0%B0%D0%BB%D1%8B%D0%B9+%D0%A7%D0%B5%D1%80%D0%BA%D0%B0%D1%81%D1%81%D0%BA%D0%B8%D0%B9+%D0%BF%D0%B5%D1%80.%2C+%D0%B4%D0%BE%D0%BC+2%2C+%D0%BF%D0%B0%D1%80%D0%BA%D0%BE%D0%B2%D0%BA%D0%B0">Парковки</a>
							</li>
						<?php endif; ?>
						<?php if ($item['options']['map']): ?>
							<li>
								<i class="icon-location"></i>
								<a href="">Карта</a>
							</li>
						<?php endif; ?>
						<?php if ($item['options']['panorama']): ?>
							<li>
								<i class="icon-pano"></i>
								<a href="">Панорама</a>
							</li>
						<?php endif; ?>

					</ul>


					<div id="restaurant-<?= $key ?>"
						class="gallery gallery--fullscreen"
						data-auto="false"
						data-width="100%"
				        data-height="100%"
				        data-fit="cover"
				        data-nav="false"
				        data-thumbmargin="0"
				        data-thumbborderwidth="5"
				        data-margin="0"
				        data-trackpad="false"
				        data-keyboard="true"
				        data-click="false">
						<?php foreach($item['photos'] as $url): ?>
							<a href="<?= $url ?>"></a>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div id="restoranesMap"></div>

	<script id="infoWindowTemplate" type="text/template">
		<div class="infoWindow">
			<h3><%= address %></h3>
			<ul>
				<?php if (isset($item['email'])): ?>
					<li>
						Почта:
						<a href="mailto:<?= $item['email'] ?>">
							<?= $item['email'] ?>
						</a>
					</li>
				<?php endif; ?>
				<?php if (isset($item['phone'])): ?>
					<li>
						Телефон:
						<a class="tel" href="tel:<?= $item['phone'] ?>">
							<?= $item['phone'] ?>
						</a>
					</li>
				<?php endif; ?>
			</ul>

			<h3>Режим работы</h3>
			<ul class="workinghours">
				<li><?= $item['workinghours']['weekdays'] ?></li>
				<li><?= $item['workinghours']['weekend'] ?></li>
			</ul>
		</div>
	</script>
</div>