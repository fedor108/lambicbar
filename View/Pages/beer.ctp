<?php
    $content_menu = array(
        array(
            'title' => 'Разливное пиво',
            'slug' => '/pages/beer'
        ),
        array(
            'title' => 'Lambic beers ',
            'slug' => '/pages/beer/fish'
        ),
        array(
            'title' => 'Oud brune',
            'slug' => '/pages/beer/forbeer'
        ),
        array(
            'title' => 'Trappist beers',
            'slug' => '/pages/beer/trappist'
        ),
        array(
            'title' => 'Blonde',
            'slug' => '/pages/beer/blonde'
        ),
        array(
            'title' => 'Pils, pale lager',
            'slug' => '/pages/beer/pils'
        ),
        array(
            'title' => 'Blanche',
            'slug' => '/pages/beer/blanche'
        ),
        array(
            'title' => 'Brune',
            'slug' => '/pages/beer/brune'
        ),
        array(
            'title' => 'Amber ales',
            'slug' => '/pages/beer/amber'
        ),
        array(
            'title' => 'Stronge ales',
            'slug' => '/pages/beer/stronge'
        ),
        array(
            'title' => 'Winter beers',
            'slug' => '/pages/beer/winter'
        ),
        array(
            'title' => 'Fruit beers',
            'slug' => '/pages/beer/fruit'
        )
    );

    $beers = array(
        array(
            'title' => 'Jacobins Gueuze',
            'desc' => 'Пиво, производимое методом самопроизвольного брожения за счет находящихся в воздухе микроорганизмов, дрожжевых культур, которых в процессе может участвовать более сотни. Основное брожение длится неделю. Затем пиво выдерживается до трёх лет в винных бочках.',
            'photo' => '/images/temp/beer-1.jpg',
            'price' => array(
                array(
                    'price' => '365',
                    'volume' => '250'
                ),
                array(
                    'price' => '600',
                    'volume' => '500'
                )
            ),
            'specifications' => array(
                'type' => 'Классический гёз',
                'alcohol' => '5,5 %',
                'density' => '14%',
                'fermentation' => 'Самопроизвольное',
                'color' => 'Светло-янтарный',
                'brewery' => 'Bockor Brewery'
            )
        ),
        array(
            'title' => 'Cuvee des Jacobins',
            'desc' => 'Пиво, производимое методом самопроизвольного брожения за счет находящихся в воздухе микроорганизмов, дрожжевых культур, которых в процессе может участвовать более сотни. Основное брожение длится неделю. Затем пиво выдерживается до трёх лет в винных бочках.',
            'photo' => '/images/temp/beer-2.jpg',
            'price' => array(
                array(
                    'price' => '365',
                    'volume' => '250'
                ),
                array(
                    'price' => '600',
                    'volume' => '500'
                )
            ),
            'specifications' => array(
                'type' => 'Классический гёз',
                'alcohol' => '5,5 %',
                'density' => '14%',
                'fermentation' => 'Самопроизвольное',
                'color' => 'Светло-янтарный',
                'brewery' => 'Bockor Brewery'
            )
        ),
        array(
            'title' => 'Orval',
            'desc' => 'Пиво, производимое методом самопроизвольного брожения за счет находящихся в воздухе микроорганизмов, дрожжевых культур, которых в процессе может участвовать более сотни. Основное брожение длится неделю. Затем пиво выдерживается до трёх лет в винных бочках.',
            'photo' => '/images/temp/beer-3.jpg',
            'price' => array(
                array(
                    'price' => '365',
                    'volume' => '250'
                ),
                array(
                    'price' => '600',
                    'volume' => '500'
                )
            ),
            'specifications' => array(
                'type' => 'Классический гёз',
                'alcohol' => '5,5 %',
                'density' => '14%',
                'fermentation' => 'Самопроизвольное',
                'color' => 'Светло-янтарный',
                'brewery' => 'Bockor Brewery'
            )
        )
    );
?>



<div class="page page--beer">
    <div class="page-inner">
        <aside class="aside">
        <h1 class="page-title">Пиво</h1>
            <div class="select-backgrounded">
                <div class="custom-select">
                    <select class="select js-select-page">
                        <option value="/pages/voronsovo-pole/beer">Воронцово Поле</option>
                        <option value="/pages/lebedinnoe-pole/beer">Лебединное Поле</option>
                    </select>
                </div>
            </div>
            <?php
                echo $this->element('contentmenu', array(
                    'contentmenu' => $content_menu,
                    'responsive' => true)
                );
            ?>
        </aside>
        <div class="content">
            <div class="reference">
                <div class="outline">
                    <div class="outline-inner">
                        <div class="reference-content">
                            <h3>Lambic beers</h3>
                            <p>Пиво, производимое методом естественного самопроизвольного брожения за счет находящихся в воздухе микроорганизмов – диких дрожжевых культур, которых в процессе может участвовать более сотни. Основное брожение длится неделю. Затем пиво выдерживается до трёх лет в винных бочках.</p>
                            <i class="icon-branch"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="beer-list">
                <?php foreach($beers as $item): ?>
                    <div class="beer">
                        <div class="beer-photo">
                            <img src="<?= $item['photo'] ?>">
                        </div>
                        <div class="beer-details">
                            <h2 class="beer-title beer--new"><?= $item['title'] ?></h2>
                            <p class="beer-desc"><?= $item['desc'] ?></p>
                            <div class="beer-specifications">
                                <ul>
                                    <li><b>Тип.</b> <?= $item['specifications']['type'] ?></li>
                                    <li><b>Алкоголь.</b> <?= $item['specifications']['alcohol'] ?></li>
                                    <li><b>Плотность.</b> <?= $item['specifications']['density'] ?></li>
                                    <li><b>Брожение.</b> <?= $item['specifications']['fermentation'] ?></li>
                                    <li><b>Цвет.</b> <?= $item['specifications']['color'] ?></li>
                                    <li><b>Пивоварня.</b> <?= $item['specifications']['brewery'] ?></li>
                                </ul>
                            </div>
                            <div class="beer-price">
                                <ul>
                                    <?php foreach($item['price'] as $price): ?>
                                        <li>
                                            <span class="volume"><?= $price['volume'] ?> мл</span>
                                            <?= $price['price'] ?><span class="hs">&nbsp;</span><i class="icon-rubl"></i>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>