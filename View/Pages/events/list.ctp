<?php
	$events = array(
		array(
			'title' => 'Ресторан Lambic представляет новый аббатский эль Grimbergen',
			'date' => '31 января 2015 — 1 февраля 2015',
			'slug' => '/pages/events/view'
		),
		array(
			'title' => 'Анонс новой линейки бутылочного пива',
			'date' => '19 февраля 2014',
			'slug' => '/pages/events/view'
		),
		array(
			'title' => 'Пивная линейка ресторана «Lambic» пополняется бельгийским подлинно аббатским (монастырским) пивом Leffe Blonde (Леффе светлое).',
			'date' => '4 сентября 2012',
			'slug' => '/pages/events/view'
		),
		array(
			'title' => 'Анонс новой линейки бутылочного пива',
			'date' => '19 февраля 2014',
			'slug' => '/pages/events/view'
		)
	);
?>

<div class="page page--events">
	<header class="page-header">
		<h1 class="page-title">События</h1>
	</header>
	<div class="page-inner">
		<aside class="aside">
			<div class="banner" style="background-image: url(/images/temp/banner.jpg);">
				<div class="banner-inner">
					<time class="banner-time">2 октября 2014</time>
					<h2 class="banner-title">Выступление кавер-группы «Dance Bomb»</h2>
					<p class="banner-desc">В брассерии на Долгоруковской.</p>
					<a class="banner-link-more" href="">Подробнее</a>
				</div>
				<a class="banner-link" href=""></a>
			</div>
		</aside>
		<div class="content">
			<div class="events-list">
				<?php foreach ($events as $event): ?>
					<div class="event">
						<time class="event-date"><?= $event['date'] ?></time>
						<h2 class="event-title">
							<a href="<?= $event['slug'] ?>"><?= $event['title'] ?></a>
						</h2>
					</div>
				<?php endforeach; ?>

				<!-- Pagination (begin) -->
			    <nav class="pagination">
			        <ul>
			            <li class="is-active">1</li>
			            <li><a href="" title="">2</a></li>
			            <li><a href="" title="">3</a></li>
			            <li><a href="" title="">4</a></li>
			            <li><a href="" title="">5</a></li>
			            <li class="next"><a href="" title="">Дальше</a></li>
			        </ul>
			    </nav>
			    <!-- Pagination (end) -->
			</div>
		</div>
	</div>
</div>