<div class="page page--events-view">
	<div class="page-inner">
		<aside class="aside">
			<a class="goback js-sticky" href="/pages/events/list">
				<i class="icon-anchor-left"></i>
				<span>Вернуться в&nbsp;события</span>
			</a>
		</aside>
		<div class="content">
			<div class="text">
				<time>19 февраля 2014</time>
				<h1>Анонс новой линейки бутылочного пива</h1>
				<div class="gallery"
					data-fit="cover"
					data-margin="0"
					data-width="100%"
					data-nav="false"
					data-height="100%"
					data-maxheight="384">
					<img src="/images/temp/menu-item-1.jpg">
					<img src="/images/temp/menu-item-2.jpg">
					<img src="/images/temp/menu-item-3.jpg">
				</div>
				<p>Наше собрание насчитывает более 50 популярных и редких сортов — это и великолепные ламбики, давно ставшие визитной карточкой настоящего бельгийского пива, здесь же и славное своими традициями траппистское пиво старых монастырей. В нашей бутылочной коллекции - пиво на любой вкус: белое, янтарное, темное, фруктовое; низового, верхового, двойного, тройного брожения; легкое, крепкое, сезонное, а так же пиво с натуральными ягодами и соками.</p>
				<p>По своему направлению все бутылочное пиво сгрупированно<br> в нашем меню в следующие разделы:</p>
				<ul>
					<li>Lambic Beers — пиво, производимое методом самопроизвольного брожения;</li>
					<li>Oud Brune (Flemish Red) — ламбики из дубовых бочек;</li>
					<li>Trappist Beers — монастырское пиво избранных монастырей;</li>
					<li>Blonde — светлое пиво;</li>
					<li>Pils, Pale Lager — легкое пиво.</li>
				</ul>
			</div>

			<div class="social-likes">
				<h3 class="social-likes__title">Рассказать друзьям</h3>
				<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
				<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
			</div>
		</div>
	</div>
</div>