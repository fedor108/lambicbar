<div class="page page--home">
	<!-- Home Gallery (begin) -->
	<div class="gallery gallery--home"
		data-fit="cover"
		data-margin="0"
		data-width="100%"
		data-nav="false"
		data-height="100%"
		data-minheight="700"
		data-loop="true"
		data-auto="false">
		<div class="gallery-slide" data-img="/images/temp/menu-item-1.jpg">
			<h2 class="gallery-slide-title">Фестиваль мидий</h2>
			<p class="gallery-slide-desc">в брассерии на Долгоруковской</p>
			<p><a class="gallery-slide-link" href="/pages/events/view">Подробнее</a></p>
			<a class="gallery-slide-anchor" href="#intro"></a>
		</div>
		<div class="gallery-slide" data-img="/images/temp/menu-item-2.jpg">
			<h2 class="gallery-slide-title">Море морепродуктов</h2>
			<p class="gallery-slide-desc">в брассерии на Мясницкой</p>
			<p><a class="gallery-slide-link" href="/pages/events/view">Подробнее</a></p>
			<a class="gallery-slide-anchor" href="#intro"></a>
		</div>
		<div class="gallery-slide" data-img="/images/temp/menu-item-3.jpg">
			<h2 class="gallery-slide-title">Новые сорта пива</h2>
			<p class="gallery-slide-desc">в брассерии на Неверовского</p>
			<p><a class="gallery-slide-link js-scroll-to" href="#intro">Подробнее</a></p>
			<a class="gallery-slide-anchor" href="#intro"></a>
		</div>
	</div>
	<!-- Home Gallery (end) -->

	<div class="page-inner">
		<!-- Intro (begin)  -->
		<section id="intro" class="intro text">
			<div class="intro-column">
				<h1 class="title-hero">Брассерия Lambic</h1>
			</div>
			<div class="intro-column">
				<p class="lead">Брассерия… как много в этом звуке слилось для истинного пивного гурмана!</p>
				<p>Демократичная атмосфера заведения с легким налетом элитарности, феерия вкусов пенящегося напитка и добротная душевность закусок.</p>
			</div>
			<div class="intro-column">
				<p>«Ламбик» – так называется сеть ресторанов-брассерий, открывшихся за последние 3 года в Москве. Мы выбрали это название, не мудрствуя лукаво и не вынуждая потенциальных посетителей теряться в догадках по поводу основного составляющего меню.</p>
			</div>
		</section>
		<!-- Intro (end) -->
	</div>
</div>