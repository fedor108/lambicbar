<?php $this->layout = '404'; ?>

<div class="page page--404">
	<div class="page-inner">
		<h1>404</h1>
		<p>Страница не найдена,<br> тут только пиво.</p>
		<a href="/"><span>На главную</span></a>
		<img src="/images/logo--404.svg">
	</div>
</div>