<div class="page">
	<header class="page-header">
		<h1 class="page-title">Написать отзыв</h1>
	</header>
	<div class="page-inner">
		<form class="form">
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Вид обращения</label>
				</div>
				<div class="form-col form-col--control">
					<select class="select">
						<option value="1">Довольный гость</option>
						<option value="2">Пьяный гость</option>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Имя</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" placeholder="Введите имя чтобы мы знали как к вам обращаться" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Эл. почта</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" type="text" data-parsley-type="email" data-type="email" placeholder="Пример: lambic@mail.ru" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Телефон</label>
				</div>
				<div class="form-col form-col--control">
					<input class="text-input" data-parsley-phone="true" type="text" data-type="phone" placeholder="+7">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Адрес ресторана</label>
				</div>
				<div class="form-col form-col--control">
					<select class="select">
						<option>ул. Долгоруковская 19, стр. 7</option>
						<option>ул. Долгоруковская 19, стр. 8</option>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Дата посещения</label>
				</div>
				<div class="form-col form-col--control form-col--date">
					<button class="btn btn--light" data-datepicker="datepicker">Выбрать дату</button>
					<input id="datepicker" class="datepicker" value="<?= date("j F Y") ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Причина обращения</label>
				</div>
				<div class="form-col form-col--control">
					<select class="select">
						<option>Общее впечатление</option>
						<option>Недоразумение</option>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Комментарий</label>
					<p id="textarea-limit" class="form-limit"></p>
				</div>
				<div class="form-col form-col--control">
					<textarea class="textarea" data-limit="256" data-label="textarea-limit" required></textarea>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col--label">
					<label class="form-label">Прикрепить файл</label>
				</div>
				<div class="form-col form-col--alignright form-col--control">
					<input class="file-input" type="file" data-requirements=".jpg, .png не более 5Мб" name="file">
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn--default">Отправить</button>
			</div>
		</form>
	</div>
</div>