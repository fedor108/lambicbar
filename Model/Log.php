<?php
App::uses('AppModel', 'Model');
/**
 * Log Model
 *
 * @property Model $Model
 * @property User $User
 */
class Log extends AppModel
{

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'model_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'description' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'diff' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => array('id', 'username', 'group_id'),
            'order' => ''
        ),
    );

/**
 * После чтения данных получаем связанные данные
 */
    public function afterFind($results, $primary = false)
    {
        foreach ($results as &$result) {
            if (empty($result['Log']['model_name'])) continue;
            $alias = $result['Log']['model_name'];
            App::import('Model', $alias);
            $model = new $alias();
            $data = $model->find('first', array(
                'conditions' => array($alias.'.id' => $result['Log']['model_id']),
                'recursive' => -1
            ));
            $result['Log']['model_label'] = $model->label;
            if (!empty($data)) {
                $result[$alias] = $data[$alias];
                if ('Image' == $alias) {
                    App::import('Model', 'Gallery');
                    $Gallery = new Gallery();
                    $gallery = $Gallery->findById($result[$alias]['gallery_id']);
                    if (!empty($gallery)) {
                        $result[$alias]['node_id'] = $gallery['Gallery']['node_id'];
                    }
                }
            }
        }
        return $results;
    }
}
