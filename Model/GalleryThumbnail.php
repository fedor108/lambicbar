<?php
App::uses('AppModel', 'Model');
/**
 * GalleryThumbnail Model
 *
 * @property Thumbnail $Thumbnail
 */
class GalleryThumbnail extends AppModel
{
    public $name = 'GallerySetting';
    public $label = 'Настройки галерей';
/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'gallery_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'thumbnail_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Thumbnail' => array(
            'className' => 'Thumbnail',
            'foreignKey' => 'thumbnail_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
