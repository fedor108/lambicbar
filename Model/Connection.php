<?php
App::uses('AppModel', 'Model');
/**
 * Connection Model
 *
 * @property Node $Node
 * @property Term $Term
 * @property Dictionary $Dictionary
 *
 * Обеспечивает связь Узел (node_id) - Узел (term_id)
 */
class Connection extends AppModel
{
    public $name = 'Connection';
    public $label = 'Связь';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'node_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'term_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'dictionary_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Term' => array(
            'className' => 'Node',
            'foreignKey' => 'term_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Dictionary' => array(
            'className' => 'Node',
            'foreignKey' => 'dictionary_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $hasMany = array(
        'Price' => array(
            'className' => 'Price',
            'foreignKey' => 'model_id',
            'dependent' => true,
            'conditions' => array('model_alias' => 'Connection'),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
    );

/**
 * Список id подключенных узлов к термину
 */
    public function connectedNodes($term_id)
    {
        $node_ids = $this->find('list', array(
            'conditions' => array(
                'active' => 1,
                'term_id' => $term_id,
            ),
            'fields' => array('id', 'node_id')
        ));
        return $node_ids;
    }

/**
 * Убираем связи
 * $node_ids - список id узлов для удаления связей
 * $term_ids - список id терминов
 */
    public function unsetToNodes($node_ids, $term_ids)
    {
        if (empty($term_ids)) {
            return true;
        }

        $connections = $this->find('all', array(
            'conditions' => array(
                'Connection.node_id' => $node_ids,
                'Connection.active' => 1,
                'Connection.term_id' => $term_ids,
            ),
            'recursive' => -1
        ));

        if (!empty($connections)) {
            foreach ($connections as $n => $item) {
                $this->id = $item['Connection']['id'];
                $this->saveField('active', 0);
            }
        }
    }

/**
 * Добавляем связи
 * $node_ids - список id узлов для установки связей
 * $term_ids - список id терминов
 * $dictionary_id - id справочника
 */
    public function setToNodes($node_ids, $term_ids, $dictionary_id)
    {
        if (empty($term_ids)) {
            return true;
        }
        if (!is_array($node_ids)) {
            $node_ids = array($node_ids);
        }
        foreach ($node_ids as $node_id) {
            $connections = $this->find('all', array(
                'conditions' => array(
                    'Connection.node_id' => $node_id,
                    'Connection.dictionary_id' => $dictionary_id,
                    'Connection.term_id' => $term_ids,
                ),
                'recursive' => -1
            ));
            $connections = Hash::combine($connections, '{n}.Connection.term_id', '{n}.Connection');

            foreach ($term_ids as $term_id) {
                if (array_key_exists($term_id, $connections)) {
                    $this->id = $connections[$term_id]['id'];
                    $this->saveField('active', 1);
                } else {
                    $connection = array(
                        'node_id' => $node_id,
                        'term_id' => $term_id,
                        'active' => 1,
                        'dictionary_id' => $dictionary_id,
                    );
                    $this->create();
                    $this->save(array('Connection' => $connection));
                }
            }
        }
    }

}
