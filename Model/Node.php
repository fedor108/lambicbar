<?php
App::uses('AppModel', 'Model');
/**
 * Node Model
 *
 * @property Node $ParentNode
 * @property User $User
 * @property Announcement $Announcement
 * @property Attachment $Attachment
 * @property Connection $Connection
 * @property Dictionary $Dictionary
 * @property Flag $Flag
 * @property Line $Line
 * @property Link $Link
 * @property Menu $Menu
 * @property Node $ChildNode
 * @property Number $Number
 * @property Seo $Seo
 * @property Text $Text
 */
class Node extends AppModel
{

/**
 * Behaviors
 *
 * @var array
 */
    public $name = 'Node';
    public $label = 'Узел';
    public $actsAs = array(
        'Tree', 'Containable',
    );

    public $order = "Node.lft";

    public $types = array(
        'page' => 'Страница',
        'event' => 'Событие',
        'place' => 'Место',
        'product' => 'Блюдо'
    );

    public $states = array(
        'draft' => 'Черновик',
        'published' => 'Опубликован',
    );

    public $recursive = 1;

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'title' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'node_type' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'node_state' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'ParentNode' => array(
            'className' => 'Node',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => 'children_count',
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Attachment' => array(
            'className' => 'Attachment',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
        'Connection' => array(
            'className' => 'Connection',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Dictionary' => array(
            'className' => 'Dictionary',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Flag' => array(
            'className' => 'Flag',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
            'label' => 'Логическое поле'
        ),
        'Line' => array(
            'className' => 'Line',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
            'label' => 'Строка'
        ),
        'Link' => array(
            'className' => 'Link',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
            'label' => 'Ссылка'
        ),
        'Number' => array(
            'className' => 'Number',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
            'label' => 'Число'
        ),
        'Text' => array(
            'className' => 'Text',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
            'label' => 'Текст'
        ),
        'ChildNode' => array(
            'className' => 'Node',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => 'lft',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
        'Gallery' => array(
            'className' => 'Gallery',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
            'label' => 'Галерея'
        ),

        'Price' => array(
            'className' => 'Price',
            'foreignKey' => 'model_id',
            'dependent' => true,
            'conditions' => array('model_alias' => 'Node'),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );

    public $hasOne = array(
        'Beer' => array(
            'className' => 'Beer',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Seo' => array(
            'className' => 'Seo',
            'foreignKey' => 'node_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

/**
 *  Сортировка в соответствии с новым порядком
 *  Отличие старого и нового порядка в положении ОДНОГО узла
 */
    public function treeSort($sort)
    {
        // родительский узел, его потомков сортируем
        $parent = $this->find('first', array(
            'conditions' => array('Node.id' => $sort[0]),
            'recursive'  => -1
        ));

        // узлы до сортировки
        $data = $this->find('list', array(
            'conditions' => array('Node.parent_id' => $parent['Node']['parent_id']),
            'fields'     => array('id')
        ));
        $data = array_values($data);

        $diff_1 = array_values(array_diff_assoc($sort, $data));
        $diff_2 = array_values(array_diff_assoc($data, $sort));
        $delta = count($diff_1) - 1;

        if ($diff_1[0] == $diff_2[$delta]) {
            // переместили вверх
            $this->moveUp($diff_1[0], $delta);
        } else {
            // переместили вниз
            $this->moveDown($diff_2[0], $delta);
        }
    }

/**
 * Список связанных данных для добавления
 * node_id - узел, к которому могут добавляться данные
 * node_type - тип узла
 */
    public function itemsForAdd($node_id, $node_type)
    {
        // список id узлов данного типа
        $node_ids = $this->find('list', array(
            'conditions' => array('node_type' => $node_type),
            'fields'     => 'id'
        ));
        foreach ($this->hasMany as $key => $item) {
            if (isset($item['label'])) {
                $items[$key] = $item;

                // список уже существующих элементов у редактируемого узла
                // для предотвращения создания дубликатов
                $names_exists = $this->{$item['className']}->find('list', array(
                    'conditions' => array($item['foreignKey'] => $node_id),
                    'fields'     => array('id', 'name')
                ));
                $items[$key]['add'] = $this->{$item['className']}->find('list', array(
                    'conditions' => array(
                        $item['foreignKey'] => $node_ids,
                        'name !='           => $names_exists
                    ),
                    'fields'    => array('name', 'label'),
                    'group'     => array('name'),
                    'recursive' => -1
                ));
            }
        }

        return $items;
    }

/**
 * Перед сохранением узла
 */
    public function beforeSave($options = array())
    {
        $this->setName($this->data[$this->alias]);
        $this->setSlug($this->data[$this->alias]);
        $this->setPublished($this->data[$this->alias]);
        return true;
    }

/**
 * Установка даты публикации
 */
    private function setPublished(&$node)
    {
        if (!empty($node['Node']['node_state'])
            && ('published' == $node['Node']['node_state'])
        ) {
            if (array_key_exists('published', $node['Node'])) {
                $published = $node['Node']['published'];
            } else {
                $published = $this->field('published');
            }
            if (empty($published)) {
                $node['Node']['published'] = date("Y-m-d H:i:s");
            }
        }
    }

/**
 * Установка латинского обозначения
 */
    private function setName(&$node)
    {
        if (empty($node['id'])) {
            return null;
        } else {
            if (!array_key_exists('name', $node)) {
                $this->id = $node['id'];
                $node['name'] = $this->field('name');
            } else {
                $name = $node['name'];
                $name = preg_replace('/[^a-zA-Z0-9-_]/', '', $name);
                $name = preg_replace('/-{2,}/', '-', $name);
                $name = preg_replace('/_{2,}/', '_', $name);
                $name = strtolower($name);
                if (empty($name)) {
                    $node['name'] = $node['id'];
                } else {
                    $node['name'] = $name;
                }
            }
        }
    }

/**
 * Установка адреса узла на сайте
 */
    private function setSlug(&$node)
    {
        if (empty($node['parent_id'])) {
            if (!empty($node['id'])) {
                $this->id = $node['id'];
                $node['parent_id'] = $this->field('parent_id');
            }
        }

        if (empty($node['parent_id'])) {
            // адрес главной страницы
            $node['slug'] = '/';
        } else {
            $this->contain();
            $parent = $this->findById($node['parent_id']);
            $node['slug'] = $parent[$this->alias]['slug'];
            if ('/' != substr($node['slug'], -1)) {
                $node['slug'] .= '/';
            }
            $node['slug'] .= $node['name'];
            $node['slug'] = str_replace("//", "/", $node['slug']);
        }

        if (!empty($node['id'])) {
            $this->id = $node['id'];
            if ($node['slug'] != $this->field('slug')) {
                $node['updated_slug'] = true;
            }
        }
    }


/**
 * После сохранения узла
 */
    public function afterSave($created, $options = array())
    {
        if ($created) {
            // при создании узла
            $this->createTypicalItems($this->data);
        } else {
            // при обновлении узла
            $this->updateChildSlugs($this->data[$this->alias]);
        }

        return true;
    }

/**
 * Проверка обновление поля slug у потомков
 */
    private function updateChildSlugs($node)
    {
        // по установленному при сохранении флагу
        if (!empty($node['updated_slug'])) {
            $Node = new Node;
            $Node->contain();
            $children = $Node->children($node['id'], false, array('id', 'parent_id', 'name', 'slug'));
            $children = Hash::combine($children, "{n}.Node.id", "{n}.Node");
            foreach ($children as &$child) {
                if ($node['id'] == $child['parent_id']) {
                    $child['slug'] = $node['slug']
                        . '/' . $child['name'];
                } elseif (array_key_exists($child['parent_id'], $children)) {
                    $child['slug'] = $children[$child['parent_id']]['slug']
                        . '/' . $child['name'];
                }
                $child['slug'] = str_replace("//", "/", $child['slug']);
            }
            $Node->saveMany($children, array('callbacks' => false));
        }
    }

/**
 * Создать типовые элементы
 */
    private function createTypicalItems($node)
    {
        $items = $this->typicalItems($node['Node']['node_type']);
        if (empty($items)) {
            return null;
        }

        foreach ($items as $alias => $data) {
            if (array_key_exists($alias, $this->hasMany)) {
                foreach ($data as $item) {
                    $this->createTypicalItem($node['Node']['id'], $item, $alias);
                }
            } else {
                $this->createTypicalItem($node['Node']['id'], $data, $alias);
            }
        }
    }

/**
 * Создать типовой элемент
 */
    private function createTypicalItem($node_id, $item, $alias)
    {
        // внешний ключ для связи с родительским узлом
        $item['node_id'] = $node_id;

        // внешний ключ для Price
        $item['model_id'] = $node_id;
        $item['model_alias'] = $this->alias;

        $this->{$alias}->create();
        $this->{$alias}->save($item);
    }

/**
 * Набор типовых полей узла
 */
    public function typicalItems($node_type)
    {
        $items = array();

        // страница
        $items['page'] = array();
        $items['page']['Text'][] = array('name' => 'main', 'label' => 'Основной текст', 'editor' => 'visual');
        $items['page']['Seo'] = array();

        // событие
        $items['event'] = array();
        $items['event']['Text'][] = array('name' => 'main', 'label' => 'Основной текст', 'editor' => 'visual');
        $items['event']['Text'][] = array('name' => 'announce', 'label' => 'Анонс', 'editor' => 'plain');
        $items['event']['Gallery'][] = array('name' => 'main', 'label' => 'Основная галерея');

        // место
        $items['place'] = array();
        $items['place']['Text'][] = array('name' => 'address', 'label' => 'Адрес', 'editor' => 'plain');
        $items['place']['Text'][] = array('name' => 'schedule', 'label' => 'Режим работы', 'editor' => 'plain');
        $items['place']['Line'][] = array('name' => 'phone', 'label' => 'Телефон');
        $items['place']['Line'][] = array('name' => 'email', 'label' => 'Эл. адес');
        $items['place']['Flag'][] = array('name' => 'veranda', 'label' => 'Веранда');
        $items['place']['Gallery'][] = array('name' => 'main', 'label' => 'Основная галерея');

        // блюдо
        $items['product'] = array();
        $items['product']['Text'][] = array('name' => 'announce', 'label' => 'Описание блюда', 'editor' => 'plain');
        $items['product']['Price'] = array();
        $items['product']['Flag'][] = array('name' => 'new', 'label' => 'Новинка', 'value' => 1);
        $items['product']['Gallery'][] = array('name' => 'main', 'label' => 'Основная галерея');

        return $items[$node_type];
    }

/**
 * Умная галочка наличия для разделов меню
 * Проверка связей после сохранения узла и его связей
 * Работает для справочника address - наличие блюд в ресторане
 */
    public function checkConnections($id)
    {
        // справочник - узел раздела Адреса
        $address = $this->find('first', array(
            'conditions' => array('Node.name' => 'address'),
            'recursive' => -1
        ));
        $dictionary_id = $address['Node']['id'];

        // получаем нужные данные сохраненного узла
        $fields = array('id', 'parent_id', 'lft', 'rght', 'node_type', 'slug');
        $node = $this->find('first', array(
            'conditions' => array('Node.id' => $id),
            'contain' => array(
                'Connection' => array(
                    'conditions' => array('Connection.dictionary_id' => $dictionary_id)
                ),
            ),
            'fields' => $fields,
        ));

        // определяем раздел узла
        $path = explode('/', $node['Node']['slug']);
        if ((count($path) < 2)
            or !in_array($path[1], array('menu', 'beer'))
        ) {
            // узел не из раздела Меню или Пиво
            return true;
        }

        if ('page' == $node['Node']['node_type']) {
            // разделы и подразделы меню
            // проверить и обновить связи потомков
            $this->checkChildrenConnections($node, $dictionary_id);

        } elseif ('product' == $node['Node']['node_type']) {
            // блюда меню
            // проверяем и обновляем связи предка
            $this->checkParentConnections($node['Node']['parent_id'], $dictionary_id);
        }
    }

    private function checkChildrenConnections($node, $dictionary_id)
    {
        $children = $this->find('all', array(
            'conditions' => array(
                'Node.lft >' => $node['Node']['lft'],
                'Node.rght <' => $node['Node']['rght']
            ),
            'contain' => array(
                'Connection' => array(
                    'conditions' => array('Connection.dictionary_id' => $dictionary_id),
                ),
            ),
        ));
        $child_ids = Hash::extract($children, '{n}.Node.id');

        // список всех подключенных терминов к потомкам
        $children_connected = array();
        foreach ($children as $n => $child) {
            if (!empty($child['connected'])) {
                foreach ($child['connected'] as $term_id) {
                    if (!in_array($term_id, $children_connected)) {
                        $children_connected[] = $term_id;
                    }
                }
            }
        }

        // список терминов, связь с которыми не установлена, но есть у потомков
        // эти связи нужно убрать у потомков
        $to_unset = $children_connected;
        if (!empty($node['connected'])) {
            $to_unset = array_diff($children_connected, $node['connected']);
        }
        $this->Connection->unsetToNodes($child_ids, $to_unset, $dictionary_id);

        // список терминов, связь с которыми установлена, но ее нет у потомков
        // эти связи установить потомкам
        $to_set = array();
        if (!empty($node['connected'])) {
            $to_set = array_diff($node['connected'], $children_connected);
        }
        $this->Connection->setToNodes($child_ids, $to_set, $dictionary_id);

        // проверяем и обновляем связи предка
        $this->checkParentConnections($node['Node']['parent_id'], $dictionary_id);

    }

/**
 * Проверка и обновление связей предка
 * $id предка
 * $dictionary_id - id справочника
 */
    public function checkParentConnections($id, $dictionary_id)
    {
        // получаем нужные данные предка сохраненного узла
        $fields = array('id', 'parent_id', 'lft', 'rght', 'node_type', 'slug');
        $node = $this->find('first', array(
            'conditions' => array('Node.id' => $id),
            'contain' => array(
                'Connection' => array(
                    'conditions' => array('Connection.dictionary_id' => $dictionary_id)
                ),
            ),
            'fields' => $fields,
        ));

        $path = explode('/', $node['Node']['slug']);
        if (!in_array($path[1], array('menu', 'beer'))
            or (count($path) < 3)
        ) {
            // узел не из раздела Меню или Пиво
            return true;
        }

        $children = $this->find('all', array(
            'conditions' => array(
                'Node.lft >' => $node['Node']['lft'],
                'Node.rght <' => $node['Node']['rght']
            ),
            'contain' => array(
                'Connection' => array(
                    'conditions' => array('Connection.dictionary_id' => $dictionary_id),
                ),
            ),
        ));
        $child_ids = Hash::extract($children, '{n}.Node.id');

        // список всех подключенных терминов к потомкам
        $children_connected = array();
        foreach ($children as $n => $child) {
            if (!empty($child['connected'])) {
                foreach ($child['connected'] as $term_id) {
                    if (!in_array($term_id, $children_connected)) {
                        $children_connected[] = $term_id;
                    }
                }
            }
        }

        // список терминов, связь с которыми установлена, но ее нет у потомков
        // эти связи нужно убрать у текущего узла
        $to_unset = array();
        if (!empty($node['connected'])) {
            $to_unset = array_diff($node['connected'], $children_connected);
        }
        $this->Connection->unsetToNodes($id, $to_unset);

        // список терминов, связь с которыми не установлена, но есть у потомков
        // эти связи нужно установить у текущего узла
        $to_set = $children_connected;
        if (!empty($node['connected'])) {
            $to_set = array_diff($children_connected, $node['connected']);
        }
        $this->Connection->setToNodes($id, $to_set, $dictionary_id);

        // проверяем у предка
        $this->checkParentConnections($node['Node']['parent_id'], $dictionary_id);
    }

/**
 * После получения данных
 */
    public function afterFind($results, $primary = false)
    {
        if ($primary) {
            foreach ($results as $n => &$result) {

                // формируем массив named для доступа к данным по ключу name
                foreach ($this->hasMany as $key => $value) {
                    if (!empty($result[$key][0]['name'])) {
                        $result['named'][$key] = Hash::combine($result[$key], '{n}.name', '{n}');
                    }
                }

                // формируем массив подключенных к узлу терминов
                if (!empty($result['Connection'])) {
                    $terms = Hash::combine($result['Connection'], '{n}.term_id', '{n}.active');
                    $connected = array();
                    foreach ($terms as $term_id => $active) {
                        if ($active) {
                            $connected[] = $term_id;
                        }
                    }
                    if (!empty($connected)) {
                        $result['connected'] = $connected;
                    }
                }

                // подменяем Connection:
                // дополняем неактивными записями всех терминов из подключенных справочников
                if (array_key_exists('Connection', $result)) {
                    $result['Connection'] = $this->connections($result);
                }
            }
        }
        return $results;
    }

/**
 * Формируем полный массив связей,
 * На основе списка всех терминов из подключенных справочников
 */
    private function connections($node)
    {
        $path = $this->getPath($node['Node']['id']);

        // подключенные справочники
        $dictionaries = $this->Dictionary->find('all', array(
            'conditions' => array('Dictionary.node_id' => Hash::extract($path, '{n}.Node.id')),
            'recursive'  => 1
        ));
        if (empty($dictionaries)) {
            return array();
        }

        // все термины подключенных справочников
        $terms = $this->find('all', array(
            'conditions' => array('Node.parent_id' => Hash::extract($dictionaries, '{n}.Dictionary.dictionary_id')),
            'recursive'  => -1
        ));
        if (empty($terms)) {
            return array();
        }

        // имеющиеся записи соединений с доступом по ключу term_id
        $connected = Hash::combine($node['Connection'], '{n}.term_id', '{n}');

        // полный массив соединений
        $connections = array();
        foreach ($terms as $term) {
            $term_id = $term['Node']['id'];
            if (array_key_exists($term_id, $connected)) {
                $connections[] = $connected[$term_id];
            } else {
                $connections[] = array(
                    'node_id' => $node['Node']['id'],
                    'term_id' => $term_id,
                    'active' => false,
                    'dictionary_id' => $term['Node']['parent_id'],
                    'Node' => $node['Node'],
                    'Term' => $term['Node'],
                );
            }
        }

        return $connections;
    }

/**
 * Правильное получение path узла
 */
    public function getNodePath($id)
    {
        $path = array();
        $Node = new Node;
        $Node->contain('Seo');
        $node = $Node->findById($id);
        while ($node) {
            $path[] = $node;
            $Node->contain('Seo');
            $node = $Node->findById($node['Node']['parent_id']);
        }
        return array_reverse($path);
    }

}
