<?php
App::uses('AppModel', 'Model');
/**
 * Number Model
 *
 * @property Node $Node
 */
class Number extends AppModel
{
    public $name = 'Number';
    public $label = 'Число';

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
