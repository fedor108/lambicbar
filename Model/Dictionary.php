<?php
App::uses('AppModel', 'Model');
/**
 * Dictionary Model
 *
 * @property Node $Node
 * @property Dictionary $Dictionary
 * @property Connection $Connection
 * @property Dictionary $Dictionary
 *
 * Подключение справочников к разделам сайта
 * node_id - раздел
 * dictionary_id - справочник
 */
class Dictionary extends AppModel
{

    public $name = 'Dictionary';
    public $label = 'Справочник';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'node_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'dictionary_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DictionaryNode' => array(
            'className' => 'Node',
            'foreignKey' => 'dictionary_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * Сохранение подключенных к узлу справочников
 * Удаление отключенных
 *
 */
    public function save($data = null, $validate = true, $fieldList = array()) {
        if (empty($data['id']) && (!empty($data['value']))) {
            // установить связь
            $result = parent::save($data, $validate, $fieldList);
        } elseif (!empty($data['id']) && (empty($data['value']))) {
            // удалить связь
            $result = $this->delete($data['id']);
        } else {
            // ничего не делать
            $result = true;
        }
        return $result;
    }

}
