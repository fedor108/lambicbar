<?php
App::uses('AppModel', 'Model');
/**
 * Link Model - Ссылка
 *
 * @property Node $Node
 */
class Link extends AppModel
{
    public $name = 'Link';
    public $label = 'Ссылка';
/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'node_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * Данные по умолчанию, при создании новой записи
 *
 * @var array
 */
    public $targets = array(
        '' => 'Нормально',
        '_blank' => 'В новом окне',
    );
}
