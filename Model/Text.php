<?php
App::uses('AppModel', 'Model');
App::uses('RemoteTypograf', 'Vendor');
/**
 * Text Model - Текст
 *
 * @property Node $Node
 */
class Text extends AppModel
{
    public $name = 'Text';
    public $label = 'Текст';

    public $editors = array(
        'plain'  => 'Только текст',
        'html'   => 'Произвольный HTML',
        'visual' => 'Визуальный редактор'
    );

    public $types = array(
        'main' => array(
            'label'  => 'Основной текст',
            'editor' => 'visual'
        ),
        'announce' => array(
            'label'  => 'Краткий текст',
            'editor' => 'plain'
        ),
        'address' => array(
            'label'  => 'Адрес',
            'editor' => 'plain'
        ),
    );

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'visual_editor' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * Перед сохранением данных
 */
    public function beforeSave($options = array())
    {
        // Обработка типографом
        if (!empty($this->data['Text']['value'])) {
            $typo = new RemoteTypograf('utf8');
            $typo->noEntities();
            $typo->br(false);
            $typo->p(false);
            $typo->nobr(3);
            $typo->quotA('laquo raquo');
            $typo->quotB('bdquo ldquo');
            $this->data['Text']['value'] = $typo->processText($this->data['Text']['value']);
        }
        return parent::beforeSave($options);
    }
}
