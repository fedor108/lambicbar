<?php
App::uses('AppModel', 'Model');
/**
 * GallerySetting Model - Настройки галерей
 *
 */
class GallerySetting extends AppModel
{
    public $name = 'GallerySetting';
    public $label = 'Настройки галерей';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'gallery_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'resize' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
}
