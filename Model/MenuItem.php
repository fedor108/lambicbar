<?php
App::uses('AppModel', 'Model');
/**
 * MenuItem Model - Модель пунктов навигационного меню
 *
 * @property Menu $Menu
 * @property Node $Node
 */
class MenuItem extends AppModel
{
    public $name = 'MenuItem';
    public $label = 'Пункт меню';

/**
 * Behaviors
 *
 */
    public $actsAs = array(
        'Tree', 'Containable'
    );

    public $order = "MenuItem.lft";

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'menu_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Menu' => array(
            'className' => 'Menu',
            'foreignKey' => 'menu_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => array('id', 'title', 'slug', 'node_state'),
            'order' => ''
        )
    );

/**
 * После получения данных
 */
    public function afterFind($results, $primary = false)
    {
        if ($primary) {
            foreach ($results as $n => &$result) {
                if (!empty($result['Node'])) {
                    if (!empty($result['MenuItem']['node_id'])) {
                        $result['MenuItem']['url'] = $result['Node']['slug'];
                        if (empty($result['MenuItem']['title'])) {
                            $result['MenuItem']['title'] = $result['Node']['title'];
                        }
                    }
                }
            }
        }
        return $results;
    }

/**
 * Сохранение нового порядка пунктов меню
 */
    public function reorder($order) {
        $this->Behaviors->disable('Tree', 'Logable');
        $this->setOrder($order);
        $this->Behaviors->enabled('Tree', 'Logable');
    }

/**
 * Рекурсивный расчет и сохранение нового порядка
 */
    private function setOrder(&$order, $parent_id = null, $i = 0) {
        $lft = $i + 1;
        foreach ($order as &$item) {
            $item['parent_id'] = $parent_id;
            $item['lft'] = $lft;
            if (empty($item['children'])) {
                $item['rght'] = $item['lft'] + 1;
            } else {
                $item['rght'] = $this->setOrder($item['children'], $item['id'], $item['lft']) + 1;
            }
            $rght = $item['rght'];
            $lft = $rght + 1;
            $this->save(array('MenuItem' => $item));
        }
        return $rght;
    }

/**
 * Получаем дерево элементов меню по Menu.name
 */
    public function getTree($name) {
        $this->Menu->recursive = -1;
        $menu = $this->Menu->findByName($name);
        $this->contain('Node');
        $items = $this->findAllByMenuId($menu['Menu']['id']);
        $items = Hash::nest($items);
        return $items;
    }

/**
 * Список узлов для выбора
 */
    public function nodes() {
        $data = $this->Node->find('all', array(
            'conditions' => array(
                'Node.node_type' => 'page',
                'Node.node_state' => 'published',
                'Node.slug !=' => ''
            ),
            'recursive' => -1,
            'fields' => array('id', 'title', 'slug')
        ));
        $prefix = "____________________";
        $nodes = array(0 => '');
        foreach ($data as &$node) {
            $depth = substr_count($node['Node']['slug'], '/');
            $str = substr($prefix, 0, 2 * ($depth - 1));
            $str = "{$str} {$node['Node']['title']}: {$node['Node']['slug']}";
            $nodes[$node['Node']['id']] = $str;
        }
        return $nodes;
    }
}
