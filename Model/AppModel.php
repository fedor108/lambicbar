<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{
    public $actsAs = array(
        'Loggable'
    );

/**
 * Сохранение моделей
 */
    public function save($data = null, $validate = true, $fieldList = array())
    {
        if (!empty($data['delete'])) {
            // удаление по галочке Удалить
            $result = $this->delete($data['id']);
        } elseif (empty($data['id']) && array_key_exists('create', $data) && empty($data['create'])) {
            // не создавать, если есть неустановленный флаг Создать
            $result = true;
        } else {
            // сохранить данные
            $result = parent::save($data, $validate, $fieldList);
        }
        return $result;
    }
}
