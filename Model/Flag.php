<?php
App::uses('AppModel', 'Model');
/**
 * Flag Model - Логические данные
 *
 * @property Node $Node
 */
class Flag extends AppModel
{
    public $name = 'Flag';
    public $label = 'Флаг';

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
