<?php
App::uses('AppModel', 'Model');
/**
 * Seo Model
 *
 * @property Node $Node
 */
class Seo extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'node_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

}
