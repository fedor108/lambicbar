<?php
App::uses('AppModel', 'Model');
/**
 * Image Model - Изображение
 *
 * @property Gallery $Gallery
 * @property Announcement $Announcement
 */
class Image extends AppModel
{
    public $name = 'Image';
    public $label = 'Изображение';

    public $errors = array();

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'gallery_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'upload' => array(
            'rule' => array('checkMaxWidth', 1920),
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        )
    );

/**
 * Проверка типа файла изображения
 *
 */
    public function checkMaxWidth($check, $max_width)
    {
        list($width, $height, $type) = getimagesize($check['upload']['tmp_name']); //получаем данные загруженного файла
        if ($width > $max_width) {
            $this->errors[] = array(
                'filename' => $check['upload']['name'],
                'message' => "Ширина изображения больше {$max_width} px. Файл не сохранен."
            );
            return false;
        }
        return true;
    }

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Gallery' => array(
            'className' => 'Gallery',
            'foreignKey' => 'gallery_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * Перед сохранением данных
 */
    public function beforeSave($options = array())
    {
        if (!empty($this->data[$this->alias]['upload']) && !$this->data[$this->alias]['upload']['error']) {
            $src = $this->upload(
                $this->data[$this->alias]['upload'],
                $this->data[$this->alias]['gallery_id']
            );
            if (!empty($src)) {
                $this->data[$this->alias]['src'] = $src;
            }
        }
        return parent::beforeSave($options);
    }

/**
 * Загрузка изображений
 */
    private function upload($upload, $gallery_id)
    {
        $src = '';
        if (!$upload['error']) {

            // каталог для хранения изображений узла
            $dir = $dir = $this->WWW_ROOT . 'media/' . $gallery_id;
            if (!file_exists($dir)) {
                if (!mkdir($dir, 0755, true)) {
                    CakeLog::write('debug', 'Не удалось создать каталог: ' . $dir);
                    return false;
                }
            }

            // путь и имя файла для сохранения
            $file = $dir . DS . $upload['name'];

            // перемещаем загруженный файл
            move_uploaded_file($upload['tmp_name'], $file);

            // адрес изображения
            $src = "/media/{$gallery_id}/{$upload['name']}";
        }
        return $src;
    }
}
