<?php
App::uses('AppModel', 'Model');
/**
 * Gallery Model - Галерея
 *
 * @property Node $Node
 * @property Image $Image
 */
class Gallery extends AppModel {

    public $name = 'Gallery';
    public $label = 'Галерея';
    public $recursive = 1;

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'node_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'label' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Node',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Image' => array(
            'className' => 'Image',
            'foreignKey' => 'gallery_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => 'Image.order',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

/**
 * Загрузка изображений
 */
    public function beforeSave($options = array()) {
        if (!empty($this->data[$this->alias]['upload'])) {
            if (!empty($this->data[$this->alias]['id'])) {
                foreach ($this->data[$this->alias]['upload'] as $item) {
                    if (empty($item['error'])) {
                        $this->Image->create();
                        $this->Image->save(array(
                            'Image' => array(
                                'upload' => $item,
                                'gallery_id' => $this->data[$this->alias]['id']
                            )
                        ));
                    }
                }
                unset($this->data[$this->alias]['upload']);
            }
        }
        return true;
    }

}
