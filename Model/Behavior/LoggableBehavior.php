<?php
/**
 * Автоматическая запись изменений в журнал
 */
App::uses('ModelBehavior', 'Model');

class LoggableBehavior extends ModelBehavior
{

    protected $_defaults = array('ignore' => array('modified'));
    protected $beforeSaveData = array();
    public function setup(Model $Model, $config = array())
    {
        $this->settings[$Model->alias] = $config + $this->_defaults;
    }

/**
 * Перед сохранением запоминаем в $beforeSaveData, что было в базе
 */
    public function beforeSave(Model $Model, $options = array())
    {
        if (in_array($Model->alias, array('Log', 'Connection'))) {
            return true;
        }

        if (empty($Model->data[$Model->alias][$Model->primaryKey])) {
            return true;
        } else {
            $data = $Model->data[$Model->alias];
            $beforeSaveData = $Model->find('first', array(
                'conditions' => array($Model->primaryKey => $data[$Model->primaryKey]),
                'recursive' => -1
            ));
            $this->beforeSaveData[$Model->alias][$data[$Model->primaryKey]] = $beforeSaveData[$Model->alias];
            return true;
        }
    }

/**
 * После сохранения читаем, что стало в базе, сравниваем с тем, что было.
 * Если изменилось делаем запись
 */
    public function afterSave(Model $Model, $created, $options = array())
    {
        if ('Log' == $Model->alias) {
            return true;
        }

        $data = $Model->data[$Model->alias];
        $afterSaveData = $Model->find('first', array(
            'conditions' => array($Model->primaryKey => $data[$Model->primaryKey]),
            'recursive' => -1
        ));
        $afterSaveData = $afterSaveData[$Model->alias];
        if ($created) {
            // сохраняем запись журнала изменений
            App::import('Model','Log');
            $Log = new Log();
            $Log->create();
            $Log->save(array('Log' => array(
                'model_name' => $Model->alias,
                'model_id' => $afterSaveData[$Model->primaryKey],
                'action' => 'create',
                'user_id' => AuthComponent::user('id'),
            )));
        } else {
            if (!empty($this->beforeSaveData[$Model->alias][$data[$Model->primaryKey]])) {
                $beforeSaveData = $this->beforeSaveData[$Model->alias][$data[$Model->primaryKey]];

                // убираем поля, которые не учитываются в сравнении версий
                foreach ($this->settings[$Model->alias]['ignore'] as $key) {
                    unset($afterSaveData[$key]);
                    unset($beforeSaveData[$key]);
                }

                $diff = array_diff($beforeSaveData, $afterSaveData);
                if (!empty($diff)) {

                    // сохраняем запись журнала изменений
                    App::import('Model','Log');
                    $Log = new Log();
                    $Log->create();
                    $Log->save(array('Log' => array(
                        'model_name' => $Model->alias,
                        'model_id' => $afterSaveData[$Model->primaryKey],
                        'diff' => serialize($diff),
                        'action' => 'edit',
                        'user_id' => AuthComponent::user('id'),
                    )));
                }
            }
        }
        return true;
    }

/**
 * Перед удалением делаем запись с данными удаляемого объекта в журнал
 */
    public function beforeDelete(Model $Model, $cascade = true)
    {
        if ('Log' == $Model->alias) {
            return true;
        }
        $beforeDeleteData = $Model->find('first', array(
            'conditions' => array($Model->primaryKey => $Model->{$Model->primaryKey}),
            'recursive' => -1
        ));
        $beforeDeleteData = $beforeDeleteData[$Model->alias];

        App::import('Model','Log');
        $Log = new Log();

        // удаляем все записи про создание и изменение удаляемого объекта
        $logs = $Log->find('list', array(
            'conditions' => array(
                'Log.model_id' => $beforeDeleteData[$Model->primaryKey],
                'Log.model_name' => $Model->alias
            ),
            'fields' => array('id')
        ));
        $Log->deleteAll(array('Log.id' => array_keys($logs)));

        // создаем одну запись про удаление объекта
        $Log->create();
        $Log->save(array('Log' => array(
            'model_name' => $Model->alias,
            'model_id' => $beforeDeleteData[$Model->primaryKey],
            'diff' => serialize($beforeDeleteData),
            'action' => 'delete',
            'user_id' => AuthComponent::user('id'),
        )));
        return true;
    }
}
