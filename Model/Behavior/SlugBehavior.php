<?php
/**
 * Поведение для управления адресом узла на сайте
 */
App::uses('ModelBehavior', 'Model');

class SlugBehavior extends ModelBehavior {

/**
 * При сохранении узла, вычислить заново его адрес на сайте
 * Если адрес изменился, нужно скорректировать адрес для всех потомков
 */
    protected $_defaults = array(
        'root' => '',
        'sep' => '/',
        'name' => 'name',
        'slug' => 'slug',
        'parent' => 'parent_id',
        '__slugChange' => false
    );

    public function setup(Model $Model, $config = array()) {
        $this->settings[$Model->alias] = $config + $this->_defaults;
    }
/**
 * Перед сохранением данных
 */
    public function beforeSave(Model $Model, $options = array()) {
        extract($this->settings[$Model->alias]);

        if (empty($Model->data[$Model->alias][$Model->primaryKey])) {
            return true;
        } else {
            if (!array_key_exists($slug, $Model->data[$Model->alias])) {
                // новое значение slug не передано для сохранения прямо
                // но slug может измениться, если изменился name или slug родителя
                if (array_key_exists($parent, $Model->data[$Model->alias])) {
                    // если в сохраняемых данных есть id родителя
                    $parent_value = $Model->data[$Model->alias][$parent];
                } else {
                    // берем id родителя из базы
                    $parent_value = $Model->field($parent);
                }

                if (empty($parent_value)) {
                    // нет родителя - это корень
                    $Model->data[$Model->alias][$slug] = $root;
                } else {
                    // есть родитель - берем его slug
                    $parent_slug = $Model->field($slug, array($Model->primaryKey => $parent_value));

                    if (array_key_exists($name, $Model->data[$Model->alias])) {
                        // если в сохраняемых данных есть name
                        $name_value = $Model->data[$Model->alias][$name];
                    } else {
                        // берем name из базы
                        $name_value = $Model->field($name);
                    }

                    // новое значение slug
                    // будет записано в базу автоматически, при продолжении сохранения
                    $Model->data[$Model->alias][$slug] = "{$parent_slug}{$sep}{$name_value}";

                    // специальные фильтры для узлов
                    if ('Node' == $Model->alias) {
                        $Model->data[$Model->alias][$slug] = $this->nodeSlug($Model->data[$Model->alias][$slug]);
                    }
                }
            }
            if ($Model->data[$Model->alias][$slug] != $Model->field($slug)) {
                // если slug изменился - ставим флаг
                $this->settings[$Model->alias]['__slugChange'] = true;
            }
            return true;
        }
    }

/**
 * Специальные адреса узлов в разделах menu, beer
 */
    private function menuSlug($slug_value)
    {
        $result = $slug_value;
        if (false !== strpos($slug_value, '/menu/')) {
            // формируем адрес, ведущий на страницу раздела меню: /menu/{section}
            $arr = explode('/', $slug_value);
            $result = '';
            foreach ($arr as $name) {
                if (!empty($name)) {
                    $result .= "/{$name}";
                }
                if (false !== strpos($result, '/menu/')) {
                    break;
                }
            }
        }
        return $result;
    }

/**
 * Специальные адреса в разделе address
 */
    private function addressSlug($slug_value)
    {
        $result = $slug_value;
        if (false !== strpos($slug_value, '/address')) {
            $result = '/address';
        }
        return $result;
    }

/**
 * Специальные адреса для модели Node
 */
    private function nodeSlug($slug_value)
    {
        $slug_value = $this->menuSlug($slug_value);
        $slug_value = $this->addressSlug($slug_value);
        return $slug_value;
    }

/**
 * После сохранения данных
 */
    public function afterSave(Model $Model, $created, $options = array()) {
        extract($this->settings[$Model->alias]);
        if ($__slugChange) {
            // slug изменился и нужно обновить его у всех потомков
            $children = $Model->find('all', array(
                'conditions' => array($parent => $Model->data[$Model->alias][$Model->primaryKey]),
                'recursive' => -1
            ));
            if (!empty($children)) {
                $slug_value = $Model->data[$Model->alias][$slug];
                $save = array();
                foreach ($children as $child) {
                    // общее правило определения адреса потомков
                    $child_slug = "{$slug_value}{$sep}{$child[$Model->alias][$name]}";

                    // специальные фильтры для узлов
                    if ('Node' == $Model->alias) {
                        $child_slug = $this->nodeSlug($child_slug);
                    }

                    // если адрес потомка изменился
                    if ($child[$Model->alias][$slug] != $child_slug) {
                        // сохраняем
                        $save[] = array(
                            $Model->primaryKey => $child[$Model->alias][$Model->primaryKey],
                            $slug => $child_slug,
                        );
                    }
                }
                $Model->saveAll($save);
            }
        }
        return true;
    }
}
