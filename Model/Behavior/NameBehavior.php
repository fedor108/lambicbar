<?php
/**
 * Поведение для управления латинским обозначением
 */
App::uses('ModelBehavior', 'Model');

class NameBehavior extends ModelBehavior {

/**
 * Проверка заполненности и фильтрация поля 'name' при сохранении данных модели
 * Вместо пустого поля будет id
 * В заполненном поле останутся только маленькие латинские буквы, цифры, подчеркивание и дефис
 */
    public function beforeSave(Model $Model, $options = array()) {
        if (array_key_exists('name', $Model->data[$Model->alias]) && isset($Model->data[$Model->alias]['id'])) {
            if (empty($Model->data[$Model->alias]['name'])) {
                $Model->data[$Model->alias]['name'] = $Model->data[$Model->alias]['id'];
            } else {
                $name = $Model->data[$Model->alias]['name'];
                $name = preg_replace('/[^a-zA-Z0-9-_]/', '', $name);
                $name = preg_replace('/-{2,}/', '-', $name);
                $name = preg_replace('/_{2,}/', '_', $name);
                $name = strtolower($name);
                if (empty($name)) {
                    $name = $Model->data[$Model->alias]['id'];
                }
                $Model->data[$Model->alias]['name'] = $name;
            }
        }
        return true;
    }
}
