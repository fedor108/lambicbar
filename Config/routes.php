<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
    // Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));

/**
 * Вход и выход
 */
    Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
    Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));

/**
 * Маршрутизация для корня админ-панели дальше работает маршрутизация по умолчанию
 */
    Router::redirect('/admin/', array('controller' => 'nodes', 'action' => 'index', 'admin' => true));
    Router::redirect('/admin', array('controller' => 'nodes', 'action' => 'index', 'admin' => true));

/**
 * Статические страницы
 */
    Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Раздел меню
 */
    App::uses('CatalogRoute', 'Lib/Routes');
    Router::connect('/*', array('controller' => 'public', 'action' => 'catalog'), array('routeClass' => 'CatalogRoute'));

/**
 * Маршрутизация по slug узла
 */
    App::uses('SlugRoute', 'Lib/Routes');
    Router::connect('/*', array('controller' => 'public', 'action' => 'node'), array('routeClass' => 'SlugRoute'));

/**
 * Карта сайта Xml
 */
    Router::connect('/sitemap.xml', array('controller' => 'public', 'action' => 'sitemap'));

/**
 * Подключаем маршруты плагинов
 */
	CakePlugin::routes();

/**
 * Правила маршрутизации по умолчанию
 */
	require CAKE . 'Config' . DS . 'routes.php';
