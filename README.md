Сеть ресторанов
http://lambicbar.ru

# Установка

	0. Установить Apache2

	1. Подготовить каталог проекта
		mkdir www/lambicbar
		cd www/lambicbar
		git init
		git remote add origin git@bitbucket.org:fedor108/lambicbar.git
		git pull origin master

	2. Дать доступ для записи кэша и логов
		или sudo chown -R www-data:www-data tmp
		или chmod -R 777 tmp

	3. Настроить локальный сайт
	    DocumentRoot www/lambicbar/webroot

	4. Скопировать библиотеку cakephp
		cd www
		git clone git@bitbucket.org:fedor108/cakephp2.git lib

# Верстка

	View/Layouts/public.ctp - общий лейаут
	View/Pages/home.ctp - главная страница - http://lambicbar.localhost/
	View/Pages/page.ctp - другие страницы - http:://lambicbar.localhost/pages/page
	View/Pages/dir/page.ctp - другие страницы - http:://lambicbar.localhost/pages/dir/page
	View/Elements/header.ctp - элемент отображения хедера
		Подключается в лайауте или отображении <?php echo $this->element('header'); ?>

# Статические материалы

	Размещать в www/lambicbar/webroot/
		js/
		css/
		fonts/
		images/

	В шаблонах (.ctp) пути к статическим материалам без webroot
		/js/
		/css/
		/fonts/
		/images/

# Frontend

	1. Установить зависимости из npm
		cd webroot
		npm install
	2. Установить bower компоненты
		cd webroot
		bower update
	3. Автоматически отслеживать изменения в файлах sass и компилировать цсс
		cd webroot
		grunt dev

# Публикация на рабочий сервер http://lambicbar.ru

	bin/push